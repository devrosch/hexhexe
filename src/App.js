import React, { Component } from 'react';
import EditorPane from './components/editor/EditorPane';
import ValuePane from './components/values/ValuePane';
import { Layout, Row, Col } from 'antd';
import StatusBar from './components/StatusBar';
import NavBar from './components/menu/NavBar';
import Document from './model/Document';
import BufferedDocument from './model/BufferedDocument';
import ChunkBufferingDocument from './model/ChunkBufferingDocument';
import SettingsStorage from './model/SettingsStorage';
import './App.css';

const { Content, Header } = Layout;

class App extends Component {
  constructor(props) {
    super(props);
    this.settingsStorage = new SettingsStorage();
    this.newDocumentKey = 1;
    this.newFileNamePostfix = 1;
    this.state = {
      bytesPerRow: this.settingsStorage.bytesPerRow,
      byteGroupSize: this.settingsStorage.byteGroupSize,
      documents: [],
      activeDocumentKey: null,
      status: {
        type: null,
        message: null
      },
      theme: 'green', //currently not used
    };
    this.fileInput = React.createRef();
    this.editorPane = React.createRef();
    this.valuePane = React.createRef();
    // make this binding work as expected for functions
    this.fileNew = this.fileNew.bind(this);
    this.fileOpen = this.fileOpen.bind(this);
    this.fileSave = this.fileSave.bind(this);
    this.fileClose = this.fileClose.bind(this);
    this.fileCloseAll = this.fileCloseAll.bind(this);
    this.setBytesPerRow = this.setBytesPerRow.bind(this);
    this.setByteGroupSize = this.setByteGroupSize.bind(this);
    this.setTheme = this.setTheme.bind(this);
    this.handleActiveDocumentUpdated = this.handleActiveDocumentUpdated.bind(this);
    this.handleActiveDocumentChanged = this.handleActiveDocumentChanged.bind(this);
    this.handleFocusScrollArea = this.handleFocusScrollArea.bind(this);
    this.handleDocumentReadError = this.handleDocumentReadError.bind(this);
    this.handleFileDropped = this.handleFileDropped.bind(this);
    this.handleDragOver = this.handleDragOver.bind(this);
    this.handleDragEnter = this.handleDragEnter.bind(this);
  }

  fileNew(length, initialValue) {
    const array = new Uint8Array(length);
    if (initialValue) {
      array.fill(initialValue);
    }
    const name = 'unnamed-' + this.newFileNamePostfix++ + '.bin';
    let file = '';
    try {
      file = new File([array], name);
    }
    catch (err) {
      // file constructor not supported by browser => work around with blob
      file = new Blob([array]);
      file.name = name;
    }
    this.fileOpen([file]);
  }

  fileOpen(selectedFiles) {
    if (!selectedFiles || selectedFiles.length <= 0) {
      return;
    }

    let docs = [];
    for (let i = 0; i < selectedFiles.length; i++) {
      const document = new Document(selectedFiles[i], '' + this.newDocumentKey++);
      const chunkBufferingDocument = new ChunkBufferingDocument(document);
      const bufferedDocument = new BufferedDocument(chunkBufferingDocument);
      docs.push(bufferedDocument);
    }

    const docNames = docs.map(d => d.name).join(', ');
    console.log('File(s) new/open: ' + docNames);

    this.setState((state, props) => {
      return {
        documents: state.documents.concat(docs),
        activeDocumentKey: docs[docs.length - 1].key,
        status: {
          type: 'info',
          message: 'File(s) opened: "' + docNames + '"'
        }
      }
    });

    setTimeout(() => this.editorPane.current.focusScrollArea(), 0);
  }

  fileSave() {
    console.log('File save');
    const activeDocumentKey = this.state.activeDocumentKey;
    const activeDocument = this.state.documents.find(d => d.key === activeDocumentKey);
    if (!activeDocument) {
      // no active document available to close
      return;
    }
    const blob = activeDocument.asBlob();
    // save blob via anchor element with download attribute and object URL
    let a = document.createElement("a");
    a.href = URL.createObjectURL(blob);
    a.download = activeDocument.name;
    document.body.appendChild(a);
    a.click();
    // remove element
    setTimeout(function () {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(a.href);
    }, 100);
  }

  fileClose(key) {
    console.log('File close');

    const activeDocumentKey = this.state.activeDocumentKey;
    const activeDocumentIndex = this.state.documents.findIndex(d => d.key === activeDocumentKey);
    let closeDocumentIndex = activeDocumentIndex;
    if (typeof (key) !== 'undefined') {
      // close other than active document
      closeDocumentIndex = this.state.documents.findIndex(d => d.key === key);
    }

    if (closeDocumentIndex < 0) {
      // no document to be closed could be found
      return;
    }

    const closeDocument = this.state.documents[closeDocumentIndex];
    // remove document from array
    const newDocumentArray = this.state.documents.filter(d => d.key !== closeDocument.key);

    // determine new active document
    let newActiveDocumentIndex = activeDocumentIndex;
    if (closeDocumentIndex < activeDocumentIndex) {
      // closing of active document or preceding document
      newActiveDocumentIndex--;
    }
    if (newDocumentArray.length <= 0) {
      // last document was closed
      newActiveDocumentIndex = -1;
    }
    else if (newDocumentArray.length >= newActiveDocumentIndex) {
      // rightmost document was closed
      newActiveDocumentIndex = newDocumentArray.length - 1;
    }
    const newActiveDocument = newActiveDocumentIndex >= 0 ?
      newDocumentArray[newActiveDocumentIndex] :
      null;
    const newActiveDocumentKey = newActiveDocument === null ?
      null : newActiveDocument.key;

    this.setState({
      documents: newDocumentArray,
      activeDocumentKey: newActiveDocumentKey,
      status: {
        type: 'info',
        message: 'File closed'
      }
    });
    
    if (newDocumentArray && newDocumentArray.length > 0) {
      setTimeout(() => this.editorPane.current.focusScrollArea(), 0);
    }
  }

  fileCloseAll() {
    console.log('File close all');
    this.setState({
      documents: [],
      activeDocumentKey: null,
      status: {
        type: 'info',
        message: 'All files closed'
      }
    });
  }

  handleDocumentReadError(documentKey, error) {
    if (this.lastReadErrorDocumentKey === documentKey) {
      // should be handled already
      return;
    }
    this.lastReadErrorDocumentKey = documentKey;
    console.log('Error reading document: ' + error);
    this.fileClose(documentKey);
    this.setState({ status: { type: 'error', message: error } });
  }

  handleActiveDocumentChanged(key) {
    console.log('Active document changed: ' + key);
    this.setState({ activeDocumentKey: key });
  }

  setBytesPerRow(numBytes) {
    console.log('Byte per row: ' + numBytes);
    this.settingsStorage.bytesPerRow = numBytes;
    this.setState({
      bytesPerRow: numBytes,
      status: { type: 'info', message: 'Byte per row: ' + numBytes }
    });
  }

  setByteGroupSize(size) {
    console.log('Byte group size: ' + size);
    this.settingsStorage.byteGroupSize = size;
    this.setState({
      byteGroupSize: size,
      status: { type: 'info', message: 'Byte group size: ' + size }
    });
  }

  setTheme(theme) {
    console.log(theme);
    // see https://superuser.com/questions/361297/what-colour-is-the-dark-green-on-old-fashioned-green-screen-computer-displays
    // see http://unusedino.de/ec64/technical/misc/vic656x/colors/
    this.setState({
      theme: theme,
      status: { type: 'info', message: 'Theme: ' + theme }
    });
  }

  handleActiveDocumentUpdated() {
    this.editorPane.current.scrollToCursorPosition();
    this.editorPane.current.focusScrollArea();
    this.forceUpdate();
  }

  handleFocusScrollArea() {
    this.editorPane.current.focusScrollArea();
  }

  handleFileDropped(e) {
    e.stopPropagation();
    e.preventDefault();
    const files = e.dataTransfer.files;
    // filter out directories, if possible
    // this is only possible with a non-standardized function
    // see: https://stackoverflow.com/questions/25016442/how-to-distinguish-if-a-file-or-folder-is-being-dragged-prior-to-it-being-droppe
    // see: https://html5-demos.appspot.com/static/dnd/all_types_of_import.html
    // see: https://developer.mozilla.org/en-US/docs/Web/API/DataTransferItem/webkitGetAsEntry
    const items = e.dataTransfer.items;
    let filteredFiles = [];
    for (let i = 0; i < items.length; i++) {
      if (items[i].webkitGetAsEntry) {
        const entry = items[i].webkitGetAsEntry(); // non-standard
        if (entry && entry.isFile) {
          filteredFiles.push(files[i]);
        }
      }
      else {
        // non-standard webkitGetAsEntry() not available => rely on error handling when trying to read the data
        this.fileOpen(files);
        return;
      }
    }
    this.fileOpen(filteredFiles);
  }

  handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy';
  }

  handleDragEnter(evt) {
    // see https://www.quirksmode.org/blog/archives/2009/09/the_html5_drag.html for why this is necessary
    evt.stopPropagation();
    evt.preventDefault();
  }

  render() {
    const activeDocumentKey = this.state.activeDocumentKey;
    const activeDocument = this.state.documents.find(d => d.key === activeDocumentKey);
    const cursorPosition = activeDocument == null ?
      { address: null, nibble: null } :
      activeDocument.cursorPosition;

    return (
      <div
        onDrop={this.handleFileDropped}
        onDragOver={this.handleDragOver}
        onDragEnter={this.handleDragEnter}
      >
        <Layout >
          <Header>
            <NavBar
              bytesPerRow={this.state.bytesPerRow}
              byteGroupSize={this.state.byteGroupSize}
              theme={this.state.theme}
              handleFileNew={this.fileNew}
              handleFileOpen={this.fileOpen}
              handleFileSave={this.fileSave}
              handleFileClose={this.fileClose}
              handleFileCloseAll={this.fileCloseAll}
              handleSetBytesPerRow={this.setBytesPerRow}
              handleSetByteGroupSize={this.setByteGroupSize}
              handleShowAbout={this.handleShowAbout}
              handleSetTheme={this.setTheme}
              activeDocument={activeDocument}
              handleActiveDocumentUpdated={this.handleActiveDocumentUpdated}
              handleFocusScrollArea={this.handleFocusScrollArea}
            >
            </NavBar>
          </Header>
          <Content
            className="main-area"
          >
            <Row
              type="flex"
            >
              <Col
                className="col-height"
                xs={{ span: 24 }}
                sm={{ span: 12 }}
                md={{ span: 12 }}
                lg={{ span: 13 }}
                xl={{ span: 15 }}
                xxl={{ span: 16 }}
              >
                <EditorPane
                  ref={this.editorPane}
                  bytesPerRow={this.state.bytesPerRow}
                  byteGroupSize={this.state.byteGroupSize}
                  documents={this.state.documents}
                  activeDocumentKey={activeDocumentKey}
                  cursorPosition={cursorPosition}
                  handleActiveDocumentUpdated={this.handleActiveDocumentUpdated}
                  handleFileClose={this.fileClose}
                  handleActiveDocumentChanged={this.handleActiveDocumentChanged}
                  handleDocumentReadError={this.handleDocumentReadError}
                />
              </Col>
              <Col
                className="col-height"
                xs={{ span: 24 }}
                sm={{ span: 12 }}
                md={{ span: 12 }}
                lg={{ span: 11 }}
                xl={{ span: 9 }}
                xxl={{ span: 8 }}
              >
              
                <ValuePane
                  ref={this.valuePane}
                  settingsStorage={this.settingsStorage}
                  file={activeDocument}
                  // use cursor position instead of just the address, so that edits are noticed
                  cursorPosition={cursorPosition}
                />
              </Col>
            </Row>
          </Content>
          <StatusBar
            status={this.state.status}
            cursorPosition={cursorPosition}
          />
        </Layout>
      </div>
    );
  }
}

export default App;
