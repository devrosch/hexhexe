import GoToDialog from './GoToDialog';

let dialog;

beforeEach(() => {
  dialog = new GoToDialog();
});

it('successfully validates absolute decimal addresses', () => {
  const isValid0 = dialog.isValidInput('123');
  const isValid1 = dialog.isValidInput(' 123');
  const isValid2 = dialog.isValidInput('123 ');
  const isValid3 = dialog.isValidInput(' 123 ');
  expect(isValid0).toEqual(true);
  expect(isValid1).toEqual(true);
  expect(isValid2).toEqual(true);
  expect(isValid3).toEqual(true);
});

it('successfully validates relative decimal addresses', () => {
  const isValid0 = dialog.isValidInput('+123');
  const isValid1 = dialog.isValidInput('-123');
  const isValid2 = dialog.isValidInput('+ 123');
  const isValid3 = dialog.isValidInput('- 123');
  const isValid4 = dialog.isValidInput(' + 123 ');
  const isValid5 = dialog.isValidInput(' - 123 ');
  expect(isValid0).toEqual(true);
  expect(isValid1).toEqual(true);
  expect(isValid2).toEqual(true);
  expect(isValid3).toEqual(true);
  expect(isValid4).toEqual(true);
  expect(isValid5).toEqual(true);
});

it('rejects invalid absolute decimal addresses', () => {
  const isValid0 = dialog.isValidInput('a123');
  const isValid1 = dialog.isValidInput('123b');
  const isValid2 = dialog.isValidInput('1.23');
  const isValid3 = dialog.isValidInput('1 23');
  expect(isValid0).toEqual(false);
  expect(isValid1).toEqual(false);
  expect(isValid2).toEqual(false);
  expect(isValid3).toEqual(false);
});

it('rejects invalid relative decimal addresses', () => {
  const isValid0 = dialog.isValidInput('++123');
  const isValid1 = dialog.isValidInput('--123b');
  const isValid2 = dialog.isValidInput('123+');
  const isValid3 = dialog.isValidInput('1-23');
  expect(isValid0).toEqual(false);
  expect(isValid1).toEqual(false);
  expect(isValid2).toEqual(false);
  expect(isValid3).toEqual(false);
});

it('successfully validates absolute hexadecimal addresses', () => {
  const isValid0 = dialog.isValidInput('0xAB');
  const isValid1 = dialog.isValidInput('0Xab');
  const isValid2 = dialog.isValidInput('0x aB');
  const isValid3 = dialog.isValidInput(' 0xAB ');
  const isValid4 = dialog.isValidInput('0x\t12ab');
  expect(isValid0).toEqual(true);
  expect(isValid1).toEqual(true);
  expect(isValid2).toEqual(true);
  expect(isValid3).toEqual(true);
  expect(isValid4).toEqual(true);
});

it('successfully validates relative hexadecimal addresses', () => {
  const isValid0 = dialog.isValidInput('+0xAB');
  const isValid1 = dialog.isValidInput('-0xAB');
  const isValid2 = dialog.isValidInput('+ 0xAB');
  const isValid3 = dialog.isValidInput('- 0xAB');
  const isValid4 = dialog.isValidInput(' - 0xAB ');
  const isValid5 = dialog.isValidInput(' - 0xAB ');
  expect(isValid0).toEqual(true);
  expect(isValid1).toEqual(true);
  expect(isValid2).toEqual(true);
  expect(isValid3).toEqual(true);
  expect(isValid4).toEqual(true);
  expect(isValid5).toEqual(true);
});

it('rejects invalid absolute hexadecimal addresses', () => {
  const isValid0 = dialog.isValidInput('0xFG');
  const isValid1 = dialog.isValidInput('OxAB');
  const isValid2 = dialog.isValidInput('0xA.B');
  const isValid3 = dialog.isValidInput('AB0x');
  const isValid4 = dialog.isValidInput('0 xAB');
  expect(isValid0).toEqual(false);
  expect(isValid1).toEqual(false);
  expect(isValid2).toEqual(false);
  expect(isValid3).toEqual(false);
  expect(isValid4).toEqual(false);
});

it('rejects invalid relative hexadecimal addresses', () => {
  const isValid0 = dialog.isValidInput('++0xFG');
  const isValid1 = dialog.isValidInput('--OxAB');
  const isValid2 = dialog.isValidInput('0x+AB');
  const isValid3 = dialog.isValidInput('0x-AB');
  expect(isValid0).toEqual(false);
  expect(isValid1).toEqual(false);
  expect(isValid2).toEqual(false);
  expect(isValid3).toEqual(false);
});