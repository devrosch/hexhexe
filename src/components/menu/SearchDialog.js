import { Form, Input, Modal, Tooltip, Row, Col, Progress, Button } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import React, { Component } from 'react';
import DocumentSearcher from '../../model/DocumentSearcher';

const hexDigits = '0123456789abcdef';

const addressTooltipText = 'Address at which to start/continue the search. Initialized to current cursor position.';
const bytesTooltipText = 'Bytes to search for in hexadecimal notation.';
const asciiTooltipText = 'ASCII text to search for.';

const invalidActiveDocumentMessage = 'No active document.';

class SearchDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: 0,
      addressValidateStatus: 'success',
      addressErrorMessage: null,
      bytes: '',
      bytesValidateStatus: 'success',
      bytesErrorMessage: null,
      ascii: '',
      asciiValidateStatus: 'success',
      asciiErrorMessage: null,
      showSearchProgress: false,
      searchProgress: 0,
    }

    this._searcher = null;
    this._pastedValue = null;
    this._searchCancelled = false;

    this.bytesRef = React.createRef();
    this.convertSearchStringToByteArray = this.convertSearchStringToByteArray.bind(this);
    this.convertByteArrayToSearchString = this.convertByteArrayToSearchString.bind(this);
    this.convertSearchStringToAscii = this.convertSearchStringToAscii.bind(this);
    this.searchProgressCallback = this.searchProgressCallback.bind(this);
    this.searchCompletedCallback = this.searchCompletedCallback.bind(this);
    this.handleSearchCancelled = this.handleSearchCancelled.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleBytesInput = this.handleBytesInput.bind(this);
    this.handleAsciiInput = this.handleAsciiInput.bind(this);
    this.handleAsciiPaste = this.handleAsciiPaste.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.visible && this.props.visible) {
      const activeDocument = this.props.activeDocument;
      if (!activeDocument) {
        this.setState({
          address: 0,
          addressValidateStatus: 'error',
          addressErrorMessage: invalidActiveDocumentMessage
        });
        return;
      }

      const cursorAddress = activeDocument.cursorPosition &&
        activeDocument.cursorPosition.address ?
        activeDocument.cursorPosition.address :
        0;
      this.setState({
        address: cursorAddress,
        addressValidateStatus: 'success',
        addressErrorMessage: ''
      });

      // this is necessary due to https://github.com/vazco/uniforms/issues/279
      setTimeout(() => {
        if (this.bytesRef.current) {
          this.bytesRef.current.focus();
          this.bytesRef.current.select();
        }
      }, 100);
    }
    if (prevProps.visible && !this.props.visible) {
      this.setState({
        addressValidateStatus: 'success',
        addressErrorMessage: null,
        bytesValidateStatus: 'success',
        bytesErrorMessage: null,
        asciiValidateStatus: 'success',
        asciiErrorMessage: null,
        showSearchProgress: false,
      });
    }
  }

  convertSearchStringToByteArray(bytesString) {
    // turn search string into byte array
    let byteArray = [];
    if (bytesString === undefined || bytesString === null) {
      return byteArray;
    }
    for (let i = 0; i < bytesString.length; i += 2) {
      const hi = hexDigits.indexOf(bytesString[i].toLowerCase());
      const lo = hexDigits.indexOf(bytesString[i + 1].toLowerCase());
      const value = hi * 16 + lo;
      byteArray.push(value);
    }
    return byteArray;
  }

  convertByteArrayToSearchString(byteArray) {
    let byteString = '';
    for (let b of byteArray) {
      const byte = b.toString(16).padStart(2, '0');
      byteString += byte;
    }
    return byteString;
  }

  convertSearchStringToAscii(byteString) {
    let ascii = '';
    for (let i = 0; i < byteString.length; i += 2) {
      const upperNibbleChar = byteString[i].toLowerCase();
      const lowerNibbleChar = i + 1 < byteString.length ? byteString[i + 1].toLowerCase() : '0';
      const upperNibble = hexDigits.indexOf(upperNibbleChar);
      const lowerNibble = hexDigits.indexOf(lowerNibbleChar);
      const charCode = upperNibble * 16 + lowerNibble;
      const asciiChar = charCode < 32 || charCode > 126 ? '.' : String.fromCharCode(charCode);
      ascii += asciiChar;
    }
    return ascii;
  }

  searchProgressCallback(progress) {
    console.log('Search progress: ' + progress);
    this.setState({ searchProgress: Math.round(100 * progress) });
  }

  searchCompletedCallback(hitAddress) {
    console.log('Search hit: ' + hitAddress);
    this.setState({ showSearchProgress: false, });
    const activeDocument = this.props.activeDocument;
    const bytes = this.convertSearchStringToByteArray(this.state.bytes);
    if (hitAddress < 0) {
      if (!this._searchCancelled) {
        this.setState({
          bytesErrorMessage: 'Not found.',
          bytesValidateStatus: 'error',
        });
      }
      activeDocument.markedRange = { start: 0, end: 0 };
      this.props.handleActiveDocumentUpdated();
      this.bytesRef.current.focus();
      this.bytesRef.current.select();
      return;
    }
    // mark byte sequence in document
    activeDocument.markedRange = { start: hitAddress, end: hitAddress + bytes.length };
    activeDocument.cursorPosition = { address: hitAddress, nibble: 1, mode: activeDocument.cursorPosition.mode };
    this.props.handleActiveDocumentUpdated();
    if (hitAddress > 0) {
      this.props.handleCloseDialog();
    }
  }

  handleSearchCancelled() {
    this._searchCancelled = true;
    this.setState({
      showSearchProgress: false
    });
    if (this._searcher) {
      this._searcher.cancelSearch();
    }
  }

  handleOk(e) {
    if (!this.props.activeDocument) {
      // no active document => don't do anything
      return;
    }
    if (this.state.showSearchProgress) {
      // search ongoing => do nothing
      return;
    }
    this._searchCancelled = false;
    // turn search string into byte array
    const bytes = this.convertSearchStringToByteArray(this.state.bytes);
    if (bytes === undefined || bytes === null || bytes.length <= 0) {
      // no search value defined => don't do anything
      this.setState({
        bytesErrorMessage: 'No search data specified.',
        bytesValidateStatus: 'error',
      });
      return;
    }
    // show search progress dialog
    this.setState({
      searchProgress: 0,
      showSearchProgress: true
    });
    // search
    const startAddress = parseInt(this.state.address, 10);
    if (this._searcher) {
      this._searcher.cancelSearch();
      this._searcher = null;
    }
    this._searcher = new DocumentSearcher(
      this.props.activeDocument,
      this.searchCompletedCallback,
      this.searchProgressCallback);
    this._searcher.searchDocument(startAddress, bytes);
  }

  handleCancel(e) {
    this.handleSearchCancelled();
    this.props.handleCloseDialog();
    this.setState({
      addressValidateStatus: 'success',
      addressErrorMessage: null,
      numBytesValidateStatus: 'success',
      numBytesErrorMessage: null,
      byteValueValidateStatus: 'success',
      byteValueErrorMessage: null,
    });
  }

  handleBytesInput(e) {
    let selectionStart = e.target.selectionStart;
    let selectionEnd = e.target.selectionEnd;
    let value = e.target.value;

    for (const c of value) {
      if (!hexDigits.includes(c.toLowerCase())) {
        value = this.state.bytes;
        selectionStart = selectionStart - 1;
        selectionEnd = selectionEnd - 1;
        break;
      }
    }

    if (value.length % 2 !== 0) {
      value = value.substring(0, selectionStart) + '0' + value.substring(selectionStart);
      selectionEnd = selectionEnd + 1;
    }

    const ascii = this.convertSearchStringToAscii(value);

    this.setState(
      {
        bytes: value.toUpperCase(),
        ascii: ascii
      },
      // see https://github.com/facebook/react/issues/955
      () => {
        this.bytesRef.current.setSelectionRange(selectionStart, selectionEnd);
      }
    );
  }

  handleAsciiInput(e) {
    const selectionStart = e.target.selectionStart;
    const value = e.target.value;
    const pastedData = this._pastedValue;
    this._pastedValue = null;

    // determine inserted data and start of insertion
    let insertedData = pastedData;
    if (pastedData === null && value.length > 0) {
      // single character inserted
      insertedData = value[selectionStart - 1];
    }
    if (insertedData === null) {
      insertedData = '';
    }
    const startIndex = selectionStart > 0 ? selectionStart - insertedData.length : 0;

    // determine new byte array from ASCII input
    let previousBytes = this.convertSearchStringToByteArray(this.state.bytes);
    let newBytes = previousBytes.slice(0, startIndex);
    for (let i = 0; i < insertedData.length; i++) {
      const c = insertedData.charCodeAt(i);
      if (c >= 32 || c <= 126) {
        // ASCII char
        newBytes.push(c);
      }
      // ignore non ASCII chars
    }
    const numTrailingBytes = value.length - newBytes.length;
    if (numTrailingBytes > 0) {
      newBytes.push(...previousBytes.slice(-numTrailingBytes));
    }

    // update state
    const byteString = this.convertByteArrayToSearchString(newBytes);
    const ascii = this.convertSearchStringToAscii(byteString);
    this.setState(
      {
        bytes: byteString.toUpperCase(),
        ascii: ascii
      },
    );
  }

  handleAsciiPaste(e) {
    const clipboardData = e.clipboardData;
    const pasteData = clipboardData.getData('text');
    this._pastedValue = pasteData;
  }

  render() {
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 18 },
    };

    return (
      <div>
        <Modal
          title="Search"
          open={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          destroyOnClose={true} // avoid flashing
        >
          <Form>
            <Form.Item
              {...formItemLayout}
              label="Address"
              validateStatus={this.state.addressValidateStatus}
              help={this.state.addressErrorMessage}
            >
              <Tooltip
                trigger={['hover']}
                title={addressTooltipText}
                placement="topLeft"
              >
                <Input
                  value={this.state.address}
                  onPressEnter={this.handleOk}
                  readOnly={true}
                />
              </Tooltip>
            </Form.Item>

            <Form.Item
              {...formItemLayout}
              label="Bytes"
              validateStatus={this.state.bytesValidateStatus}
              help={this.state.bytesErrorMessage}
            >
              <Tooltip
                trigger={['hover']}
                title={bytesTooltipText}
                placement="topLeft"
              >
                <input
                  ref={this.bytesRef}
                  placeholder={'bytes to search in hexadecimal notation'}
                  value={this.state.bytes}
                  onChange={this.handleBytesInput}
                  onKeyDown={(e) => {
                    // cannot use onPressEnter here as this is a regular input, because ref to DOM node is needed
                    if (e.key === 'Enter') { this.handleOk() }
                  }}
                  className="ant-input"
                  readOnly={this.state.showSearchProgress}
                />
              </Tooltip>
            </Form.Item>

            <Form.Item
              {...formItemLayout}
              label="ASCII"
              validateStatus={this.state.asciiValidateStatus}
              help={this.state.asciiErrorMessage}
            >
              <Tooltip
                trigger={['hover']}
                title={asciiTooltipText}
                placement="topLeft"
              >
                <Input
                  placeholder={'ASCII string to search'}
                  value={this.state.ascii}
                  onChange={this.handleAsciiInput}
                  onPaste={this.handleAsciiPaste}
                  onPressEnter={this.handleOk}
                  readOnly={this.state.showSearchProgress}
                />
              </Tooltip>
            </Form.Item>

            {this.state.showSearchProgress ?
              <Form.Item
                {...formItemLayout}
                label="Progress"
              >
                <Row gutter={8} align="middle">
                  <Col flex="auto">
                    <Progress percent={this.state.searchProgress} />
                  </Col>
                  <Col flex="none">
                    <Tooltip title="Cancel search">
                      <Button
                        name="cancelSearch"
                        icon={<CloseCircleOutlined />}
                        onClick={this.handleSearchCancelled}
                      />
                    </Tooltip>
                  </Col>
                </Row>
              </Form.Item> :
              null
            }
          </Form>

        </Modal>

      </div>
    );
  }
}

export default SearchDialog;