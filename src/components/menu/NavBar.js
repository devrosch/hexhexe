import React, { PureComponent } from 'react';
import { Drawer } from 'antd';
import AppMenu from './AppMenu';
import './NavBar.css';
import icon from '../../assets/favicon.jsx.svg';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';

class NavBar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      drawerVisible: false
    }

    // ref for reading data from file input
    this.fileInput = React.createRef();
    // ref for closing data in file input
    this.fileOpenForm = React.createRef()
    // toggle drawer
    this.showDrawer = this.showDrawer.bind(this);
  }

  showDrawer() {
    this.setState((state) => {
      return { drawerVisible: !state.drawerVisible };
    });
  }

  render() {
    const title = <div>
      <img src={icon} className="navbar-logo" alt="HexHexe Logo" />
      HexHexe Menu
    </div>

    const horizontalMenu = <AppMenu
      mode="horizontal"
      bytesPerRow={this.props.bytesPerRow}
      byteGroupSize={this.props.byteGroupSize}
      endianness={this.props.endianness}
      theme={this.props.theme}
      drawerVisible={this.state.drawerVisible}
      handleFileNew={this.props.handleFileNew}
      handleFileOpen={this.props.handleFileOpen}
      handleFileSave={this.props.handleFileSave}
      handleFileClose={this.props.handleFileClose}
      handleFileCloseAll={this.props.handleFileCloseAll}
      handleSetBytesPerRow={this.props.handleSetBytesPerRow}
      handleSetByteGroupSize={this.props.handleSetByteGroupSize}
      handleSetEndianness={this.props.handleSetEndianness}
      handleShowAbout={this.props.handleShowAbout}
      handleSetTheme={this.props.handleSetTheme}
      handleShowDrawer={this.showDrawer}
      activeDocument={this.props.activeDocument}
      handleActiveDocumentUpdated={this.props.handleActiveDocumentUpdated}
      handleFocusScrollArea={this.props.handleFocusScrollArea}

    >
    </AppMenu>

    const verticalMenu = <AppMenu
      mode="inline"
      bytesPerRow={this.props.bytesPerRow}
      byteGroupSize={this.props.byteGroupSize}
      endianness={this.props.endianness}
      theme={this.props.theme}
      drawerVisible={this.state.drawerVisible}
      handleFileNew={this.props.handleFileNew}
      handleFileOpen={this.props.handleFileOpen}
      handleFileSave={this.props.handleFileSave}
      handleFileClose={this.props.handleFileClose}
      handleFileCloseAll={this.props.handleFileCloseAll}
      handleSetBytesPerRow={this.props.handleSetBytesPerRow}
      handleSetByteGroupSize={this.props.handleSetByteGroupSize}
      handleSetEndianness={this.props.handleSetEndianness}
      handleShowAbout={this.props.handleShowAbout}
      handleSetTheme={this.props.handleSetTheme}
      handleShowDrawer={this.showDrawer}
      activeDocument={this.props.activeDocument}
      handleActiveDocumentUpdated={this.props.handleActiveDocumentUpdated}
      handleFocusScrollArea={this.props.handleFocusScrollArea}
    />

    const hamburgerMenu = <div
      key="hamburgerMenuItem"
      className="hamburger-menu"
      onClick={this.showDrawer}
    >
      {this.state.drawerVisible ?
        <MenuUnfoldOutlined className="hamburger-menu-icon" /> :
        <MenuFoldOutlined className="hamburger-menu-icon" />}
    </div>

    return (
      <div className='navbar'>
        <div className="navbar-logo-container">
          <img src={icon} className="navbar-logo" alt="HexHexe Logo" />
          <span>HexHexe</span>
        </div>
        <div className='navbar-menu'>
          {horizontalMenu}
        </div>
        {hamburgerMenu}
        <Drawer
          width={'70%'}
          title={title}
          placement="left"
          closable={false}
          onClose={this.showDrawer}
          open={this.state.drawerVisible}
        >
          {verticalMenu}
        </Drawer>
      </div>
    )
  }

}

export default NavBar;
