import React, { Component } from 'react';
import { Modal, Input, Tooltip, Form } from 'antd';

const numBytesTooltipText = 'Initial length of the new document. Prefix with "0x" for hexadecimal number.';
const byteValueTooltipText = 'Initial value for bytes. Must be in the range 0 - 255. Prefix with "0x" for hexadecimal value.';

const invalidNumBytesErrorMessage = 'Must be a positive integer number.';
const invalidByteValueErrorMessage = 'Must be a positive integer number between 0 and 255.';

class NewFileDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numBytes: '1024',
      numBytesValidateStatus: 'success',
      numBytesErrorMessage: null,
      byteValue: '0',
      byteValueValidateStatus: 'success',
      byteValueErrorMessage: null,
    }
    this.numBytesRef = React.createRef();
    this.parseNumBytesInput = this.parseNumBytesInput.bind(this);
    this.parseByteValueInput = this.parseByteValueInput.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleNumBytesInput = this.handleNumBytesInput.bind(this);
    this.handleByteValueInput = this.handleByteValueInput.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.visible && this.props.visible) {
      // this is necessary due to https://github.com/vazco/uniforms/issues/279
      setTimeout(() => {
        if (this.numBytesRef.current) {
          this.numBytesRef.current.focus();
          this.numBytesRef.current.select();
        }
      }, 100);
    }
    if (prevProps.visible && !this.props.visible) {
      this.setState({
        numBytesValidateStatus: 'success',
        numBytesErrorMessage: null,
        byteValueValidateStatus: 'success',
        byteValueErrorMessage: null,
      });
    }
  }

  parseNumBytesInput(rawInput) {
    const isValidDecimalNumber = /^\s*[+]?\s*\d+\s*$/.test(rawInput);
    const isValidHexadecimalNumber = /^\s*[+-]?\s*(0x|0X)\s*[\da-fA-F]+\s*$/.test(rawInput);
    let value = null;
    const normalizedInput = this.state.addressInput === null ?
      '' : rawInput.replace(/\s/g, '');
    if (isValidDecimalNumber) {
      value = parseInt(normalizedInput, 10);
    }
    else if (isValidHexadecimalNumber) {
      value = parseInt(normalizedInput, 16);
    }
    return isNaN(value) || value === null || value < 0 ? null : value;
  }

  parseByteValueInput(rawInput) {
    const isValidDecimalNumber = /^\s*[+]?\s*\d+\s*$/.test(rawInput);
    const isValidHexadecimalNumber = /^\s*[+]?\s*(0x|0X)\s*[\da-fA-F]+\s*$/.test(rawInput);
    let value = null;
    const normalizedInput = this.state.addressInput === null ?
      '' : rawInput.replace(/\s/g, '');
    if (isValidDecimalNumber) {
      value = parseInt(normalizedInput, 10);
    }
    else if (isValidHexadecimalNumber) {
      value = parseInt(normalizedInput, 16);
    }
    if (value !== null
      && !isNaN(value)
      && value >= 0
      && value <= 255) {
      return value;
    }
    return null;
  }

  handleOk(e) {
    // validate input
    const numBytes = this.parseNumBytesInput(this.state.numBytes);
    const byteValue = this.parseByteValueInput(this.state.byteValue);
    if (numBytes === null) {
      this.setState({
        numBytesValidateStatus: 'error',
        numBytesErrorMessage: invalidNumBytesErrorMessage
      });
    }
    if (byteValue === null) {
      this.setState({
        byteValueValidateStatus: 'error',
        byteValueErrorMessage: invalidByteValueErrorMessage
      });
    }
    if (numBytes === null || byteValue === null) {
      return;
    }

    this.props.handleFileNew(numBytes, byteValue);
    this.props.handleCloseDialog();
  }

  handleCancel(e) {
    this.props.handleCloseDialog();
    this.setState({
      numBytesValidateStatus: 'success',
      numBytesErrorMessage: null,
      byteValueValidateStatus: 'success',
      byteValueErrorMessage: null,
    });
  }

  handleNumBytesInput(e) {
    const value = e.target.value;
    this.setState({ numBytes: value });
  }

  handleByteValueInput(e) {
    const value = e.target.value;
    this.setState({ byteValue: value });
  }

  render() {
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 18 },
    };

    return (
      <Modal
        title="New File"
        open={this.props.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        destroyOnClose={true} // avoid flashing
      >
        <Form>
          <Form.Item
            {...formItemLayout}
            label="Number Of Bytes"
            validateStatus={this.state.numBytesValidateStatus}
            help={this.state.numBytesErrorMessage}
          >
            <Tooltip
              trigger={['hover']}
              title={numBytesTooltipText}
              placement="topLeft"
            >
              <Input
                ref={this.numBytesRef}
                placeholder={'e.g. "255" or "0xFF"'}
                value={this.state.numBytes}
                onChange={this.handleNumBytesInput}
                onPressEnter={this.handleOk}
              />
            </Tooltip>
          </Form.Item>

          <Form.Item
            {...formItemLayout}
            label="Initial Value"
            validateStatus={this.state.byteValueValidateStatus}
            help={this.state.byteValueErrorMessage}
          >
            <Tooltip
              trigger={['hover']}
              title={byteValueTooltipText}
              placement="topLeft"
            >
              <Input
                placeholder={'initial value (0 - 255) for bytes'}
                value={this.state.byteValue}
                onChange={this.handleByteValueInput}
                onPressEnter={this.handleOk}
              />
            </Tooltip>
          </Form.Item>

        </Form>
      </Modal>
    );
  }
}

export default NewFileDialog;