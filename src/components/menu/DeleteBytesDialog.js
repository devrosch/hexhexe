import React, { Component } from 'react';
import { Modal, Input, Tooltip, Form } from 'antd';

const addressTooltipText = 'Address at which to insert bytes (readonly). Initialized to current cursor position.';
const numBytesTooltipText = 'Number of bytes to insert. Prefix with "0x" for hexadecimal number.';

const invalidActiveDocumentMessage = 'No active document.';
const invalidNumBytesErrorMessage = 'Must be a positive integer number.';

class DeleteBytesDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: 0,
      addressValidateStatus: 'success',
      addressErrorMessage: null,
      numBytes: '1',
      numBytesValidateStatus: 'success',
      numBytesErrorMessage: null,
    }
    this.numBytesRef = React.createRef();
    this.parseNumBytesInput = this.parseNumBytesInput.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleNumBytesInput = this.handleNumBytesInput.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.visible && this.props.visible) {
      const activeDocument = this.props.activeDocument;
      if (!activeDocument) {
        this.setState({
          address: 0,
          addressValidateStatus: 'error',
          addressErrorMessage: invalidActiveDocumentMessage
        });
        return;
      }

      const cursorAddress = activeDocument.cursorPosition &&
        activeDocument.cursorPosition.address ?
        activeDocument.cursorPosition.address :
        0;
      this.setState({
        address: cursorAddress,
        addressValidateStatus: 'success',
        addressErrorMessage: ''
      });

      // this is necessary due to https://github.com/vazco/uniforms/issues/279
      setTimeout(() => {
        if (this.numBytesRef.current) {
          this.numBytesRef.current.focus();
          this.numBytesRef.current.select();
        }
      }, 100);
    }
    if (prevProps.visible && !this.props.visible) {
      this.setState({
        addressValidateStatus: 'success',
        addressErrorMessage: null,
        numBytesValidateStatus: 'success',
        numBytesErrorMessage: null,
      });
    }
  }

  parseNumBytesInput(rawInput) {
    const isValidDecimalNumber = /^\s*[+]?\s*\d+\s*$/.test(rawInput);
    const isValidHexadecimalNumber = /^\s*[+-]?\s*(0x|0X)\s*[\da-fA-F]+\s*$/.test(rawInput);
    let value = null;
    const normalizedInput = this.state.addressInput === null ?
      '' : rawInput.replace(/\s/g, '');
    if (isValidDecimalNumber) {
      value = parseInt(normalizedInput, 10);
    }
    else if (isValidHexadecimalNumber) {
      value = parseInt(normalizedInput, 16);
    }
    return isNaN(value) || value === null || value < 0 ? null : value;
  }

  handleOk(e) {
    // validate input
    const activeDocument = this.props.activeDocument;
    const numBytes = this.parseNumBytesInput(this.state.numBytes);
    if (!activeDocument) {
      this.setState({
        addressValidateStatus: 'error',
        addressErrorMessage: invalidActiveDocumentMessage
      });
    }
    if (numBytes === null) {
      this.setState({
        numBytesValidateStatus: 'error',
        numBytesErrorMessage: invalidNumBytesErrorMessage
      });
    }
    if (!activeDocument || numBytes === null) {
      return;
    }

    // delete bytes
    const address = this.state.address;
    activeDocument.deleteBytes(address, numBytes);
    this.props.handleActiveDocumentUpdated();
    this.props.handleCloseDialog();
  }

  handleCancel(e) {
    this.props.handleCloseDialog();
    this.setState({
      addressValidateStatus: 'success',
      addressErrorMessage: null,
      numBytesValidateStatus: 'success',
      numBytesErrorMessage: null,
    });
  }

  handleNumBytesInput(e) {
    const value = e.target.value;
    this.setState({ numBytes: value });
  }

  render() {
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 18 },
    };

    return (
      <Modal
        title="Delete Bytes"
        open={this.props.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        destroyOnClose={true} // avoid flashing
      >
        <Form>

          <Form.Item
            {...formItemLayout}
            label="Address"
            validateStatus={this.state.addressValidateStatus}
            help={this.state.addressErrorMessage}
          >
            <Tooltip
              trigger={['hover']}
              title={addressTooltipText}
              placement="topLeft"
            >
              <Input
                value={this.state.address}
                onPressEnter={this.handleOk}
                readOnly={true}
              />
            </Tooltip>
          </Form.Item>

          <Form.Item
            {...formItemLayout}
            label="Number Of Bytes"
            validateStatus={this.state.numBytesValidateStatus}
            help={this.state.numBytesErrorMessage}
          >
            <Tooltip
              trigger={['hover']}
              title={numBytesTooltipText}
              placement="topLeft"
            >
              <Input
                ref={this.numBytesRef}
                placeholder={'number of bytes to delete, e.g. "255" or "0xFF"'}
                value={this.state.numBytes}
                onChange={this.handleNumBytesInput}
                onPressEnter={this.handleOk}
              />
            </Tooltip>
          </Form.Item>

        </Form>
      </Modal>
    );
  }
}

export default DeleteBytesDialog;