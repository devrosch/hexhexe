import InsertBytesDialog from './InsertBytesDialog';

let dialog;

beforeEach(() => {
  dialog = new InsertBytesDialog();
});

it('successfully parses decimal number of bytes', () => {
  const isValid0 = dialog.parseNumBytesInput('123');
  const isValid1 = dialog.parseNumBytesInput(' 123');
  const isValid2 = dialog.parseNumBytesInput('123 ');
  const isValid3 = dialog.parseNumBytesInput(' 123 ');
  const isValid4 = dialog.parseNumBytesInput('+123');
  const isValid5 = dialog.parseNumBytesInput(' + 123');
  expect(isValid0).toEqual(123);
  expect(isValid1).toEqual(123);
  expect(isValid2).toEqual(123);
  expect(isValid3).toEqual(123);
  expect(isValid4).toEqual(123);
  expect(isValid5).toEqual(123);
});

it('rejects invalid decimal number of bytes', () => {
  const isValid0 = dialog.parseNumBytesInput('a123');
  const isValid1 = dialog.parseNumBytesInput('123b');
  const isValid2 = dialog.parseNumBytesInput('1.23');
  const isValid3 = dialog.parseNumBytesInput('1 23');
  const isValid4 = dialog.parseNumBytesInput('-123');
  expect(isValid0).toEqual(null);
  expect(isValid1).toEqual(null);
  expect(isValid2).toEqual(null);
  expect(isValid3).toEqual(null);
  expect(isValid4).toEqual(null);
});

it('successfully parses decimal byte value', () => {
  const isValid0 = dialog.parseByteValueInput('123');
  const isValid1 = dialog.parseByteValueInput(' 123');
  const isValid2 = dialog.parseByteValueInput('123 ');
  const isValid3 = dialog.parseByteValueInput(' 123 ');
  const isValid4 = dialog.parseByteValueInput('+123');
  const isValid5 = dialog.parseByteValueInput(' + 123');
  const isValid6 = dialog.parseByteValueInput('0');
  const isValid7 = dialog.parseByteValueInput('255');
  expect(isValid0).toEqual(123);
  expect(isValid1).toEqual(123);
  expect(isValid2).toEqual(123);
  expect(isValid3).toEqual(123);
  expect(isValid4).toEqual(123);
  expect(isValid5).toEqual(123);
  expect(isValid6).toEqual(0);
  expect(isValid7).toEqual(255);
});

it('rejects invalid decimal byte values', () => {
  const isValid0 = dialog.parseNumBytesInput('a123');
  const isValid1 = dialog.parseNumBytesInput('123b');
  const isValid2 = dialog.parseNumBytesInput('1.23');
  const isValid3 = dialog.parseNumBytesInput('1 23');
  const isValid4 = dialog.parseNumBytesInput('-123');
  const isValid5 = dialog.parseNumBytesInput('-1');
  const isValid6 = dialog.parseNumBytesInput('-256');
  expect(isValid0).toEqual(null);
  expect(isValid1).toEqual(null);
  expect(isValid2).toEqual(null);
  expect(isValid3).toEqual(null);
  expect(isValid4).toEqual(null);
  expect(isValid5).toEqual(null);
  expect(isValid6).toEqual(null);
});

it('successfully parses hexadecimal number of bytes', () => {
  const isValid0 = dialog.parseNumBytesInput('0xAB');
  const isValid1 = dialog.parseNumBytesInput('0Xab');
  const isValid2 = dialog.parseNumBytesInput('0x aB');
  const isValid3 = dialog.parseNumBytesInput(' 0xAB ');
  const isValid4 = dialog.parseNumBytesInput('0x\t12ab');
  const isValid5 = dialog.parseNumBytesInput(' 0x AB ');
  expect(isValid0).toEqual(0xAB);
  expect(isValid1).toEqual(0xAB);
  expect(isValid2).toEqual(0xAB);
  expect(isValid3).toEqual(0xAB);
  expect(isValid4).toEqual(0x12AB);
  expect(isValid5).toEqual(0xAB);
});

it('rejects invalid hexadecimal number of bytes', () => {
  const isValid0 = dialog.parseNumBytesInput('0xFG');
  const isValid1 = dialog.parseNumBytesInput('OxAB');
  const isValid2 = dialog.parseNumBytesInput('0xA.B');
  const isValid3 = dialog.parseNumBytesInput('AB0x');
  const isValid4 = dialog.parseNumBytesInput('0 xAB');
  expect(isValid0).toEqual(null);
  expect(isValid1).toEqual(null);
  expect(isValid2).toEqual(null);
  expect(isValid3).toEqual(null);
  expect(isValid4).toEqual(null);
});

it('successfully parses hexadecimal byte values', () => {
  const isValid0 = dialog.parseByteValueInput('0xAB');
  const isValid1 = dialog.parseByteValueInput('0Xab');
  const isValid2 = dialog.parseByteValueInput('0x aB');
  const isValid3 = dialog.parseByteValueInput(' 0xAB ');
  const isValid4 = dialog.parseByteValueInput(' 0x AB ');
  const isValid5 = dialog.parseByteValueInput('0x0');
  const isValid6 = dialog.parseByteValueInput('0xFF');
  expect(isValid0).toEqual(0xAB);
  expect(isValid1).toEqual(0xAB);
  expect(isValid2).toEqual(0xAB);
  expect(isValid3).toEqual(0xAB);
  expect(isValid4).toEqual(0xAB);
  expect(isValid5).toEqual(0x0);
  expect(isValid6).toEqual(0xFF);
});

it('rejects invalid hexadecimal byte values', () => {
  const isValid0 = dialog.parseByteValueInput('0xFG');
  const isValid1 = dialog.parseByteValueInput('OxAB');
  const isValid2 = dialog.parseByteValueInput('0xA.B');
  const isValid3 = dialog.parseByteValueInput('AB0x');
  const isValid4 = dialog.parseByteValueInput('0 xAB');
  const isValid5 = dialog.parseByteValueInput('-0x01');
  const isValid6 = dialog.parseByteValueInput('0x100');
  expect(isValid0).toEqual(null);
  expect(isValid1).toEqual(null);
  expect(isValid2).toEqual(null);
  expect(isValid3).toEqual(null);
  expect(isValid4).toEqual(null);
  expect(isValid5).toEqual(null);
  expect(isValid6).toEqual(null);
});
