import React, { Component } from 'react';
import { Modal, Input, Tooltip, Form } from 'antd';

const tooltipText = 'Address to move cursor to. Prefix with "0x" for hexadecimal address. Use + or - at the beginning for addresses relative to current cursor position.';
const invalidInputErrorMessage = <div>
  Invalid input! Valid input:
  <ul>
    <li>A decimal number, e.g. "123".</li>
    <li>A hexadecimal number, prefixed with "0x", e.g. "0xAB".</li>
    <li>"+" or "-" as prefix for relative address, e.g "-8" or "+0x10".</li>
  </ul>
</div>;

class GoToDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validateStatus: 'success',
      errorMessage: null,
    }
    this.inputRef = React.createRef();
    this.isValidInput = this.isValidInput.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.visible && this.props.visible) {
      // this is necessary due to https://github.com/vazco/uniforms/issues/279
      setTimeout(() => {
        if (this.inputRef.current) {
          this.inputRef.current.focus();
          this.inputRef.current.select();
        }
      }, 100);
    }
    if (prevProps.visible && !this.props.visible) {
      this.setState({ validateStatus: 'success', errorMessage: null });
    }
  }

  isValidInput(rawInput) {
    const isValidDecimalAddress = /^\s*[+-]?\s*\d+\s*$/.test(rawInput);
    const isValidHexadecimalAddress = /^\s*[+-]?\s*(0x|0X)\s*[\da-fA-F]+\s*$/.test(rawInput);
    return isValidDecimalAddress || isValidHexadecimalAddress;
  }

  handleOk(e) {
    const activeDocument = this.props.activeDocument;
    if (!activeDocument) {
      this.setState({
        validateStatus: 'error',
        errorMessage: 'No active document.'
      });
      return;
    }

    const isValidInput = this.isValidInput(this.state.addressInput);
    if (!isValidInput) {
      this.setState({
        validateStatus: 'error',
        errorMessage: invalidInputErrorMessage
      });
      return;
    }

    const normalizedInput = this.state.addressInput === null ?
      '' :
      this.state.addressInput.replace(/\s/g, '');
    const isRelativeAddress = /^[+-]/.test(normalizedInput);
    const sign = isRelativeAddress ? normalizedInput.substring(0, 1) : null;
    const addressInput = isRelativeAddress ? normalizedInput.substring(1) : normalizedInput;
    const isHexadecimal = addressInput.startsWith('0x') || addressInput.startsWith('0X');
    const rawAddressInput = isHexadecimal ? addressInput.substring(2) : addressInput;

    let address = parseInt(rawAddressInput, isHexadecimal ? 16 : 10);
    if (isRelativeAddress) {
      const initialAddress =
        activeDocument.cursorPosition.address ?
          activeDocument.cursorPosition.address :
          0;
      address = sign === '+' ?
        initialAddress + address :
        initialAddress - address;
    }

    if (address < 0) {
      this.setState({
        validateStatus: 'error',
        errorMessage: 'New address negative.'
      });
      return;
    }
    if (address > activeDocument.size) {
      this.setState({
        validateStatus: 'error',
        errorMessage: 'New address past end of document.'
      });
      return;
    }

    activeDocument.cursorPosition = {
      address: address,
      nibble: 1,
      mode: 'bytes'
    };
    this.props.handleActiveDocumentUpdated();
    this.props.handleCloseDialog();
  }

  handleCancel(e) {
    this.props.handleCloseDialog();
    this.setState({ validateStatus: 'success', errorMessage: null });
  }

  handleInput(e) {
    const value = e.target.value;
    this.setState({ addressInput: value });
  }

  render() {
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 20 },
    };

    return (
      <Modal
        title="Go To Byte"
        open={this.props.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        destroyOnClose={true} // avoid flashing
      >
        <Form>
          <Form.Item
            {...formItemLayout}
            label="Address"
            validateStatus={this.state.validateStatus}
            help={this.state.errorMessage}
          >
            <Tooltip
              trigger={['hover']}
              title={tooltipText}
              placement="topLeft"
            >
              <Input
                ref={this.inputRef}
                placeholder={'address, e.g. "255" or "0xFF"'}
                value={this.state.addressInput}
                onChange={this.handleInput}
                onPressEnter={this.handleOk}
              />
            </Tooltip>
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default GoToDialog;