import DeleteBytesDialog from './DeleteBytesDialog';

let dialog;

beforeEach(() => {
  dialog = new DeleteBytesDialog();
});

it('successfully parses decimal number of bytes', () => {
  const isValid0 = dialog.parseNumBytesInput('123');
  const isValid1 = dialog.parseNumBytesInput(' 123');
  const isValid2 = dialog.parseNumBytesInput('123 ');
  const isValid3 = dialog.parseNumBytesInput(' 123 ');
  const isValid4 = dialog.parseNumBytesInput('+123');
  const isValid5 = dialog.parseNumBytesInput(' + 123');
  expect(isValid0).toEqual(123);
  expect(isValid1).toEqual(123);
  expect(isValid2).toEqual(123);
  expect(isValid3).toEqual(123);
  expect(isValid4).toEqual(123);
  expect(isValid5).toEqual(123);
});

it('rejects invalid decimal number of bytes', () => {
  const isValid0 = dialog.parseNumBytesInput('a123');
  const isValid1 = dialog.parseNumBytesInput('123b');
  const isValid2 = dialog.parseNumBytesInput('1.23');
  const isValid3 = dialog.parseNumBytesInput('1 23');
  const isValid4 = dialog.parseNumBytesInput('-123');
  expect(isValid0).toEqual(null);
  expect(isValid1).toEqual(null);
  expect(isValid2).toEqual(null);
  expect(isValid3).toEqual(null);
  expect(isValid4).toEqual(null);
});

it('successfully parses hexadecimal number of bytes', () => {
  const isValid0 = dialog.parseNumBytesInput('0xAB');
  const isValid1 = dialog.parseNumBytesInput('0Xab');
  const isValid2 = dialog.parseNumBytesInput('0x aB');
  const isValid3 = dialog.parseNumBytesInput(' 0xAB ');
  const isValid4 = dialog.parseNumBytesInput('0x\t12ab');
  const isValid5 = dialog.parseNumBytesInput(' 0x AB ');
  expect(isValid0).toEqual(0xAB);
  expect(isValid1).toEqual(0xAB);
  expect(isValid2).toEqual(0xAB);
  expect(isValid3).toEqual(0xAB);
  expect(isValid4).toEqual(0x12AB);
  expect(isValid5).toEqual(0xAB);
});

it('rejects invalid hexadecimal number of bytes', () => {
  const isValid0 = dialog.parseNumBytesInput('0xFG');
  const isValid1 = dialog.parseNumBytesInput('OxAB');
  const isValid2 = dialog.parseNumBytesInput('0xA.B');
  const isValid3 = dialog.parseNumBytesInput('AB0x');
  const isValid4 = dialog.parseNumBytesInput('0 xAB');
  expect(isValid0).toEqual(null);
  expect(isValid1).toEqual(null);
  expect(isValid2).toEqual(null);
  expect(isValid3).toEqual(null);
  expect(isValid4).toEqual(null);
});
