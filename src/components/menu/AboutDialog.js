import React, { Component } from 'react';
import { Modal } from 'antd';
import './AboutDialog.css';
import icon from '../../assets/favicon.jsx.svg';

class AboutDialog extends Component {
  render() {
    return (
      <Modal
        open={this.props.visible}
        title={
          <div className="about-logo-container">
            <img src={icon} className="about-logo" alt="HexHexe Logo" />
            <div>
              <span>HexHexe</span>
              <span style={{ fontSize: '0.5em' }}>{' v' + process.env.REACT_APP_VERSION}</span>
            </div>
          </div>
        }
        centered
        onCancel={this.props.handleCloseDialog}
        footer={null}
        destroyOnClose={true} // avoid flashing
      >
        <div>
          <p>HexHexe is a hex editor that runs in the browser. All data processing happens in the browser.</p>
          <p>©2018-2020, 2022 Robert Schiwon</p>
          <p>It has been tested for compatibility with recent versions of Firefox and Chrome.</p>
          <p>HexHexe is free software according to the terms of the GNU General Public License Version 3 (license: <a
            href="https://gitlab.com/devrosch/hexhexe/blob/master/COPYING">GPL</a>
            , source code: <a
              href="https://gitlab.com/devrosch/hexhexe">
              GitLab
            </a>) and makes use of the following third-party packages that come with their respective license terms:</p>
          <ul>
            <li>
              <a
                href="https://www.npmjs.com/package/react">
                react
              </a> (license: <a
                href="https://github.com/facebook/react/blob/master/LICENSE">MIT</a>
              , source code: <a
                href="https://github.com/facebook/react">
                GitHub
              </a>)
            </li>
            <li>
              <a
                href="https://www.npmjs.com/package/react-dom">
                react-dom
              </a> (license: <a
                href="https://github.com/facebook/react/blob/master/LICENSE">MIT</a>
              , source code: <a
                href="https://github.com/facebook/react">
                GitHub
              </a>)
            </li>
            <li>
              <a
                href="https://www.npmjs.com/package/antd">
                antd
              </a> (license: <a
                href="https://github.com/ant-design/ant-design/blob/master/LICENSE">MIT</a>
              , source code: <a
                href="https://github.com/ant-design/ant-design">
                GitHub
              </a>)
            </li>
            <li>
              <a
                href="https://www.npmjs.com/package/@ant-design/icons">
                @ant-design/icons
              </a> (license: <a
                href="https://github.com/ant-design/ant-design-icons/blob/master/packages/icons-react/LICENSE">MIT</a>
              , source code: <a
                href="https://github.com/ant-design/ant-design-icons">
                GitHub
              </a>)
            </li>
            <li>
              <a
                href="https://www.npmjs.com/package/int64-buffer">
                int64-buffer
              </a> (license: <a
                href="https://github.com/kawanet/int64-buffer/blob/master/LICENSE">MIT</a>
              , source code: <a
                href="https://github.com/kawanet/int64-buffer">
                GitHub
              </a>)
            </li>
            <li>
              <a
                href="https://developer.chrome.com/docs/workbox">
                Workbox
              </a> (license: <a
                href="https://github.com/GoogleChrome/workbox/blob/v6/LICENSE">MIT</a>
              , source code: <a
                href="https://github.com/googlechrome/workbox">
                GitHub
              </a>)
            </li>
          </ul>
          <p>and for development</p>
          <ul>
            <li>
              <a
                href="https://www.npmjs.com/package/react-scripts">
                react-scripts / create-react-app
              </a> (license: <a
                href="https://github.com/facebook/create-react-app/blob/master/LICENSE">MIT</a>
              , source code: <a
                href="https://github.com/facebook/create-react-app">
                GitHub
              </a>)
            </li>
            <li>
              <a
                href="https://www.npmjs.com/package/babel-plugin-import">
                babel-plugin-import
              </a> (license: <a
                href="https://github.com/ant-design/babel-plugin-import/blob/master/package.json">MIT</a>
              , source code: <a
                href="https://github.com/ant-design/babel-plugin-import">
                GitHub
              </a>)
            </li>
          </ul>
          and their various dependencies.
        </div>
      </Modal>
    );
  }
}

export default AboutDialog;