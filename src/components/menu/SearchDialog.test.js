import SearchDialog from './SearchDialog';

let dialog;

beforeEach(() => {
  dialog = new SearchDialog();
});

it('converts search string to byte array', () => {
  const bytes = dialog.convertSearchStringToByteArray('000A10FF');
  expect(bytes).toEqual([0x00, 0x0A, 0x10, 0xFF]);
});

it('converts null search string to zero length byte array', () => {
  const bytes = dialog.convertSearchStringToByteArray(null);
  expect(bytes).toEqual([]);
});

it('converts undefined search string to zero length byte array', () => {
  const bytes = dialog.convertSearchStringToByteArray();
  expect(bytes).toEqual([]);
});

it('converts empty search string to zero length byte array', () => {
  const bytes = dialog.convertSearchStringToByteArray('');
  expect(bytes).toEqual([]);
});

it('converts byte array to search string', () => {
  const byteString = dialog.convertByteArrayToSearchString([0x00, 0x0A, 0x10, 0xFF]);
  expect(byteString.toUpperCase()).toEqual('000A10FF');
});

it('converts byte search string to ASCII', () => {
  const byteString = dialog.convertSearchStringToAscii('000A10FF616263414243');
  expect(byteString).toEqual('....abcABC');
});