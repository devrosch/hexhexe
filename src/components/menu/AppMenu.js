// import { Icon, Menu } from 'antd';
import { Menu } from 'antd';
import {
  BarsOutlined, FileOutlined, FileAddOutlined,
  SaveOutlined, CloseCircleOutlined, CloseOutlined, EditOutlined, ArrowRightOutlined, SearchOutlined,
  UndoOutlined, RedoOutlined, PlusCircleOutlined, MinusCircleOutlined, EyeOutlined, CheckOutlined, InfoCircleOutlined
} from '@ant-design/icons';
import React, { Component } from 'react';
import NewFileDialog from './NewFileDialog';
import GoToDialog from './GoToDialog';
import SearchDialog from './SearchDialog';
import InsertBytesDialog from './InsertBytesDialog';
import DeleteBytesDialog from './DeleteBytesDialog';
import AboutDialog from './AboutDialog';
import './AppMenu.css';
import SysInfoProvider from '../../model/SysInfoProvider';

const isMacOs = SysInfoProvider.detectOS() === 'macOS';
const fileShortcutsModifierKeys = isMacOs ? '⇧ ⌃ ' : 'Alt-Shift-';
const fileOpenShortcutModifierKeys = isMacOs ? '⌃ ⌥ ' : fileShortcutsModifierKeys;
const editShortcutsModifierKeys = isMacOs ? '⌃ ' : 'Ctrl-';

class AppMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNewFileDialog: false,
      showGoToDialog: false,
      showSearchDialog: false,
      showInsertBytesDialog: false,
      showDeleteBytesDialog: false,
      showAboutDialog: false,
    }

    // ref for reading data from file input
    this.fileInput = React.createRef();

    // make this binding work as expected for functions
    this.onClick = this.onClick.bind(this);
    this.handleShortcuts = this.handleShortcuts.bind(this);
    this.fileNew = this.fileNew.bind(this);
    this.handleCloseNewFileDialog = this.handleCloseNewFileDialog.bind(this);
    this.fileOpen = this.fileOpen.bind(this);
    this.fileSave = this.fileSave.bind(this);
    this.fileClose = this.fileClose.bind(this);
    this.fileCloseAll = this.fileCloseAll.bind(this);
    this.setBytesPerRow = this.setBytesPerRow.bind(this);
    this.showGoToDialog = this.showGoToDialog.bind(this);
    this.handleCloseGoToDialog = this.handleCloseGoToDialog.bind(this);
    this.showSearchDialog = this.showSearchDialog.bind(this);
    this.handleCloseSearchDialog = this.handleCloseSearchDialog.bind(this);
    this.handleUndo = this.handleUndo.bind(this);
    this.handleRedo = this.handleRedo.bind(this);
    this.showInsertBytesDialog = this.showInsertBytesDialog.bind(this);
    this.handleCloseInsertBytesDialog = this.handleCloseInsertBytesDialog.bind(this);
    this.showDeleteBytesDialog = this.showDeleteBytesDialog.bind(this);
    this.handleCloseDeleteBytesDialog = this.handleCloseDeleteBytesDialog.bind(this);
    this.showAboutDialog = this.showAboutDialog.bind(this);
    this.handleCloseAboutDialog = this.handleCloseAboutDialog.bind(this);
    this.setTheme = this.setTheme.bind(this);
  }

  componentDidMount() {
    if (this.props.mode === 'horizontal') {
      document.addEventListener('keydown', this.handleShortcuts, false);
    }
  }

  componentWillUnmount() {
    if (this.props.mode === 'horizontal') {
      document.removeEventListener('keydown', this.handleShortcuts, false);
    }
  }

  onClick(e) {
    const key = e.key;
    switch (key) {
      case 'fileNew':
        this.fileNew();
        break;
      case 'fileOpen':
        // special case, handled directly by fileOpen()
        break;
      case 'fileSave':
        this.fileSave();
        break;
      case 'fileClose':
        this.fileClose();
        break;
      case 'fileCloseAll':
        this.fileCloseAll();
        break;
      case 'goTo':
        this.showGoToDialog();
        break;
      case 'search':
        this.showSearchDialog();
        break;
      case 'undo':
        this.handleUndo();
        break;
      case 'redo':
        this.handleRedo();
        break;
      case 'insertBytes':
        this.showInsertBytesDialog();
        break;
      case 'deleteBytes':
        this.showDeleteBytesDialog();
        break;
      case 'bytesPerRow8':
        this.setBytesPerRow(8);
        break;
      case 'bytesPerRow16':
        this.setBytesPerRow(16);
        break;
      case 'bytesPerRow32':
        this.setBytesPerRow(32);
        break;
      case 'groupBytesBy1':
        this.setByteGroupSize(1);
        break;
      case 'groupBytesBy2':
        this.setByteGroupSize(2);
        break;
      case 'groupBytesBy4':
        this.setByteGroupSize(4);
        break;
      case 'showAbout':
        this.showAboutDialog();
        break;
      default:
        console.error('Unsupported file item key: ' + key);
    }
  }

  handleShortcuts(e) {
    const fileModifiersPressed = isMacOs ?
      e.shiftKey && e.ctrlKey && !e.altKey && !e.metaKey :
      e.shiftKey && e.altKey && !e.ctrlKey && !e.metaKey;
    const editModifiersPressed = e.ctrlKey && !e.altKey && !e.shiftKey && !e.metaKey;

    if (fileModifiersPressed && e.key.toLowerCase() === 'n') {
      this.fileNew();
      e.preventDefault();
      return false;
    }
    // cannot use same mechanism for fileOpen() due to browser security limitation => use "accessKey" property instead
    else if (fileModifiersPressed && e.key.toLowerCase() === 's') {
      this.fileSave();
      e.preventDefault();
      return false;
    }
    else if (fileModifiersPressed && e.key.toLowerCase() === 'c') {
      this.fileClose();
      e.preventDefault();
      return false;
    }
    else if (fileModifiersPressed && e.key.toLowerCase() === 'q') {
      this.fileCloseAll();
      e.preventDefault();
      return false;
    }
    else if (editModifiersPressed && e.key.toLowerCase() === 'g') {
      this.showGoToDialog();
      e.preventDefault();
      return false;
    }
    else if (editModifiersPressed && e.key.toLowerCase() === 'f') {
      this.showSearchDialog();
      e.preventDefault();
      return false;
    }
    else if (editModifiersPressed && e.key.toLowerCase() === 'i') {
      this.showInsertBytesDialog();
      e.preventDefault();
      return false;
    }
    else if (editModifiersPressed && e.key.toLowerCase() === 'd') {
      this.showDeleteBytesDialog();
      e.preventDefault();
      return false;
    }
    else if (editModifiersPressed && e.key.toLowerCase() === 'z') {
      this.handleUndo();
      e.preventDefault();
      return false;
    }
    else if (editModifiersPressed && e.key.toLowerCase() === 'y') {
      this.handleRedo();
      e.preventDefault();
      return false;
    }
  }

  fileNew() {
    this.setState({ showNewFileDialog: true });
  }

  handleCloseNewFileDialog() {
    this.setState({ showNewFileDialog: false });
    this.props.handleFocusScrollArea();
  }

  fileOpen(e) {
    // for details see: https://reactjs.org/docs/uncontrolled-components.html#the-file-input-tag
    let selectedFiles = this.fileInput.current.files;
    // copy selected files before resetting input
    let files = [];
    for (let i = 0; i < selectedFiles.length; i++) {
      files.push(selectedFiles[i]);
    }
    // reset file input, see https://stackoverflow.com/questions/20549241/how-to-reset-input-type-file
    // if not reset, opening the same file again does not fire on change event => cannot be opened
    this.fileInput.current.value = '';
    this.props.handleFileOpen(files);
  }

  fileSave(e) {
    this.props.handleFileSave();
    this.props.handleFocusScrollArea();
  }

  fileClose(e) {
    this.props.handleFileClose();
    this.props.handleFocusScrollArea();
  }

  fileCloseAll(e) {
    this.props.handleFileCloseAll();
  }

  setBytesPerRow(numBytes) {
    this.props.handleSetBytesPerRow(numBytes);
    this.props.handleFocusScrollArea();
  }

  setByteGroupSize(size) {
    this.props.handleSetByteGroupSize(size);
    this.props.handleFocusScrollArea();
  }

  showGoToDialog() {
    this.setState({ showGoToDialog: true });
  }

  handleCloseGoToDialog() {
    this.setState({ showGoToDialog: false });
    this.props.handleFocusScrollArea();
  }

  showSearchDialog() {
    this.setState({ showSearchDialog: true });
  }

  handleCloseSearchDialog() {
    this.setState({ showSearchDialog: false });
    this.props.handleFocusScrollArea();
  }

  handleUndo() {
    if (!this.props.activeDocument) {
      return;
    }
    this.props.activeDocument.undo();
    this.props.handleActiveDocumentUpdated();
  }

  handleRedo() {
    if (!this.props.activeDocument) {
      return;
    }
    this.props.activeDocument.redo();
    this.props.handleActiveDocumentUpdated();
  }

  showInsertBytesDialog() {
    this.setState({ showInsertBytesDialog: true });
  }

  handleCloseInsertBytesDialog() {
    this.setState({ showInsertBytesDialog: false });
    this.props.handleFocusScrollArea();
  }

  showDeleteBytesDialog() {
    this.setState({ showDeleteBytesDialog: true });
  }

  handleCloseDeleteBytesDialog() {
    this.setState({ showDeleteBytesDialog: false });
    this.props.handleFocusScrollArea();
  }

  showAboutDialog() {
    this.setState({ showAboutDialog: true });
  }

  handleCloseAboutDialog() {
    this.setState({ showAboutDialog: false });
    this.props.handleFocusScrollArea();
  }

  setTheme(theme) {
    // see https://superuser.com/questions/361297/what-colour-is-the-dark-green-on-old-fashioned-green-screen-computer-displays
    // see http://unusedino.de/ec64/technical/misc/vic656x/colors/
    this.props.handleSetTheme(theme);
  }

  render() {
    const bytesPerRow = this.props.bytesPerRow;
    const byteGroupSize = this.props.byteGroupSize;
    const isHorizontalMenu = this.props.mode === 'horizontal';

    const items = [
      {
        key: 'filesSub',
        label: 'Files',
        icon: <BarsOutlined />,
        children: [
          {
            key: 'fileNew',
            icon: <FileOutlined />,
            label:
              <span>
                <span>New File...</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{fileShortcutsModifierKeys}N</span> : null}
              </span>,
          },
          {
            key: 'fileOpen',
            label:
              <>
                <input
                  id="fileOpenInput"
                  ref={this.fileInput}
                  type="file"
                  className="file-input"
                  onChange={this.fileOpen}
                  multiple={true}
                />
                <label
                  htmlFor="fileOpenInput"
                  className="file-input-label"
                  accessKey="o"
                >
                  <FileAddOutlined />
                  <span>Open File...</span>
                  {isHorizontalMenu ? <span className="shortcut">&nbsp;{fileOpenShortcutModifierKeys}O</span> : null}
                </label>
              </>,
          },
          {
            key: 'fileSave',
            icon: <SaveOutlined />,
            label:
              <span>
                <span>Save/Download...</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{fileShortcutsModifierKeys}S</span> : null}
              </span>,
          },
          {
            key: 'fileClose',
            icon: <CloseCircleOutlined />,
            label:
              <span>
                <span>Close File</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{fileShortcutsModifierKeys}C</span> : null}
              </span>,
          },
          {
            key: 'fileCloseAll',
            icon: <CloseOutlined />,
            label:
              <span>
                <span>Close All Files</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{fileShortcutsModifierKeys}Q</span> : null}
              </span>,
          },
        ]
      },
      {
        key: 'editSub',
        icon: <EditOutlined />,
        label: 'Edit',
        children: [
          {
            key: 'goTo',
            icon: <ArrowRightOutlined />,
            label:
              <span>
                <span>Go To...</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{editShortcutsModifierKeys}G</span> : null}
              </span>,
          },
          {
            key: 'search',
            icon: <SearchOutlined />,
            label:
              <span>
                <span>Search...</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{editShortcutsModifierKeys}F</span> : null}
              </span>,
          },
          {
            type: 'divider',
          },
          {
            key: 'undo',
            icon: <UndoOutlined />,
            label:
              <span>
                <span>Undo</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{editShortcutsModifierKeys}Z</span> : null}
              </span>,
          },
          {
            key: 'redo',
            icon: <RedoOutlined />,
            label:
              <span>
                <span>Redo</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{editShortcutsModifierKeys}Y</span> : null}
              </span>,
          },
          {
            type: 'divider',
          },
          {
            key: 'insertBytes',
            icon: <PlusCircleOutlined />,
            label:
              <span>
                <span>Insert Bytes...</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{editShortcutsModifierKeys}I</span> : null}
              </span>,
          },
          {
            key: 'deleteBytes',
            icon: <MinusCircleOutlined />,
            label:
              <span>
                <span>Delete Bytes...</span>
                {isHorizontalMenu ? <span className="shortcut">&nbsp;{editShortcutsModifierKeys}D</span> : null}
              </span>,
          },
        ],
      },
      {
        key: 'DisplaySub',
        label: 'Display',
        icon: <EyeOutlined />,
        children: [
          {
            type: 'group',
            label: 'Bytes Per Row',
            children: [
              {
                key: "bytesPerRow8",
                icon: bytesPerRow === 8 ? <CheckOutlined /> : ' ',
                label: '8',
              },
              {
                key: "bytesPerRow16",
                icon: bytesPerRow === 16 ? <CheckOutlined /> : ' ',
                label: '16',
              },
              {
                key: "bytesPerRow32",
                icon: bytesPerRow === 32 ? <CheckOutlined /> : ' ',
                label: '32',
              },
            ],
          },
          {
            type: 'group',
            label: 'Group Bytes',
            children: [
              {
                key: "groupBytesBy1",
                icon: byteGroupSize === 1 ? <CheckOutlined /> : ' ',
                label: '1',
              },
              {
                key: "groupBytesBy2",
                icon: byteGroupSize === 2 ? <CheckOutlined /> : ' ',
                label: '2',
              },
              {
                key: "groupBytesBy4",
                icon: byteGroupSize === 4 ? <CheckOutlined /> : ' ',
                label: '4',
              },
            ],
          },
        ],
      },
      {
        key: "showAbout",
        icon: <InfoCircleOutlined />,
        label: 'About',
      },
    ];

    return (
      <>
        <Menu
          theme="light"
          mode={this.props.mode}
          selectable={false} // prevent automatic highlighting of last selected menu item
          activeKey="" // prevent menu item being after when drawer slides in
          forceSubMenuRender={true} // to make "accessKey" property for "File Open..." work
          items={items}
          onClick={this.onClick}
        />

        <NewFileDialog
          visible={this.state.showNewFileDialog}
          handleCloseDialog={this.handleCloseNewFileDialog}
          handleFileNew={this.props.handleFileNew}
        />

        <GoToDialog
          visible={this.state.showGoToDialog}
          handleCloseDialog={this.handleCloseGoToDialog}
          activeDocument={this.props.activeDocument}
          handleActiveDocumentUpdated={this.props.handleActiveDocumentUpdated}
        />

        <SearchDialog
          visible={this.state.showSearchDialog}
          handleCloseDialog={this.handleCloseSearchDialog}
          activeDocument={this.props.activeDocument}
          handleActiveDocumentUpdated={this.props.handleActiveDocumentUpdated}
        />

        <InsertBytesDialog
          visible={this.state.showInsertBytesDialog}
          handleCloseDialog={this.handleCloseInsertBytesDialog}
          activeDocument={this.props.activeDocument}
          handleActiveDocumentUpdated={this.props.handleActiveDocumentUpdated}
        />

        <DeleteBytesDialog
          visible={this.state.showDeleteBytesDialog}
          handleCloseDialog={this.handleCloseDeleteBytesDialog}
          activeDocument={this.props.activeDocument}
          handleActiveDocumentUpdated={this.props.handleActiveDocumentUpdated}
        />

        <AboutDialog
          visible={this.state.showAboutDialog}
          handleCloseDialog={this.handleCloseAboutDialog}
        />

      </>
    )
  }

}

export default AppMenu;
