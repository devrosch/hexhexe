import React, { PureComponent } from 'react';
import './Address.css';

const LN_16 = Math.log(16);

class Address extends PureComponent {
  constructor(props) {
    super(props);
    this.calculateNumHexDigits = this.calculateNumHexDigits.bind(this);
  }

  calculateNumHexDigits(documentSize) {
    let numHexDigits = Math.ceil(Math.log(documentSize + 1) / LN_16);
    if (isNaN(numHexDigits) || numHexDigits < 8) {
      // always display at least eight hex digits for address
      numHexDigits = 8;
    }
    if (numHexDigits % 2) {
      // always display even number of hex digits
      numHexDigits += 1;
    }
    return numHexDigits;
  }

  render() {
    const numHexDigits = this.calculateNumHexDigits(this.props.documentSize);
    const hexString = this.props.address
      .toString(16)
      .padStart(numHexDigits, '0')
      .toUpperCase();

    return (
      <span
        className="address"
      >
        {hexString}
      </span>
    );
  }
}

export default Address;