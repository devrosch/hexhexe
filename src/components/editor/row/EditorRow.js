import React, { Component } from 'react';
import Address from './Address';
import Bytes from './Bytes';
import Ascii from './Ascii';
import './EditorRow.css';

class EditorRow extends Component {
  /**
   * Only update row when potentially affected by state change.
   * Necessary to deal with large number of child elements that usually don't need to be rerendered.
   * @param {*} nextProps 
   */
  shouldComponentUpdate(nextProps, nextState) {
    // definitely update, if any of these props have changed
    if (
      nextProps.address !== this.props.address
      || nextProps.fileSize !== this.props.fileSize
      || nextProps.bytesPerRow !== this.props.bytesPerRow
      || (nextProps.cursorPosition.address >= nextProps.address
        && nextProps.cursorPosition.address < nextProps.address + nextProps.bytesPerRow)
      || (this.props.cursorPosition.address >= this.props.address
        && this.props.cursorPosition.address < this.props.address + this.props.bytesPerRow)
      || nextProps.markedRange !== this.props.markedRange
      || nextProps.bytes !== this.props.bytes
      || nextProps.byteGroupSize !== this.props.byteGroupSize
    ) {
      return true;
    }
    // otherwise, don't update to improve performance
    return false;
  }

  render() {
    const address = this.props.address;
    const documentSize = this.props.documentSize;
    const bytesPerRow = this.props.bytesPerRow;
    const byteGroupSize = this.props.byteGroupSize;
    const bytes = this.props.bytes;
    const cursorPosition = this.props.cursorPosition;
    const markedRange = this.props.markedRange;
    const handleCursorPositionChanged = this.props.handleCursorPositionChanged;
    const handleDocumentReadError = this.props.handleDocumentReadError;

    return (
      <div
        className="editor-row"
      >
        <Address
          address={address}
          documentSize={documentSize}
        />
        <Bytes
          address={address}
          bytesPerRow={bytesPerRow}
          byteGroupSize={byteGroupSize}
          bytes={bytes}
          cursorPosition={cursorPosition}
          markedRange={markedRange}
          handleCursorPositionChanged={(address, nibble) => handleCursorPositionChanged(address, nibble, 'bytes')}
          handleDocumentReadError={handleDocumentReadError}
        />
        <Ascii
          address={address}
          bytes={bytes}
          cursorPosition={cursorPosition}
          markedRange={markedRange}
          handleCursorPositionChanged={(address, nibble) => handleCursorPositionChanged(address, nibble, 'ascii')}
          handleDocumentReadError={handleDocumentReadError}
        />
      </div>
    );
  }
}

export default EditorRow;