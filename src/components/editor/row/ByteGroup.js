import React, { PureComponent } from 'react';
import Nibble from './Nibble';
import './Byte.css';

class ByteGroup extends PureComponent {
  constructor(props) {
    super(props);
    this.formatByte = this.formatByte.bind(this);
  }

  formatByte(value) {
    // see: https://stackoverflow.com/questions/9909038/formatting-hexadecimal-number-in-javascript
    return ("00" + value.toString(16)).substr(-2).toUpperCase();
  }

  render() {
    const address = this.props.address;
    const values = this.props.values; // 0..n bytes
    const cursorPosition = this.props.cursorPosition;
    const markedRange = this.props.markedRange;
    const handleCursorPositionChanged = this.props.handleCursorPositionChanged;
    const byteGroupSize = this.props.byteGroupSize;

    let nibbles = [];
    for (let i = 0; i < byteGroupSize; i++) {
      const isLeftmostNibble = i === 0;
      const isRightmostNibble = i === (byteGroupSize - 1);

      const isAddressPastEnd = i >= values.length;
      const byteString = isAddressPastEnd ?
        '  ' : this.formatByte(values[i]);

      const leftNibble = <Nibble
        value={byteString.substr(0, 1)}
        key={address + i + '_1'}
        address={isAddressPastEnd ? null : address + i}
        nibblePosition={isLeftmostNibble ? 'leftmost' : 'center'}
        nibble={1}
        cursorPosition={cursorPosition}
        markedRange={markedRange}
        handleCursorPositionChanged={handleCursorPositionChanged}
      />
      const rightNibble = <Nibble
        value={byteString.substr(1, 1)}
        key={address + i + '_0'}
        address={isAddressPastEnd ? null : address + i}
        nibblePosition={isRightmostNibble ? 'rightmost' : 'center'}
        nibble={0}
        cursorPosition={cursorPosition}
        markedRange={markedRange}
        handleCursorPositionChanged={handleCursorPositionChanged}
      />

      nibbles.push(leftNibble);
      nibbles.push(rightNibble);
    }

    return (
      <span
        className="byte-group"
      >
        {nibbles}
      </span>
    );
  }
}

export default ByteGroup;