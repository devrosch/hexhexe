import Address from './Address';

it('always shows address with at least 8 hex digits for document size 0', () => {
  const address = new Address();
  const numHexDigits = address.calculateNumHexDigits(1);
  expect(numHexDigits).toEqual(8);
});

it('always shows address with at least 8 hex digits for document size 0', () => {
  const address = new Address();
  const numHexDigits = address.calculateNumHexDigits(0);
  expect(numHexDigits).toEqual(8);
});

it('shows address with 8 hex digits for negative document size', () => {
  const address = new Address();
  const numHexDigits = address.calculateNumHexDigits(-1);
  expect(numHexDigits).toEqual(8);
});

it('shows address with even number of hex digits', () => {
  const address = new Address();
  const numHexDigits = address.calculateNumHexDigits(0x100000001);
  expect(numHexDigits).toEqual(10);
});

it('shows address with even number of hex digits in corner case', () => {
  const address = new Address();
  const numHexDigits = address.calculateNumHexDigits(0x100000000);
  expect(numHexDigits).toEqual(10);
});

it('shows address with even number of hex digits', () => {
  const address = new Address();
  const numHexDigits = address.calculateNumHexDigits(0x1000000001);
  expect(numHexDigits).toEqual(10);
});

it('shows address with even number of hex digits in corner case', () => {
  const address = new Address();
  const numHexDigits = address.calculateNumHexDigits(0xFFFFFFFFF);
  expect(numHexDigits).toEqual(10);
});