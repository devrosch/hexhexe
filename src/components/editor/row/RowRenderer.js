import React from 'react';
import EditorRow from './EditorRow';

/**
 * Provides methods to render rows of file content.
 */
class RowRenderer {
  constructor(file, bytesPerRow, byteGroupSize, cursorPosition, markedRange, handleCursorPositionChanged, handleDocumentReadError) {
    this.file = file;
    this.bytesPerRow = bytesPerRow;
    this.byteGroupSize = byteGroupSize;
    this.cursorPosition = cursorPosition;
    this.markedRange = markedRange;
    this.handleCursorPositionChanged = handleCursorPositionChanged;
    this.handleDocumentReadError = handleDocumentReadError;
    this.render = this.render.bind(this);
  }

  /**
   * The number of rows available.
   */
  get numRows() {
    if (!this.file || !this.bytesPerRow) {
      return 0;
    }
    return Math.ceil(this.file.size / this.bytesPerRow);
  }

  /**
   * Returns the rows from startIndex to before endIndex.
   * @param {*} startIndex First row index to return.
   * @param {*} endIndex Row index after the last row index to return.
   */
  render(startIndex, endIndex) {
    let rows = [];
    for (let index = startIndex; index < endIndex; index++) {
      const startAddress = index * this.bytesPerRow;
      const pastEndAddress = startAddress + this.bytesPerRow;
      const documentSize = this.file.size;
      const bytesPromise = this.file.readBytes(startAddress, pastEndAddress);
      const editorRow = <EditorRow
        key={startAddress + '_' + this.bytesPerRow}
        address={startAddress}
        documentSize={documentSize}
        bytesPerRow={this.bytesPerRow}
        byteGroupSize={this.byteGroupSize}
        bytes={bytesPromise}
        cursorPosition={this.cursorPosition}
        markedRange={this.markedRange}
        handleCursorPositionChanged={this.handleCursorPositionChanged}
        handleDocumentReadError={this.handleDocumentReadError}
      />;
      rows.push(editorRow);
    }
    return rows;
  }
}

export default RowRenderer;