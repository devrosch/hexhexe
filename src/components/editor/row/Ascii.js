import React, { Component } from 'react';
import './Ascii.css';

class Ascii extends Component {
  constructor(props) {
    super(props);
    this.state = { bytes: [] };
    this.mounted = true;
    this.dataPromise = null;
    this.handleCursorPositionChanged = this.handleCursorPositionChanged.bind(this);
    this.updateBytes = this.updateBytes.bind(this);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
    this.updateBytes();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.bytes !== this.props.bytes
      // 2nd check required for FF in rare cases
      || this.props.bytes !== this.bytesPromise) {
      this.updateBytes();
    }
  }

  updateBytes() {
    const dataPromise = this.props.bytes;
    const handleDocumentReadError = this.props.handleDocumentReadError;
    dataPromise
      .then((data) => {
        if (this.mounted) {
          this.bytesPromise = dataPromise;
          this.setState({ bytes: data });
        }
      })
      .catch(error => {
        this.bytesPromise = dataPromise;
        handleDocumentReadError(error);
      });
  }

  handleCursorPositionChanged(e) {
    const address = parseInt(e.target.getAttribute('data-address'), 10);
    this.props.handleCursorPositionChanged(address, 1);
  }

  render() {
    const address = this.props.address;
    const dataArrayBuffer = this.state.bytes;
    const bytes = new Uint8Array(dataArrayBuffer);
    const cursorPosition = this.props.cursorPosition;
    const markedRange = this.props.markedRange;

    let chars = [];
    bytes.forEach((value) =>
      chars.push(
        value < 32 || value > 126 ? '.' : String.fromCharCode(value))
    );

    let charSpans = [];
    chars.forEach((value, index) => {
      let classNamesArray = ['ascii-character'];
      if (markedRange
        && markedRange.start <= address + index
        && markedRange.end > address + index) {
          classNamesArray.push('ascii-marked');
      }
      if (cursorPosition.address === address + index) {
        if (cursorPosition.mode === 'ascii') {
          classNamesArray.push('cursor-position ascii-mode');
        }
        else {
          classNamesArray.push('cursor-position bytes-mode');
        }
      }
      const classNames = classNamesArray.join(' ');

      charSpans.push(
        <span
          key={address + index}
          data-address={address + index}
          className={classNames}
          onClick={this.handleCursorPositionChanged}
        >
          {value}
        </span>
      );
    });

    return (
      <span
        className="ascii"
      >
        {/* nest one level deeper to align height */}
        <span>
          {charSpans}
        </span>
      </span>
    );
  }
}

export default Ascii;