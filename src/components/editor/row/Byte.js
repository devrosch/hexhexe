import React, { PureComponent } from 'react';
import Nibble from './Nibble';
import './Byte.css';

class Byte extends PureComponent {
  constructor(props) {
    super(props);
    this.formatByte = this.formatByte.bind(this);
  }

  formatByte(value) {
    // see: https://stackoverflow.com/questions/9909038/formatting-hexadecimal-number-in-javascript
    return ("00" + value.toString(16)).substr(-2).toUpperCase();
  }

  render() {
    const address = this.props.address;
    const value = this.props.value;
    const cursorPosition = this.props.cursorPosition;
    const markedRange = this.props.markedRange;
    const handleCursorPositionChanged = this.props.handleCursorPositionChanged;

    const byteString = this.formatByte(value);

    return (
      <span
        className="byte"
      >
        <Nibble
          value={byteString.substr(0, 1)}
          address={address}
          nibble={1}
          cursorPosition={cursorPosition}
          markedRange={markedRange}
          handleCursorPositionChanged={handleCursorPositionChanged} />
        <Nibble
          value={byteString.substr(1, 1)}
          address={address}
          nibble={0}
          cursorPosition={cursorPosition}
          markedRange={markedRange}
          handleCursorPositionChanged={handleCursorPositionChanged} />
      </span>
    );
  }
}

export default Byte;