import React, { Component } from 'react';
import ByteGroup from './ByteGroup';
import './Bytes.css';

class Bytes extends Component {
  constructor(props) {
    super(props);
    this.state = { bytes: [] };
    this.mounted = true;
    this.dataPromise = null;
    this.updateBytes = this.updateBytes.bind(this);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
    this.updateBytes();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.bytes !== this.props.bytes
      // 2nd check required for FF in rare cases
      || this.props.bytes !== this.bytesPromise) {
      this.updateBytes();
    }
  }

  updateBytes() {
    const dataPromise = this.props.bytes;
    const handleDocumentReadError = this.props.handleDocumentReadError;
    dataPromise
      .then((data) => {
        if (this.mounted) {
          this.bytesPromise = dataPromise;
          this.setState({ bytes: data });
        }
      })
      .catch(error => {
        this.bytesPromise = dataPromise;
        handleDocumentReadError(error);
      });
  }

  render() {
    const address = this.props.address
    const bytesPerRow = this.props.bytesPerRow;
    const byteGroupSize = this.props.byteGroupSize;
    const dataArrayBuffer = this.state.bytes;
    const bytes = new Uint8Array(dataArrayBuffer);
    const cursorPosition = this.props.cursorPosition;
    const markedRange = this.props.markedRange;
    const handleCursorPositionChanged = this.props.handleCursorPositionChanged;

    // group bytes
    let byteGroups = [];
    for (let i = 0; i < Math.ceil(bytesPerRow / byteGroupSize); i++) {
      const uint8Array = bytes.slice(i * byteGroupSize, (i + 1) * byteGroupSize);
      const byteArray = Array.from(uint8Array);
      byteGroups.push(<ByteGroup
        key={address + i * byteGroupSize}
        values={byteArray}
        address={address + i * byteGroupSize}
        byteGroupSize={byteGroupSize}
        cursorPosition={cursorPosition}
        markedRange={markedRange}
        handleCursorPositionChanged={handleCursorPositionChanged}
      />);
    }

    return (
      <span
        className="bytes"
      >
        {byteGroups}
      </span>
    );
  }
}

export default Bytes;