import React, { PureComponent } from 'react';
import './Nibble.css';

class Nibble extends PureComponent {
  constructor(props) {
    super(props);
    this.handleCursorPositionChanged = this.handleCursorPositionChanged.bind(this);
  }

  handleCursorPositionChanged(e) {
    const address = this.props.address;
    const nibble = this.props.nibble;
    if (address !== null) {
      this.props.handleCursorPositionChanged(address, nibble);
    }
  }

  render() {
    const address = this.props.address;
    const nibble = this.props.nibble;
    const value = this.props.value;
    const cursorAddress = this.props.cursorPosition.address;
    const cursorNibble = this.props.cursorPosition.nibble;
    const cursorMode = this.props.cursorPosition.mode;
    const markedRangeStart = this.props.markedRange ? this.props.markedRange.start : -1;
    const markedRangeEnd = this.props.markedRange ? this.props.markedRange.end : -1;
    const nibblePosition = this.props.nibblePosition;

    // assemble class names
    let classNamesArray = [];
    if (address === null) {
      classNamesArray.push('nibble-blank');
    }
    else {
      classNamesArray.push('nibble');
    }
    if (cursorAddress === address && cursorNibble === nibble) {
      if (cursorMode === 'bytes') {
        classNamesArray.push('cursor-position bytes-mode');
      }
      else {
        classNamesArray.push('cursor-position ascii-mode');
      }
    }
    if (address && markedRangeStart <= address && markedRangeEnd > address) {
      classNamesArray.push('nibble-marked')
    }
    if (nibblePosition === 'leftmost') {
      classNamesArray.push('nibble-leftmost');
    }
    if (nibblePosition === 'rightmost') {
      classNamesArray.push('nibble-rightmost');
    }
    const classNames = classNamesArray.join(' ');

    return (
      <span
        className={classNames}
        onClick={this.handleCursorPositionChanged}
      >
        {value}
      </span>
    );
  }
}

export default Nibble;