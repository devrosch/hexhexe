import React, { Component } from 'react';
import ScrollArea from './scrollarea/ScrollArea';
import RowRenderer from './row/RowRenderer';
import { Card, Tabs } from 'antd';

/**
 * The delay between two cursor moves (after 2nd) when holding down an arrow key.
 */
const arrowKeyIntervalMs = 20;
/**
 * The delay between the first and second cursor move when holding down an arrow key.
 */
const arrowKeyInitialIntervalMs = 100;
/**
 * List of navigation keys to react to.
 * Keys and corresponding key codes:
 * key: "ArrowLeft", keyCode: 37
 * key: "ArrowUp", keyCode: 38
 * key: "ArrowRight", keyCode: 39
 * key: "ArrowDown", keyCode: 40
 * key: "PageUp", keyCode: 33
 * key: "PageDown", keyCode: 34
 */
const navigationKeys = [
  'ArrowLeft',
  'ArrowRight',
  'ArrowUp',
  'ArrowDown',
  'PageUp',
  'PageDown'
];

const hexDigits = '0123456789abcdef';
const printableAsciiCharacters = ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';

class EditorPane extends Component {
  constructor(props) {
    super(props);
    this.arrowKeyDown = null;
    this.scrollAreaRef = React.createRef();
    // bind methods to this
    this.scrollToCursorPosition = this.scrollToCursorPosition.bind(this);
    this.focusScrollArea = this.focusScrollArea.bind(this);
    this.moveCursor = this.moveCursor.bind(this);
    this.moveCursorInterval = this.moveCursorInterval.bind(this);
    this.handleEditorEditEvent = this.handleEditorEditEvent.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleTabEdit = this.handleTabEdit.bind(this);
    this.handleCursorPositionChanged = this.handleCursorPositionChanged.bind(this);
  }

  //#region API

  scrollToCursorPosition() {
    const activeDocumentKey = this.props.activeDocumentKey;
    const activeDocument = this.props.documents.find(d => d.key === activeDocumentKey);
    const bytesPerRow = this.props.bytesPerRow;
    if (activeDocument
      && activeDocument.cursorPosition
      && activeDocument.cursorPosition.address !== null
      && this.scrollAreaRef) {
      const rowIndex = Math.floor(activeDocument.cursorPosition.address / bytesPerRow);
      this.scrollAreaRef.current.scrollToRow(rowIndex);
    }
  }

  focusScrollArea() {
    if (this.scrollAreaRef && this.scrollAreaRef.current) {
      this.scrollAreaRef.current.focus();
    }
  }

  //#endregion

  handleCursorPositionChanged(address, nibble, mode) {
    const activeDocumentKey = this.props.activeDocumentKey;
    const documents = this.props.documents;
    const activeDocument = documents.find(d => d.key === activeDocumentKey);
    activeDocument.cursorPosition = {
      address: address,
      nibble: nibble,
      mode: mode
    }
    // check if update of markedRange is necessary, to avoid unnecessary re-rendering
    if (activeDocument.markedRange
      && (activeDocument.markedRange.start >= 0
        || activeDocument.markedRange >= 0)) {
      activeDocument.markedRange = {
        start: -1,
        end: -1
      }
    }
    this.props.handleActiveDocumentUpdated();
  }

  handleTabChange(key) {
    this.props.handleActiveDocumentChanged(key);
  }

  handleTabEdit(targetKey, action) {
    if (action === 'remove') {
      this.props.handleFileClose(targetKey);
    }
  }

  componentWillUnmount() {
    this.arrowKeyDown = null;
  }

  /**
   * Move cursor according to arrow key.
   * @param {*} key The arrow key that is down.
   */
  moveCursor(key) {
    if (navigationKeys.includes(key)) {
      const cursorPosition = this.props.cursorPosition;
      const bytesPerRow = this.props.bytesPerRow;
      const destinationcursorPosition = {
        address: cursorPosition.address,
        nibble: cursorPosition.nibble,
        mode: cursorPosition.mode,
      }
      const activeDocument = this.props.documents.find(d => d.key === this.props.activeDocumentKey);
      const dataLength = activeDocument === null ? 0 : activeDocument.size;

      if (key === 'ArrowLeft') {
        if (cursorPosition.mode === 'bytes'
          && cursorPosition.nibble === 0) {
          destinationcursorPosition.nibble = 1;
        }
        else if (cursorPosition.mode === 'bytes'
          && cursorPosition.nibble === 1
          && cursorPosition.address > 0) {
          destinationcursorPosition.address -= 1;
          destinationcursorPosition.nibble = 0;
        }
        else if (cursorPosition.mode === 'ascii'
          && cursorPosition.address > 0) {
          destinationcursorPosition.address -= 1;
          destinationcursorPosition.nibble = 1;
        }
        else if (cursorPosition.mode === 'ascii'
          && cursorPosition.address === 0) {
          destinationcursorPosition.nibble = 1;
        }
      } else if (key === 'ArrowRight') {
        if (cursorPosition.mode === 'bytes'
          && cursorPosition.nibble === 1) {
          destinationcursorPosition.nibble = 0;
        }
        else if (cursorPosition.mode === 'bytes'
          && cursorPosition.nibble === 0
          && cursorPosition.address < (dataLength - 1)) {
          destinationcursorPosition.address += 1;
          destinationcursorPosition.nibble = 1;
        }
        else if (cursorPosition.mode === 'ascii'
          && cursorPosition.address < (dataLength - 1)) {
          destinationcursorPosition.address += 1;
          destinationcursorPosition.nibble = 1;
        }
        else if (cursorPosition.mode === 'ascii'
          && cursorPosition.address === (dataLength - 1)) {
          destinationcursorPosition.nibble = 1;
        }
      } else if (key === 'ArrowUp') {
        if (cursorPosition.address - bytesPerRow >= 0) {
          destinationcursorPosition.address = cursorPosition.address - bytesPerRow;
        }
      } else if (key === 'ArrowDown') {
        if (cursorPosition.address + bytesPerRow < dataLength) {
          destinationcursorPosition.address = cursorPosition.address + bytesPerRow;
        }
        else {
          const nextRowStartAddress = Math.floor((cursorPosition.address + bytesPerRow) / bytesPerRow) * bytesPerRow;
          if (nextRowStartAddress < dataLength) {
            // next row exists but is not fully populated => move to last byte/nibble
            destinationcursorPosition.address = dataLength - 1;
            destinationcursorPosition.nibble = cursorPosition.mode === 'ascii' ? 1 : 0;
          }
        }
      } else if (key === 'PageUp') {
        const numVisibleRows = this.scrollAreaRef.current.numVisibleRows;
        if (cursorPosition.address - bytesPerRow * numVisibleRows >= 0) {
          destinationcursorPosition.address = cursorPosition.address - bytesPerRow * numVisibleRows;
        }
        else {
          // cursor less than numVisibleRows below to first row
          const cursorPositionColumnIndex = cursorPosition.address % bytesPerRow;
          destinationcursorPosition.address = cursorPositionColumnIndex;
        }
      } else if (key === 'PageDown') {
        const numVisibleRows = this.scrollAreaRef.current.numVisibleRows;
        if (cursorPosition.address + bytesPerRow * numVisibleRows < dataLength) {
          destinationcursorPosition.address = cursorPosition.address + bytesPerRow * numVisibleRows;
        }
        else {
          const lastRowStartAddress = Math.floor(dataLength / bytesPerRow) * bytesPerRow;
          const lastRowNumColumns = dataLength - lastRowStartAddress;
          const currentColumn = cursorPosition.address % bytesPerRow;
          if (lastRowNumColumns <= currentColumn) {
            // last row not fully populated => move to last byte/nibble
            destinationcursorPosition.address = lastRowStartAddress + lastRowNumColumns - 1;
            destinationcursorPosition.nibble = cursorPosition.mode === 'ascii' ? 1 : 0;
          }
          else {
            destinationcursorPosition.address = lastRowStartAddress + currentColumn;
          }
        }
      }

      this.handleCursorPositionChanged(
        destinationcursorPosition.address,
        destinationcursorPosition.nibble,
        cursorPosition.mode
      )
    }
  }

  /**
  * Move cursor repeatedly when arrow key is down.
  * @param {*} timestamp The timestamp when the method is called..
  */
  moveCursorInterval(timestamp) {
    if (this.arrowKeyDown === null) {
      return;
    }
    if (this.firstRoundtrip) {
      this.firstRoundtrip = false;
      this.moveCursor(this.arrowKeyDown);
      this.lastRoundtripTimestamp = timestamp + arrowKeyInitialIntervalMs;
      requestAnimationFrame(this.moveCursorInterval);
    }
    else {
      if (timestamp - this.lastRoundtripTimestamp < arrowKeyIntervalMs) {
        requestAnimationFrame(this.moveCursorInterval);
      }
      else {
        this.moveCursor(this.arrowKeyDown)
        this.lastRoundtripTimestamp = timestamp;
        requestAnimationFrame(this.moveCursorInterval);
      }
    }
  }

  handleEditorEditEvent(value) {
    console.log('handleEditorEditEvent: ' + value);
    const activeDocumentKey = this.props.activeDocumentKey;
    const activeDocument = this.props.documents.find(d => d.key === activeDocumentKey);
    const initialCursorPosition = activeDocument.cursorPosition;
    const handleActiveDocumentUpdated = this.props.handleActiveDocumentUpdated;
    let newCursorPosition = null;
    if (initialCursorPosition
      && initialCursorPosition.mode === 'bytes'
      && initialCursorPosition.address !== null) {
      // update byte value at cursor position
      activeDocument.readBytes(initialCursorPosition.address, initialCursorPosition.address + 1)
        .then(arrayBuffer => {
          let byte = new Uint8Array(arrayBuffer)[0];
          if (initialCursorPosition.nibble === 0) {
            byte = (byte & 0xF0) + value;
            if (initialCursorPosition.address < activeDocument.size - 1) {
              newCursorPosition = {
                address: initialCursorPosition.address + 1,
                nibble: 1,
                mode: initialCursorPosition.mode,
              }
            }
          }
          else {
            byte = value * 16 + (byte & 0x0F);
            newCursorPosition = {
              address: initialCursorPosition.address,
              nibble: 0,
              mode: initialCursorPosition.mode,
            }
          }
          activeDocument.updateBytes(initialCursorPosition.address, [byte], newCursorPosition);
          handleActiveDocumentUpdated();
        });
    }
    else if (initialCursorPosition
      && initialCursorPosition.mode === 'ascii'
      && initialCursorPosition.address !== null) {
      // update byte value at cursor position
      const newAddress = initialCursorPosition.address < activeDocument.size - 1 ?
        initialCursorPosition.address + 1 :
        initialCursorPosition.address;
      newCursorPosition = {
        address: newAddress,
        nibble: 1,
        mode: initialCursorPosition.mode,
      }
      activeDocument.updateBytes(initialCursorPosition.address, [value], newCursorPosition);
      handleActiveDocumentUpdated();
    }
  }

  /**
  * Processes keyboard events from the editor pane.
  * Uses the event's key (instead of keyCode) property to process the event, which may not work in old browsers.
  * @param {*} e KeyEvent
  */
  handleKeyDown(e) {
    // console.log({
    //   key: e.key,
    //   keyCode: e.keyCode,
    //   char: e.char,
    //   charCode: e.charCode,
    //   which: e.which,
    // });

    const key = e.key;
    if (this.arrowKeyDown !== null) {
      if (navigationKeys.includes(key)) {
        // prevent default behaviour (scroll bar scrolling)
        e.preventDefault();
        return false;
      }
      return true;
    }

    if (navigationKeys.includes(key)) {
      this.arrowKeyDown = key;
      this.firstRoundtrip = true;
      this.lastRoundtripTimestamp = performance.now();
      requestAnimationFrame(this.moveCursorInterval);

      // prevent default behaviour (scroll bar scrolling)
      e.preventDefault();
      return false;
    }

    if ('Tab' === key) {
      const cursorPosition = this.props.cursorPosition;
      if (cursorPosition.mode === 'bytes'
        && !e.shiftKey && !e.altKey
        && !e.ctrlKey && !e.metaKey) {
        e.preventDefault();
        e.stopPropagation();
        this.handleCursorPositionChanged(
          cursorPosition.address,
          cursorPosition.nibble,
          'ascii'
        );
        return false;
      }
      else if (cursorPosition.mode === 'ascii'
        && e.shiftKey && !e.altKey
        && !e.ctrlKey && !e.metaKey) {
        e.preventDefault();
        e.stopPropagation();
        this.handleCursorPositionChanged(
          cursorPosition.address,
          cursorPosition.nibble,
          'bytes'
        );
        return false;
      }
    }
  }

  handleKeyUp(e) {
    this.arrowKeyDown = null;
  }

  handleKeyPress(e) {
    // console.log({
    //   key: e.key,
    //   keyCode: e.keyCode,
    //   char: e.char,
    //   charCode: e.charCode,
    //   which: e.which,
    // });

    const key = e.key;

    if (key.length !== 1) {
      // only single character values represent valid input here
      return;
    }

    const activeDocumentKey = this.props.activeDocumentKey;
    const activeDocument = this.props.documents.find(d => d.key === activeDocumentKey);
    if (!activeDocument) {
      return;
    }
    const cursorPosition = activeDocument.cursorPosition;

    if (cursorPosition &&
      cursorPosition.mode === 'bytes' &&
      cursorPosition.address !== null &&
      hexDigits.includes(key.toLowerCase())) {
      // valid hex digit entered
      this.handleEditorEditEvent(hexDigits.indexOf(key.toLowerCase()));
    }
    else if (cursorPosition &&
      cursorPosition.mode === 'ascii' &&
      cursorPosition.address !== null) {
      if (printableAsciiCharacters.includes(key)) {
        // valid ASCII character entered
        this.handleEditorEditEvent(key.charCodeAt(0));
      }
    }
  }

  render() {
    const documents = this.props.documents;
    const activeDocumentKey = this.props.activeDocumentKey;
    const bytesPerRow = this.props.bytesPerRow;
    const byteGroupSize = this.props.byteGroupSize;
    const handleKeyDown = this.handleKeyDown;
    const handleKeyUp = this.handleKeyUp;
    const handleKeyPress = this.handleKeyPress;
    const handleCursorPositionChanged = this.handleCursorPositionChanged;
    const handleDocumentReadError = this.props.handleDocumentReadError;

    const tabs =
      documents.map(doc => {
        const rowRenderer = new RowRenderer(
          doc,
          bytesPerRow,
          byteGroupSize,
          doc.cursorPosition,
          doc.markedRange,
          handleCursorPositionChanged,
          (error) => handleDocumentReadError(doc.key, error)
        );

        return {
          label: doc.name + (doc.modified ? '*' : ''),
          key: doc.key,
          children: <ScrollArea
            ref={doc.key === activeDocumentKey ? this.scrollAreaRef : null}
            className="editor-pane-content"
            rowRenderer={rowRenderer}
            handleKeyDown={handleKeyDown}
            handleKeyUp={handleKeyUp}
            handleKeyPress={handleKeyPress}
            file={doc} // for notification of potential scroll position update
          />,
        }
      });

    return (
      <Card >
        <Tabs
          hideAdd={true}
          onChange={this.handleTabChange}
          onEdit={this.handleTabEdit}
          type="editable-card"
          activeKey={activeDocumentKey}
          items={tabs}
        >
        </Tabs>
      </Card>
    );
  }
}

export default EditorPane;