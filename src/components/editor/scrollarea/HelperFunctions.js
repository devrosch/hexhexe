/**
 * Translate Px position to relative position (scroll content or bar handle).
 * @param {*} totalAvailableHeightPx 
 * @param {*} topMarginPx 
 * @param {*} bottomMarginPx 
 * @param {*} visibleHeightPx 
 * @param {*} topPx 
 */
export function translateToRelativeTop(
  totalAvailableHeightPx,
  topMarginPx,
  bottomMarginPx,
  visibleHeightPx,
  topPx) {

  const availableHeightPx = totalAvailableHeightPx - topMarginPx - bottomMarginPx - visibleHeightPx;
  let relativeTop = (topPx - topMarginPx) / availableHeightPx;
  if (isNaN(relativeTop) || relativeTop < 0) {
    relativeTop = 0;
  }
  if (relativeTop > 1) {
    relativeTop = 1;
  }
  return relativeTop;
}

/**
 * Translate relative position to Px position (scroll content or bar handle).
 * @param {*} totalAvailableHeightPx 
 * @param {*} topMarginPx 
 * @param {*} bottomMarginPx 
 * @param {*} visibleHeightPx 
 * @param {*} topRelative 
 */
export function translateToPxTop(
  totalAvailableHeightPx,
  topMarginPx,
  bottomMarginPx,
  visibleHeightPx,
  topRelative) {

  const availableHeightPx = totalAvailableHeightPx - topMarginPx - bottomMarginPx - visibleHeightPx;
  if (availableHeightPx < 0) {
    return 0;
  }
  let topPx = topMarginPx + availableHeightPx * topRelative;
  if(isNaN(topPx) || topPx < topMarginPx) {
    topPx = topMarginPx;
  }
  const maxHeightPx = totalAvailableHeightPx - bottomMarginPx - visibleHeightPx;
  if (topPx > maxHeightPx) {
    topPx = maxHeightPx;
  }
  return topPx;
}

  /**
   * Determines the actual line height of child elements in px.
   * @param {*} element A DOM element.
   */
  export function determineLineHeight(element, className) {
    // as described in: https://stackoverflow.com/questions/4392868/javascript-find-divs-line-height-not-css-property-but-actual-line-height
    let tempElement = document.createElement(element.nodeName);
    // tempElement.setAttribute('style','border:1px solid black;');
    // tempElement.setAttribute('class','nibble');
    tempElement.setAttribute('class',className);
    tempElement.innerHTML = 'Updating ...';
    tempElement = element.parentNode.appendChild(tempElement);
    const offsetHeightPx = tempElement.offsetHeight;
    tempElement.parentNode.removeChild(tempElement);
    return offsetHeightPx;
  }

  /**
   * Calculates the px offset of the content, necessary to make the specified row visible.
   * The specified row is not necessarily the first visible row.
   * @param {*} rowIndex Index of row that should be visible.
   * @param {*} contentTopPx Current content px offset.
   * @param {*} offsetHeightPx Total content px height.
   * @param {*} rowHeightPx Height of a single row in px.
   * @param {*} numRowsTotal Total number of content rows.
   */
  export function rowIndexToRelativeScrollPosition(
      rowIndex,
      contentTopPx,
      offsetHeightPx,
      rowHeightPx,
      numRowsTotal
      ) {
    const totalHeightPx = numRowsTotal * rowHeightPx;

    const firstFullyVisibleRowIndex = Math.ceil(contentTopPx / rowHeightPx);
    const lastFullyVisibleRowIndex = Math.floor(
      (contentTopPx + offsetHeightPx - rowHeightPx) / rowHeightPx);

    if (
      (rowIndex >= firstFullyVisibleRowIndex && rowIndex <= lastFullyVisibleRowIndex)
      || (lastFullyVisibleRowIndex < firstFullyVisibleRowIndex)) {
      // row already fully visible or no row visible => do nothing
      return -1;
    }

    let rowOffsetPx = 0;
    if (rowIndex < firstFullyVisibleRowIndex) {
      // make row first fully visible row
      rowOffsetPx = rowHeightPx * rowIndex;
    }
    else if (rowIndex > lastFullyVisibleRowIndex) {
      // make row last fully visible row
      rowOffsetPx = Math.ceil(rowHeightPx * (rowIndex + 1) - offsetHeightPx);
    }
    const newRelativeScrollPosition = translateToRelativeTop(totalHeightPx, 0, 0, offsetHeightPx, rowOffsetPx);
    return newRelativeScrollPosition;
  }
