import React, { Component } from 'react';
import './ScrollBarHandle.css';
import './Unselectable.css';
import {
  translateToRelativeTop,
  translateToPxTop
} from '../HelperFunctions';

//#region constants

/**
 * Minimum scroll bar handle height in px.
 */
const minScrollBarHandleHeightPx = 16;
/**
 * Initial scroll bar handle top in px. Should correspond to initial button up height in px to avoid short glitch before component is mounted.
 */
const initialScrollBarHandleTop = 19.2 // 1.2em

//#endregion constants

/**
 * Encapsules a scroll bar handle.
 */
class ScrollBarHandle extends Component {
  constructor(props) {
    super(props);

    // mount flag to only work with refs after component has mounted
    this._isMounted = false;

    // the scroll bar area including scroll bar handle and up/down arrows
    this.scrollBarHandleRef = React.createRef();

    this.state = {
      mouseDownYPosition: 0,
      mouseDownScrollBarHandleTop: 19.2, // 1.2em
      dragging: false,
    };

    // calculate methods
    this.calculateScrollBarHandleHeight = this.calculateScrollBarHandleHeight.bind(this);
    this.calculateScrollBarHandleTop = this.calculateScrollBarHandleTop.bind(this);
    this.calculateDragNewRelativeTop = this.calculateDragNewRelativeTop.bind(this);
    // event handlers mouse
    this.onScrollBarHandleMouseDown = this.onScrollBarHandleMouseDown.bind(this);
    this.onScrollBarHandleMouseUp = this.onScrollBarHandleMouseUp.bind(this);
    this.onScrollBarHandleDrag = this.onScrollBarHandleDrag.bind(this);
    // event handlers touch
    this.onScrollBarHandleTouchStart = this.onScrollBarHandleTouchStart.bind(this);
    this.onScrollBarHandleTouchEnd = this.onScrollBarHandleTouchEnd.bind(this);
    this.onScrollBarHandleTouchMove = this.onScrollBarHandleTouchMove.bind(this);
    // API
    this.getTopPx = this.getTopPx.bind(this);
    this.getHeightPx = this.getHeightPx.bind(this);
  }

  //#region lifecycle

  componentDidMount() {
    this._isMounted = true;
    window.addEventListener("mouseup", this.onScrollBarHandleMouseUp);
    window.addEventListener("mousemove", this.onScrollBarHandleDrag);
  }

  componentWillUnmount() {
    window.removeEventListener("mouseup", this.onScrollBarHandleMouseUp);
    window.removeEventListener("mousemove", this.onScrollBarHandleDrag);
  }

  //#endregion lifecycle

  //#region calculations

  /**
   * Calculate scroll bar handle height based on visible content height vs. total height.
   */
  calculateScrollBarHandleHeight() {
    if (!this._isMounted) {
      return 1;
    }
    const scrollBarHeight = this.props.scrollBarOffsetHeight;
    const arrowUpHeight = this.props.scrollBarArrowUpOffsetHeight;
    const arrowDownHeight = this.props.scrollBarArrowDownOffsetHeight;
    const scrollBarAvailableHeight = scrollBarHeight - arrowUpHeight - arrowDownHeight;
    const scrollContentVisibleFraction =
      this.props.scrollContentScrollHeight > 0 ?
        this.props.scrollContentClientHeight / this.props.scrollContentScrollHeight :
        1;
    let scrollBarHandleHeight = isNaN(scrollContentVisibleFraction)
      || scrollContentVisibleFraction === 0
      || scrollContentVisibleFraction > 1 ?
      scrollBarAvailableHeight :
      scrollBarAvailableHeight * scrollContentVisibleFraction;
    if (scrollBarHandleHeight < minScrollBarHandleHeightPx) {
      scrollBarHandleHeight = minScrollBarHandleHeightPx;
    }

    return scrollBarHandleHeight;
  }

  /**
   * Calculates the scroll bar Px top according to the relative position.
   */
  calculateScrollBarHandleTop() {
    if (!this._isMounted) {
      return initialScrollBarHandleTop;
    }

    // content relative -> Px
    const scrollBarHeightPx = this.props.scrollBarOffsetHeight;
    const arrowUpHeightPx = this.props.scrollBarArrowUpOffsetHeight;
    const arrowDownHeightPx = this.props.scrollBarArrowDownOffsetHeight;
    // do not use this.scrollBarHandleRef.current.offsetHeight as it may not yet have been updated
    const scrollBarHandleHeightPx = this.calculateScrollBarHandleHeight();

    const scrollBarHandleTopPx = translateToPxTop(
      scrollBarHeightPx,
      arrowUpHeightPx,
      arrowDownHeightPx,
      scrollBarHandleHeightPx,
      this.props.relativeScrollPosition
    );

    return scrollBarHandleTopPx;
  }

  /**
   * Calculates the new relative top during dragging (mouse or touch).
   */
  calculateDragNewRelativeTop(currentDragYPx) {
    if (!this._isMounted) {
      return initialScrollBarHandleTop;
    }

    // delta
    const draggedDeltaY = currentDragYPx - this.state.mouseDownYPosition;
    // scroll bar
    const scrollBarHeightPx = this.props.scrollBarOffsetHeight;
    const arrowUpHeightPx = this.props.scrollBarArrowUpOffsetHeight;
    const arrowDownHeightPx = this.props.scrollBarArrowDownOffsetHeight;
    const scrollBarHandleHeightPx = this.scrollBarHandleRef.current.offsetHeight;
    const scrollBarHandleTopPx = this.state.mouseDownScrollBarHandleTop + draggedDeltaY;

    // Px -> relative
    const newRelativeTop = translateToRelativeTop(
      scrollBarHeightPx,
      arrowUpHeightPx,
      arrowDownHeightPx,
      scrollBarHandleHeightPx,
      scrollBarHandleTopPx
    );

    return newRelativeTop;
  }

  //#endregion calculations

  //#region scroll bar handle events

  onScrollBarHandleMouseDown(e) {
    // prevent propagating event to parent, which would cause an unintended reaction there
    e.stopPropagation();

    this.setState({
      mouseDownYPosition: e.clientY,
      mouseDownScrollBarHandleTop: this.scrollBarHandleRef.current.offsetTop,
      dragging: true
    });
  }

  onScrollBarHandleTouchStart(e) {
    // https://www.html5rocks.com/en/mobile/touch/
    // prevent propagating event to parent, which would cause an unintended reaction there
    e.stopPropagation();

    if (e.changedTouches.length !== 1) {
      // only single touch supported
      return;
    }
    // https://stackoverflow.com/questions/6073505/what-is-the-difference-between-screenx-y-clientx-y-and-pagex-y
    this.setState({
      mouseDownYPosition: e.changedTouches[0].clientY,
      mouseDownScrollBarHandleTop: this.scrollBarHandleRef.current.offsetTop,
      dragging: true
    });
  }

  onScrollBarHandleMouseUp(e) {
    this.setState({
      dragging: false
    });
  }

  onScrollBarHandleTouchEnd(e) {
    // https://www.html5rocks.com/en/mobile/touch/
    this.setState({
      dragging: false
    });
  }

  onScrollBarHandleDrag(e) {
    if (this.state.dragging) {
      const newRelativeTop = this.calculateDragNewRelativeTop(e.clientY);
      this.props.handleRelativeScrollPositionChange(newRelativeTop);
    }
  }

  onScrollBarHandleTouchMove(e) {
    if (this.state.dragging) {
      const newRelativeTop = this.calculateDragNewRelativeTop(e.changedTouches[0].clientY);
      this.props.handleRelativeScrollPositionChange(newRelativeTop);
    }
  }

  //#endregion scroll bar handle events

  //#region API

  getTopPx() {
    return this.calculateScrollBarHandleTop();
  }

  getHeightPx() {
    return this.calculateScrollBarHandleHeight();
  }

  //#endregion API

  render() {
    const scrollBarHandleTop = this.calculateScrollBarHandleTop();
    const scrollBarHandleHeight = this.calculateScrollBarHandleHeight();
    const visible = this.props.visible;

    if (!visible) {
      return <div />;
    }

    return (
      <span
        className="scroll-bar-handle unselectable"
        ref={this.scrollBarHandleRef}
        onMouseDown={this.onScrollBarHandleMouseDown}
        onMouseMove={this.onScrollBarHandleDrag}
        onTouchStart={this.onScrollBarHandleTouchStart}
        onTouchEnd={this.onScrollBarHandleTouchEnd}
        onTouchCancel={this.onScrollBarHandleTouchEnd}
        onTouchMove={this.onScrollBarHandleTouchMove}
        style={{ top: scrollBarHandleTop, height: scrollBarHandleHeight }}
      >
      </span>
    );
  }
}

export default ScrollBarHandle;
