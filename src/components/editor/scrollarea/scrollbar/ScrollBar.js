import React, { Component } from 'react';
import './Unselectable.css';
import './ScrollBar.css';
import ScrollBarButton from './ScrollBarButton';
import ScrollBarHandle from './ScrollBarHandle';
import { translateToRelativeTop } from '../HelperFunctions';

/**
 * Encapsules a Y scroll bar.
 */
class ScrollBar extends Component {
  constructor(props) {
    super(props);

    // mount flag to only work with refs after component has mounted
    this._isMounted = false;

    // the scroll bar area including scroll bar handle and up/down arrows
    this.scrollBarRef = React.createRef();
    this.scrollBarHandleRef = React.createRef();
    this.scrollBarArrowUpRef = React.createRef();
    this.scrollBarArrowDownRef = React.createRef();
    // calculate function
    this.calculateNewRelativePosition = this.calculateNewRelativePosition.bind(this);
    // event handlers
    this.onScrollBarMouseDown = this.onScrollBarMouseDown.bind(this);
    this.onScrollBarTouchStart = this.onScrollBarTouchStart.bind(this);
  }

  //#region lifecycle

  componentDidMount() {
    this._isMounted = true;
  }

  //#endregion lifecycle

  calculateNewRelativePosition(clientY, boundingRectangle) {
    const scrollBarOffsetHeight = this.scrollBarRef.current.offsetHeight;
    const scrollBarArrowUpOffsetHeight = this.scrollBarArrowUpRef.current.getOffsetHeight();
    const scrollBarArrowDownOffsetHeight = this.scrollBarArrowDownRef.current.getOffsetHeight();
    const scrollBarHandleHeight = this.scrollBarHandleRef.current.getHeightPx();

    // see: https://stackoverflow.com/questions/3234256/find-mouse-position-relative-to-element
    const scrollBarTop = boundingRectangle.top;
    const newScrollBarHandleTopPx = clientY - scrollBarTop - scrollBarHandleHeight / 2;

    const newRelativePosition = translateToRelativeTop(
      scrollBarOffsetHeight,
      scrollBarArrowUpOffsetHeight,
      scrollBarArrowDownOffsetHeight,
      scrollBarHandleHeight,
      newScrollBarHandleTopPx
    );

    return newRelativePosition;
  }

  //#region event handlers

  /**
   * Handles clicks on scroll bar outside button or handle.
   * @param {*} e The mouse down event.
   */
  onScrollBarMouseDown(e) {
    if (!this._isMounted) {
      return;
    }

    const boundingRectangle = e.target.getBoundingClientRect();
    const newRelativePosition = this.calculateNewRelativePosition(e.clientY, boundingRectangle);
    this.props.handleRelativeScrollPositionChange(newRelativePosition);
  }

  /**
   * Handles touches on scroll bar outside button or handle.
   * @param {*} e The mouse down event.
   */
  onScrollBarTouchStart(e) {
    if (!this._isMounted) {
      return;
    }

    const boundingRectangle = e.target.getBoundingClientRect();
    const newRelativePosition = this.calculateNewRelativePosition(e.changedTouches[0].clientY, boundingRectangle);
    this.props.handleRelativeScrollPositionChange(newRelativePosition);
  }

  //#endregion event handler

  render() {
    const scrollBarOffsetHeight = this._isMounted ? this.scrollBarRef.current.offsetHeight : 1;
    const scrollBarArrowUpOffsetHeight = this._isMounted ? this.scrollBarArrowUpRef.current.getOffsetHeight() : 1;
    const scrollBarArrowDownOffsetHeight = this._isMounted ? this.scrollBarArrowDownRef.current.getOffsetHeight() : 1;

    const scrollContentClientHeight = this._isMounted ? this.props.scrollContentClientHeight : 1;
    const scrollContentScrollHeight = this._isMounted ? this.props.scrollContentScrollHeight : 1;
    const relativeScrollPosition = this._isMounted ? this.props.relativeScrollPosition : 0;

    // do not render scroll bar handle until scroll content has been rendered and has meaningful dimensions
    let renderScrollBarHandle = true;
    if (scrollBarOffsetHeight === 0 ||
      scrollContentClientHeight === 0 ||
      scrollContentScrollHeight === 0
    ) {
      renderScrollBarHandle = false;
    }

    // console.log({
    //   'scrollBarRef': this.scrollBarRef.current,
    //   'scrollBarOffsetHeight': scrollBarOffsetHeight,
    //   'scrollBarArrowUpOffsetHeight': scrollBarArrowUpOffsetHeight,
    //   'scrollBarArrowDownOffsetHeight': scrollBarArrowDownOffsetHeight,
    //   'scrollContentClientHeight': scrollContentClientHeight,
    //   'scrollContentScrollHeight': scrollContentScrollHeight,
    //   'relativeScrollPosition': relativeScrollPosition
    // });

    return (
      <div className="scroll-bar"
        ref={this.scrollBarRef}
        onMouseDown={this.onScrollBarMouseDown}
        onTouchStart={this.onScrollBarTouchStart}
      >
        <ScrollBarButton
          ref={this.scrollBarArrowUpRef}
          className={'scroll-bar-button scroll-bar-button-up unselectable'}
          handleScrollContentPxInterval={(delta) => this.props.handleScrollContentPxInterval(-1 * delta)}
          handleClearScrollContentPxInterval={this.props.handleClearScrollContentPxInterval}
        >▲
        </ScrollBarButton>
        <ScrollBarHandle
          ref={this.scrollBarHandleRef}
          visible={renderScrollBarHandle}
          scrollContentClientHeight={scrollContentClientHeight}
          scrollContentScrollHeight={scrollContentScrollHeight}
          scrollBarOffsetHeight={scrollBarOffsetHeight}
          scrollBarArrowUpOffsetHeight={scrollBarArrowUpOffsetHeight}
          scrollBarArrowDownOffsetHeight={scrollBarArrowDownOffsetHeight}
          relativeScrollPosition={relativeScrollPosition}
          handleRelativeScrollPositionChange={this.props.handleRelativeScrollPositionChange}
        />
        <ScrollBarButton
          ref={this.scrollBarArrowDownRef}
          className={'scroll-bar-button scroll-bar-button-down unselectable'}
          handleScrollContentPxInterval={this.props.handleScrollContentPxInterval}
          handleClearScrollContentPxInterval={this.props.handleClearScrollContentPxInterval}
        >▼
        </ScrollBarButton>
      </div>

    );
  }
}

export default ScrollBar;
