import React, { Component } from 'react';
import './ScrollBarButton.css';
import './Unselectable.css';

/**
 * The delta in px the content is moved (repeatedly) when a scroll bar button is pressed.
 */
const scrollBarButtonDelta = 16;

/**
 * Encapsules a scroll bar button.
 */
class ScrollBarButton extends Component {
  constructor(props) {
    super(props);

    // the scroll bar button
    this.scrollBarButtonSpanRef = React.createRef();

    // event handlers mouse
    this.onScrollBarButtonMouseDown = this.onScrollBarButtonMouseDown.bind(this);
    this.onScrollBarButtonMouseUp = this.onScrollBarButtonMouseUp.bind(this);
    // event handlers touch
    this.onScrollBarButtonTouchStart = this.onScrollBarButtonTouchStart.bind(this);
    this.onScrollBarButtonTouchEnd = this.onScrollBarButtonTouchEnd.bind(this);
    this.onScrollBarButtonTouchCancel = this.onScrollBarButtonTouchCancel.bind(this);
    // API
    this.getOffsetHeight = this.getOffsetHeight.bind(this);
  }

  //#region lifecycle

  componentDidMount() {
    window.addEventListener("mouseup", this.onScrollBarButtonMouseUp);
  }

  componentWillUnmount() {
    window.removeEventListener("mouseup", this.onScrollBarButtonMouseUp);
  }

  //#endregion lifecycle

  //#region event handlers

  onScrollBarButtonMouseDown(e) {
    // prevent propagating event to parent, which would cause an unintended reaction there
    e.stopPropagation();
    this.props.handleScrollContentPxInterval(scrollBarButtonDelta);
  }

  onScrollBarButtonTouchStart(e) {
    // prevent propagating event to parent, which would cause an unintended reaction there
    e.stopPropagation();
    this.props.handleScrollContentPxInterval(scrollBarButtonDelta);
  }

  onScrollBarButtonMouseUp() {
    this.props.handleClearScrollContentPxInterval();
  }

  onScrollBarButtonTouchEnd(e) {
    e.preventDefault(); // do not propagate to cause click() event
    this.props.handleClearScrollContentPxInterval();
  }
  
  onScrollBarButtonTouchCancel(e) {
    this.props.handleClearScrollContentPxInterval();
  }  
  
  //#endregion event handler

  //#region API

  getOffsetHeight() {
    return this.scrollBarButtonSpanRef.current.offsetHeight;
  }

  //#endregion API

  render() {
    return (
      <span
        className={this.props.className}
        ref={this.scrollBarButtonSpanRef}
        onMouseDown={this.onScrollBarButtonMouseDown}
        onTouchStart={this.onScrollBarButtonTouchStart}
        onTouchEnd={this.onScrollBarButtonTouchEnd}
        onTouchCancel={this.onScrollBarButtonTouchCancel}
      >
        <span
          className="scroll-bar-button-text"
        >
          {this.props.children}
        </span>
      </span>
    );
  }
}

export default ScrollBarButton;
