import React, { Component } from 'react';
import './ScrollArea.css';
import { translateToRelativeTop } from './HelperFunctions';
import ScrollBar from './scrollbar/ScrollBar';
import ScrollContentArea from './scrollcontent/ScrollContentArea';

//#region constants

/**
 * The interval in ms at which the content is scrolled while a scroll bar button is pressed.
 */
const scrollBarButtonInterval = 100;
/**
 * The delta in px the content is moved (repeatedly) when the mouse wheel is turned.
 */
const mouseWheelDelta = 32;

//#endregion constants

/**
 * Encapsules a scroll area including scrollable content and Y scroll bar.
 */
class ScrollArea extends Component {
  constructor(props) {
    super(props);

    // mount flag to only work with refs after component has mounted
    this._isMounted = false;
    // the scroll area height, to periodically check for changes and forceUpdate() if changed
    this._scrollAreaHeight = 0;

    // the scroll area including content and scroll bar
    this.scrollAreaRef = React.createRef();
    // the actual scroll content
    this.scrollContentAreaRef = React.createRef();

    this.state = {
      relativeScrollPosition: 0
    };
    // API
    this.focus = this.focus.bind(this);
    // helper methods
    this.scrollContentPx = this.scrollContentPx.bind(this);
    this.scrollContentPxInterval = this.scrollContentPxInterval.bind(this);
    // event handlers
    this.onWheelEvent = this.onWheelEvent.bind(this);
    this.handleRelativeScrollPositionChange = this.handleRelativeScrollPositionChange.bind(this);
  }

  //#region API

  focus() {
    this.scrollAreaRef.current.focus();
  }

  //#endregion

  //#region lifecycle

  componentDidMount() {
    this._isMounted = true;
    // in the future this might be done via ResizeObserver
    // https://wicg.github.io/ResizeObserver/
    this.elementSizeChangeWatcher = setInterval(
      () => {
        const scrollAreaHeight = this.scrollAreaRef.current.offsetHeight;
        if (this._scrollAreaHeight !== scrollAreaHeight) {
          this.forceUpdate();
          this._scrollAreaHeight = this.scrollAreaRef.current.offsetHeight;
        }
      },
      100
    );

    // in particular, update scroll bar handle position/size
    this.forceUpdate();
  }

  componentWillUnmount() {
    clearInterval(this.elementSizeChangeWatcher);
  }

  componentDidUpdate(prevProps) {
    if (this.props.file !== prevProps.file) {
      this.setState({ relativeScrollPosition: 0 });
    }
  }

  //#endregion lifecycle

  //#region scroll content

  scrollContentPx(deltaYPx) {
    // content
    const contentVisibleHeightPx = this.scrollContentAreaRef.current.getClientHeight();
    // if elements with margin are included, the scroll height does not include the bottom margin
    // see: https://stackoverflow.com/questions/11602914/scrollheight-gives-incorrect-value-when-height-property-is-provided-in-css
    // see: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing
    const contentTotalHeightPx = this.scrollContentAreaRef.current.getScrollHeight();
    const contentTopPx = -this.scrollContentAreaRef.current.getOffsetTop();
    const contentNewTopPx = contentTopPx + deltaYPx
    // content Px -> relative
    const newRelativeTop = translateToRelativeTop(
      contentTotalHeightPx,
      0,
      0,
      contentVisibleHeightPx,
      contentNewTopPx
    );

    this.setState({ relativeScrollPosition: newRelativeTop });
  }

  scrollContentPxInterval(delatyYPx) {
    this.scrollContentPx(delatyYPx);
    this.scrollBarButtonInterval = setInterval(
      () => {
        this.scrollContentPx(delatyYPx);
      },
      scrollBarButtonInterval
    );
  }

  //#endregion scroll content

  //#region content events

  onWheelEvent(e) {
    // see https://stackoverflow.com/questions/14926366/mousewheel-event-in-modern-browsers
    // see https://developer.mozilla.org/en-US/docs/Web/Events/wheel
    const deltaY = Math.sign(e.deltaY); // +1 or -1, depending on wheel direction
    const deltaYPx = deltaY * mouseWheelDelta;
    this.scrollContentPx(deltaYPx);
    this.forceUpdate();
  }

  //#endregion content events

  //#region API

  /**
   * Scrolls content, so that specified row is visible.
   * @param {*} rowIndex Index of row that should be visible.
   */
  scrollToRow(rowIndex) {
    this.scrollContentAreaRef.current.scrollToRow(rowIndex);
  }

  /**
   * The number of visible rows.
   */
  get numVisibleRows() {
    const scrollContentOffsetHeightPx = this._isMounted ? this.scrollContentAreaRef.current.getOffsetHeight() : 0;
    const scrollContentRowHeightPx = this._isMounted ? this.scrollContentAreaRef.current.getRowHeightPx() : 1;
    const numVisibleRows = Math.floor(scrollContentOffsetHeightPx / scrollContentRowHeightPx);
    return numVisibleRows;
  }

  //#endregion

  handleRelativeScrollPositionChange(newRelativeScrollPosition) {
    this.setState({
      relativeScrollPosition: newRelativeScrollPosition
    });
  }

  render() {
    const relativeScrollPosition = this.state.relativeScrollPosition;
    const handleClearScrollContentPxInterval = () => clearInterval(this.scrollBarButtonInterval);
    const rowRenderer = this.props.rowRenderer;
    const handleKeyUp = this.props.handleKeyUp;
    const handleKeyDown = this.props.handleKeyDown;
    const handleKeyPress = this.props.handleKeyPress;
    const scrollContentClientHeight = this._isMounted ? this.scrollContentAreaRef.current.getClientHeight() : 0;
    // const scrollContentScrollHeight = this._isMounted ? this.scrollContentAreaRef.current.getScrollHeight() : 0;
    const scrollContentScrollHeight = this._isMounted ? this.scrollContentAreaRef.current.getRowHeightPx() * rowRenderer.numRows : 0;

    return (
      <div
        className={'scroll-area ' + this.props.className}
        ref={this.scrollAreaRef}
        onWheel={this.onWheelEvent}
        onKeyDown={handleKeyDown}
        onKeyUp={handleKeyUp}
        onKeyPress={handleKeyPress}
        tabIndex="0"
      >
        <ScrollContentArea
          ref={this.scrollContentAreaRef}
          relativeScrollPosition={this.state.relativeScrollPosition}
          handleRelativeScrollPositionChange={this.handleRelativeScrollPositionChange}
          rowRenderer={rowRenderer}
        />
        <ScrollBar
          relativeScrollPosition={relativeScrollPosition}
          scrollContentClientHeight={scrollContentClientHeight}
          scrollContentScrollHeight={scrollContentScrollHeight}
          handleRelativeScrollPositionChange={this.handleRelativeScrollPositionChange}
          handleScrollContentPxInterval={this.scrollContentPxInterval}
          handleClearScrollContentPxInterval={handleClearScrollContentPxInterval}
        />
      </div>
    );
  }
}

export default ScrollArea;
