import React, { Component } from 'react';
import {
  translateToPxTop,
  determineLineHeight,
  rowIndexToRelativeScrollPosition
} from '../HelperFunctions';
import './ScrollContent.css';
import '../../row/Address.css';

/**
 * Class name to be applied to dummy element for determining line height.
 * Should be or match output of actual rendered elements.
 */
const dummyRowClassName = 'address';

/**
 * Encapsules the scroll content inside a scroll area.
 */
class PartialRenderScrollContent extends Component {
  constructor(props) {
    super(props);

    // mount flag to only work with refs after component has mounted
    this._isMounted = false;
    this.scrollContentRef = React.createRef();
    // calculate methods
    this.calculateContentTop = this.calculateContentTop.bind(this);
    this.calculateDisplayProperties = this.calculateDisplayProperties.bind(this);
    // API
    this.getClientHeight = this.getClientHeight.bind(this);
    this.getScrollHeight = this.getScrollHeight.bind(this);
    this.getOffsetHeight = this.getOffsetHeight.bind(this);
    this.getOffsetTop = this.getOffsetTop.bind(this);
    this.scrollToRow = this.scrollToRow.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  /**
   * Calculates the content Px top according to the relative position.
   * @param {*} relativeTop Relative scroll position between 0 and 1.
   */
  calculateContentTop() {
    if (!this._isMounted) {
      return 0;
    }

    // content relative -> Px
    const contentTotalHeightPx = this.getScrollHeight();
    const contentVisibleHeightPx = this.getClientHeight();

    const contentNewTopPx = translateToPxTop(
      contentTotalHeightPx,
      0,
      0,
      contentVisibleHeightPx,
      this.props.relativeScrollPosition
    );

    return -contentNewTopPx;
  }

  calculateDisplayProperties() {
    if (!this._isMounted) {
      return {
        firstRowIndex: 0,
        offsetPx: 0,
        numVisibleRows: 0
      };
    }

    const lineHeightPx = determineLineHeight(this.scrollContentRef.current, dummyRowClassName);
    const contentTopPx = -this.getOffsetTop();
    const firstRowIndex = Math.floor(contentTopPx / lineHeightPx);
    const remainderPx = contentTopPx % lineHeightPx;

    const numLinesTotal = this.props.rowRenderer.numRows;
    const viewportHeightPx = this.getOffsetHeight()
    let numVisibleRows = Math.ceil(viewportHeightPx / lineHeightPx) + 1;

    if (firstRowIndex + numVisibleRows > numLinesTotal) {
      numVisibleRows = numLinesTotal - firstRowIndex;
    }

    return {
      firstRowIndex: firstRowIndex,
      offsetPx: remainderPx,
      numVisibleRows: numVisibleRows,
      lineHeightPx: lineHeightPx,
      viewportHeightPx: viewportHeightPx,
      contentTopPx: contentTopPx
    };
  }

  //#region API

  /**
   * Returns the components client height property.
   */
  getClientHeight() {
    return this.scrollContentRef.current.clientHeight;
  }

  /**
   * Returns the components scroll height property.
   */
  getScrollHeight() {
    const numLines = this.props.rowRenderer.numRows;
    const lineHeightPx = determineLineHeight(this.scrollContentRef.current, dummyRowClassName);
    let scrollHeight = numLines * lineHeightPx;
    if (scrollHeight < this.getOffsetHeight()) {
      scrollHeight = this.getOffsetHeight();
    }
    return scrollHeight;
  }

  /**
   * Returns the components offset height property.
   */
  getOffsetHeight() {
    return this.scrollContentRef.current.offsetHeight;
  }

  /**
   * Returns the components offset top property.
   */
  getOffsetTop() {
    const contentVisibleHeightPx = this.getClientHeight();
    const numLines = this.props.rowRenderer.numRows;
    const lineHeightPx = determineLineHeight(this.scrollContentRef.current, dummyRowClassName);
    const contentTotalHeightPx = numLines * lineHeightPx;

    const contentTopPx = translateToPxTop(
      contentTotalHeightPx,
      0,
      0,
      contentVisibleHeightPx,
      this.props.relativeScrollPosition
    );

    return -contentTopPx;
  }

  /**
   * Scrolls content, so that specified row is visible.
   * @param {*} rowIndex Index of row that should be visible.
   */
  scrollToRow(rowIndex) {
    const contentTopPx = -this.getOffsetTop();
    const viewportHeightPx = this.getOffsetHeight();
    const rowHeightPx = determineLineHeight(this.scrollContentRef.current, dummyRowClassName);
    const numRowsTotal = this.props.rowRenderer.numRows;
    const newRelativeScrollPosition = rowIndexToRelativeScrollPosition(
      rowIndex, contentTopPx, viewportHeightPx, rowHeightPx, numRowsTotal);
    if (newRelativeScrollPosition >= 0) {
      // -1 indicates that row is already visible
      this.props.handleRelativeScrollPositionChange(newRelativeScrollPosition);
    }
  }

  /**
   * Returns the height of a single rendered row in px.
   */
  getRowHeightPx() {
    const rowHeightPx = determineLineHeight(this.scrollContentRef.current, dummyRowClassName);
    return rowHeightPx;
  }

  //#endregion API

  render() {
    const displayProperties = this.calculateDisplayProperties();
    const rowRenderer = this.props.rowRenderer;
    const displayRows = rowRenderer.render(
      displayProperties.firstRowIndex,
      displayProperties.firstRowIndex + displayProperties.numVisibleRows
    );

    return (
      <div className="scroll-content"
        ref={this.scrollContentRef}
        style={isNaN(displayProperties.offsetPx) ? {} : { top: -displayProperties.offsetPx }}
      >
        {displayRows}
      </div>
    );
  }
}

export default PartialRenderScrollContent;
