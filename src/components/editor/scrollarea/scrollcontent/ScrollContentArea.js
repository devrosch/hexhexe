import React, { Component } from 'react';
import './ScrollContentArea.css';
import { translateToRelativeTop } from '../HelperFunctions';
// import ScrollContent from './ScrollContent';
import ScrollContent from './PartialRenderScrollContent';

//#region constants

/**
 * The interval in ms at which the content is scrolled while a scroll bar button is pressed.
 */
const scrollBarButtonInterval = 100;
/**
 * The delta in px the content is moved (repeatedly) when the mouse wheel is turned.
 */
const mouseWheelDelta = 32;

//#endregion constants

/**
 * Encapsules a scroll area including scrollable content and Y scroll bar.
 */
class ScrollContentArea extends Component {
  // relativeScrollPosition, handleRelativeScrollPositionChange(newRelativeTop)
  constructor(props) {
    super(props);

    // mount flag to only work with refs after component has mounted
    this._isMounted = false;

    // the actual scroll content
    this.scrollContentRef = React.createRef();

    this.state= {
      // content
      contentTouchStartYPosition: 0,
      contentTouchStartTop: 0,
      contentDragging: false,
    };

    // calculate methods
    this.calculateContentDragNewRelativeTop = this.calculateContentDragNewRelativeTop.bind(this);
    // helper methods
    this.scrollContentPx = this.scrollContentPx.bind(this);
    this.scrollContentPxInterval = this.scrollContentPxInterval.bind(this);
    this.onWheelEvent = this.onWheelEvent.bind(this);
    this.onContentTouchStart = this.onContentTouchStart.bind(this);
    this.onContentTouchEnd = this.onContentTouchEnd.bind(this);
    this.onContentTouchMove = this.onContentTouchMove.bind(this);
  }

  //#region lifecycle

  componentDidMount() {
    this._isMounted = true;
  }

  //#endregion lifecycle

  //#region calculations

  /**
   * Calculates the new relative top during content dragging (touch).
   */
  calculateContentDragNewRelativeTop(currentDragYPx) {
    if (!this._isMounted) {
      return 0;
    }

    // delta
    const draggedDeltaY = currentDragYPx - this.state.contentTouchStartYPosition;
    // content
    const contentHeightPx = this.scrollContentRef.current.getScrollHeight();
    const contentVisibleHeightPx = this.scrollContentRef.current.getOffsetHeight();
    const contentTopPx = this.state.contentTouchStartTop + draggedDeltaY;

    // Px -> relative
    const newRelativeTop = translateToRelativeTop(
      contentHeightPx,
      0,
      0,
      contentVisibleHeightPx,
      -contentTopPx
    );

    return newRelativeTop;
  }

  //#endregion calculations

  //#region scroll content

  scrollContentPx(deltaYPx) {
    // content
    const contentVisibleHeightPx = this.scrollContentRef.current.getClientHeight();
    // if elements with margin are included, the scroll height does not include the bottom margin
    // see: https://stackoverflow.com/questions/11602914/scrollheight-gives-incorrect-value-when-height-property-is-provided-in-css
    // see: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing
    const contentTotalHeightPx = this.scrollContentRef.current.getScrollHeight();
    const contentTopPx = -this.scrollContentRef.current.getOffsetTop();
    const contentNewTopPx = contentTopPx + deltaYPx
    // content Px -> relative
    const newRelativeTop = translateToRelativeTop(
      contentTotalHeightPx,
      0,
      0,
      contentVisibleHeightPx,
      contentNewTopPx
    );

    this.props.handleRelativeScrollPositionChange(newRelativeTop);
  }

  scrollContentPxInterval(delatyYPx) {
    this.scrollContentPx(delatyYPx);
    this.scrollBarButtonInterval = setInterval(
      () => {
        this.scrollContentPx(delatyYPx);
      },
      scrollBarButtonInterval
    );
  }

  //#endregion scroll content

  //#region content events

  onWheelEvent(e) {
    // see https://stackoverflow.com/questions/14926366/mousewheel-event-in-modern-browsers
    // see https://developer.mozilla.org/en-US/docs/Web/Events/wheel
    const deltaY = Math.sign(e.deltaY); // +1 or -1, depending on wheel direction
    const deltaYPx = deltaY * mouseWheelDelta;
    this.scrollContentPx(deltaYPx);
    this.forceUpdate();
  }

  onContentTouchStart(e) {
    if (e.changedTouches.length !== 1) {
      // only single touch supported
      return;
    }

    this.setState({
      contentTouchStartYPosition: e.changedTouches[0].clientY,
      contentTouchStartTop: this.scrollContentRef.current.getOffsetTop(),
      contentDragging: true
    });
  }

  onContentTouchMove(e) {
    const newRelativeTop = this.calculateContentDragNewRelativeTop(e.changedTouches[0].clientY);
    this.props.handleRelativeScrollPositionChange(newRelativeTop);
  }

  onContentTouchEnd(e) {
    this.setState({
      contentDragging: false
    });
  }

  //#endregion content events

  //#region API
  
  /**
   * Returns the components client height property.
   */
  getClientHeight() {
    return this.scrollContentRef.current.getClientHeight();
  }

  /**
   * Returns the components scroll height property.
   */
  getScrollHeight() {
    return this.scrollContentRef.current.getScrollHeight();
  }

  /**
   * Returns the components offset height property.
   */
  getOffsetHeight() {
    return this.scrollContentRef.current.getOffsetHeight();
  }

  /**
   * Returns the components offset top property.
   */
  getOffsetTop() {
    return this.scrollContentRef.current.getOffsetTop();
  }

  /**
   * Returns the height of a single rendered row in px.
   */
  getRowHeightPx() {
    return this.scrollContentRef.current.getRowHeightPx();
  }

  /**
   * Scrolls content, so that specified row is visible.
   * @param {*} rowIndex Index of row that should be visible.
   */
  scrollToRow(rowIndex) {
    this.scrollContentRef.current.scrollToRow(rowIndex);
  }

  //#endregion API

  render() {
    const relativeScrollPosition = this.props.relativeScrollPosition;
    const rowRenderer = this.props.rowRenderer;
    const handleRelativeScrollPositionChange = this.props.handleRelativeScrollPositionChange;

    return (
      <div className="scroll-content-area"
        onTouchStart={this.onContentTouchStart}
        onTouchMove={this.onContentTouchMove}
        onTouchEnd={this.onContentTouchEnd}
        onTouchCancel={this.onContentTouchEnd}
      >
        <ScrollContent
          ref={this.scrollContentRef}
          relativeScrollPosition={relativeScrollPosition}
          rowRenderer={rowRenderer}
          handleRelativeScrollPositionChange={handleRelativeScrollPositionChange}
        >
        </ScrollContent>
      </div>
    );
  }
}

export default ScrollContentArea;
