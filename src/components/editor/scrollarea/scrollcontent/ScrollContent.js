import React, { Component } from 'react';
import {
  translateToPxTop,
  determineLineHeight,
  rowIndexToRelativeScrollPosition
} from '../HelperFunctions';
import './ScrollContent.css';
import '../../row/Nibble.css';

/**
 * Class name to be applied to dummy element for determining line height.
 * Should be or match output of actual rendered elements.
 */
const dummyRowClassName = 'nibble';

/**
 * Encapsules the scroll content inside a scroll area.
 */
class ScrollContent extends Component {
  constructor(props) {
    super(props);

    // mount flag to only work with refs after component has mounted
    this._isMounted = false;
    this.scrollContentRef = React.createRef();
    // calculate methods
    this.calculateContentTop = this.calculateContentTop.bind(this);
    this.getScrollHeight = this.getScrollHeight.bind(this);
    this.getClientHeight = this.getClientHeight.bind(this);
    this.getOffsetHeight = this.getOffsetHeight.bind(this);
    this.getOffsetTop = this.getOffsetTop.bind(this);
    this.scrollToRow = this.scrollToRow.bind(this);
  }

  //#region lifecycle

  componentDidMount() {
    this._isMounted = true;
  }

  //#endregion lifecycle

  /**
   * Calculates the content Px top according to the relative position.
   * @param {*} relativeTop Relative scroll position between 0 and 1.
   */
  calculateContentTop() {
    if (!this._isMounted) {
      return 0;
    }

    // content relative -> Px
    const contentTotalHeightPx = this.scrollContentRef.current.scrollHeight;
    const contentVisibleHeightPx = this.scrollContentRef.current.clientHeight;

    const contentNewTopPx = translateToPxTop(
      contentTotalHeightPx,
      0,
      0,
      contentVisibleHeightPx,
      this.props.relativeScrollPosition
    );

    return -contentNewTopPx;
  }

  //#region API

  /**
   * Returns the components client height property.
   */
  getClientHeight() {
    return this.scrollContentRef.current.clientHeight;
  }

  /**
   * Returns the components scroll height property.
   */
  getScrollHeight() {
    return this.scrollContentRef.current.scrollHeight;
  }

  /**
   * Returns the components offset height property.
   */
  getOffsetHeight() {
    return this.scrollContentRef.current.offsetHeight;
  }

  /**
   * Returns the components offset top property.
   */
  getOffsetTop() {
    return this.scrollContentRef.current.offsetTop;
  }

  /**
   * Scrolls content, so that specified row is visible.
   * @param {*} rowIndex Index of row that should be visible.
   */
  scrollToRow(rowIndex) {
    const contentTopPx = -this.getOffsetTop();
    const viewportHeightPx = this.getOffsetHeight();
    const rowHeightPx = determineLineHeight(this.scrollContentRef.current, dummyRowClassName);
    const numRowsTotal = this.props.rowRenderer.numRows;
    const newRelativeScrollPosition = rowIndexToRelativeScrollPosition(
      rowIndex, contentTopPx, viewportHeightPx, rowHeightPx, numRowsTotal);
    if (newRelativeScrollPosition >= 0) {
      // -1 indicates that row is already visible
      this.props.handleRelativeScrollPositionChange(newRelativeScrollPosition);
    }
  }

  /**
   * Returns the height of a single rendered row in px.
   */
  getRowHeightPx() {
    const rowHeightPx = determineLineHeight(this.scrollContentRef.current, dummyRowClassName);
    return rowHeightPx;
  }

  //#endregion API

  render() {
    const contentTop = this.calculateContentTop();
    const rowRenderer = this.props.rowRenderer;
    const rows = rowRenderer.render(0); // render all rows

    return (
      <div className="scroll-content"
        ref={this.scrollContentRef}
        style={isNaN(contentTop) ? {} : { top: contentTop }}
      >
        {rows}
      </div>
    );
  }
}

export default ScrollContent;
