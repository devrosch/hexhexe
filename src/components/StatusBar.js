import React, { Component } from 'react';
import { Layout, Row, Col } from 'antd';
import './StatusBar.css';

const { Footer } = Layout;

class StatusBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      className: 'status-text fade-out'
    }

    this.formatCursorPosition = this.formatCursorPosition.bind(this);
    this.formatMessage = this.formatMessage.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.status !== this.props.status) {
      // set new status and start animation effects cycle
      this.setState({
        className: 'status-text fade-in'
      });
      clearTimeout(this.timeout); // stop possibly previously set animation start
      this.timeout = setTimeout(() =>
        this.setState({ className: 'status-text fade-out' }),
        3000
      );
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  formatCursorPosition() {
    if (typeof (this.props.cursorPosition) === 'undefined'
      || this.props.cursorPosition === null
      || typeof (this.props.cursorPosition.address) === 'undefined'
      || this.props.cursorPosition.address === null) {
      return '';
    }

    const text =
      'Cursor Pos.: ' +
      this.props.cursorPosition.address +
      ' (0x' +
      this.props.cursorPosition.address.toString(16).toUpperCase() +
      ')'

    return text;
  }

  formatMessage() {
    const status = this.props.status;
    if (typeof (status) === 'undefined'
      || status === null
      || typeof (status.message) === 'undefined'
      || status.message === null
    ) {
      return '';
    }
    let messageType = '';
    if (status.type === 'error') {
      messageType = 'ERROR: ';
    }
    else if (status.type === 'warn') {
      messageType = 'WARNING: ';
    }
    return messageType + status.message;
  }

  render() {
    return (
      <Footer>
        <Row>
          <Col
            xs={{ span: 4 }}
            sm={{ span: 6 }}
            md={{ span: 10 }}
            lg={{ span: 12 }}
            xl={{ span: 16 }}
            xxl={{ span: 16 }}
          >
            <span
              className={this.state.className}
            >
              {this.formatMessage()}
            </span>
          </Col>
          <Col
            xs={{ span: 20 }}
            sm={{ span: 18 }}
            md={{ span: 14 }}
            lg={{ span: 12 }}
            xl={{ span: 8 }}
            xxl={{ span: 8 }}
            style={{ textAlign: 'right' }}>
            {this.formatCursorPosition()}
          </Col>
        </Row>
      </Footer>
    );
  }
}

export default StatusBar;