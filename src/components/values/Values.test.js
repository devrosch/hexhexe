import Values from './Values';

let component;

beforeEach(() => {
  component = new Values();
});

it('converts zero double to expected string', () => {
  const fixture = 0.0;
  const actual = component.convertDoubleToOleDateString(fixture);
  expect(actual).toEqual('1899-12-30T00:00:00.000Z');
});

it('converts positive double to expected string', () => {
  const fixture = 1.5;
  const actual = component.convertDoubleToOleDateString(fixture);
  expect(actual).toEqual('1899-12-31T12:00:00.000Z');
});

it('converts negative double to expected string', () => {
  const fixture = -1.75;
  const actual = component.convertDoubleToOleDateString(fixture);
  expect(actual).toEqual('1899-12-29T18:00:00.000Z');
});

it('counts fractional part positive for negative doubles', () => {
  const fixtureInteger = -1.0;
  const fixtureFractional = -1.5;
  const actualInteger = component.convertDoubleToOleDateString(fixtureInteger);
  const actualFractional = component.convertDoubleToOleDateString(fixtureFractional);
  const dateInteger = Date.parse(actualInteger);
  const dateFractional = Date.parse(actualFractional);
  expect(dateInteger).toBeLessThan(dateFractional);
});

it('shows correct date when provided number just below max OLE date', () => {
  let fixtureMaxOleDate = 2958466; // days from epoch to 10000-01-01T00:00:00Z
  fixtureMaxOleDate -= 1.0 / (24 * 60 * 60 * 1000); // minus 1 ms
  const actual = component.convertDoubleToOleDateString(fixtureMaxOleDate);
  expect(actual).toEqual('9999-12-31T23:59:59.999Z');
});

it('shows error when provided number beyond max OLE date', () => {
  const fixtureMaxOleDate = 2958466; // days from epoch to 10000-01-01T00:00:00Z
  const actual = component.convertDoubleToOleDateString(fixtureMaxOleDate);
  expect(actual).toEqual('Invalid OLE Date (too large)');
});

it('shows correct date when provided number at min OLE date', () => {
  let fixtureMinOleDate = -657434; // days from epoch to 0100-01-01T00:00:00Z
  const actual = component.convertDoubleToOleDateString(fixtureMinOleDate);
  expect(actual).toEqual('0100-01-01T00:00:00.000Z');
});

it('shows error when provided number just below min OLE date', () => {
  let fixtureMinOleDate = -657434; // days from epoch to 0100-01-01T00:00:00Z
  // fractions are always counted positive, so subtract 2 days and add 1 ms to effectively represent 1 ms earlier
  fixtureMinOleDate -= 2;
  fixtureMinOleDate += 1.0 / (24 * 60 * 60 * 1000); // minus 1 ms
  const actual = component.convertDoubleToOleDateString(fixtureMinOleDate);
  expect(actual).toEqual('Invalid OLE Date (too small)');
});

it('shows error when provided number is NaN', () => {
  let fixture = Number.NaN;
  const actual = component.convertDoubleToOleDateString(fixture);
  expect(actual).toEqual('Invalid OLE Date');
});

it('shows error when provided number is Inf', () => {
  let fixture = Number.POSITIVE_INFINITY;
  const actual = component.convertDoubleToOleDateString(fixture);
  expect(actual).toEqual('Invalid OLE Date');
});

it('shows error when provided number is -Inf', () => {
  let fixture = Number.NEGATIVE_INFINITY;
  const actual = component.convertDoubleToOleDateString(fixture);
  expect(actual).toEqual('Invalid OLE Date');
});

it('shows error when provided number is null', () => {
  let fixture = null;
  const actual = component.convertDoubleToOleDateString(fixture);
  expect(actual).toEqual('Invalid OLE Date');
});

it('shows error when provided number is undefined', () => {
  let fixture = undefined;
  const actual = component.convertDoubleToOleDateString(fixture);
  expect(actual).toEqual('Invalid OLE Date');
});
