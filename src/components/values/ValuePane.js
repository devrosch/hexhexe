import React, { Component } from 'react';
import { Card, Collapse } from 'antd';
import Values from './Values';
import './ValuePane.css';

class ValuePane extends Component {
  render() {
    return (
      <Card className="value-pane">
        <Collapse defaultActiveKey={['values']}>
          <Collapse.Panel
            key="values"
            header="Values"
          >
            <Values
              settingsStorage={this.props.settingsStorage}
              cursorPosition={this.props.cursorPosition}
              file={this.props.file}
            />
          </Collapse.Panel>
        </Collapse>
      </Card>
    );
  }

}

export default ValuePane;