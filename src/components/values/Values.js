import React, { Component } from 'react';
import BinaryReader from '../../model/BinaryReader';
import { Form, Row, Col, Input, Radio, Divider } from 'antd';
// import FormItem from 'antd/lib/form/FormItem';
import './ValuePane.css';

class Values extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], endianness: null };
    this.convertDoubleToOleDateString = this.convertDoubleToOleDateString.bind(this);
    this.updateState = this.updateState.bind(this);
    this.handleEndiannessChange = this.handleEndiannessChange.bind(this);
  }

  componentDidMount() {
    this.updateState(this.props.cursorPosition.address, this.props.file);
    const endiannessSetting = this.props.settingsStorage.endianness;
    this.setState({ endianness: endiannessSetting })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.cursorPosition !== this.props.cursorPosition
      || prevState.endianness !== this.state.endianness) {
      this.updateState(this.props.cursorPosition.address, this.props.file);
    }
  }

  convertDoubleToOleDateString(doubleValue) {
    if (doubleValue === undefined || doubleValue === null || !Number.isFinite(doubleValue)) {
      return 'Invalid OLE Date';
    }
    // see: https://docs.microsoft.com/de-de/dotnet/api/system.datetime.tooadate?view=netcore-3.1
    // min: midnight, 1 January 0100
    // max: last moment of 31 December 9999
    // see: https://www.codeproject.com/Articles/144159/Time-Format-Conversion-Made-Easy
    // for negative numbers, fractions must be counted positive nonetheless
    const dayToMsConversionFactor = 24 * 60 * 60 * 1000;
    const integerDays = Math.trunc(doubleValue);
    const fractionalDays = Math.abs(doubleValue % 1); // always positive
    const millisecondsFromEpoch = (integerDays + fractionalDays) * dayToMsConversionFactor;
    const oleEpoch = new Date(Date.UTC(1899, 11, 30, 0, 0, 0));
    const minOleDate = new Date(Date.UTC(100, 0, 1, 0, 0, 0));
    const maxOleDate = new Date(Date.UTC(9999, 11, 31, 23, 59, 59, 999));
    if (millisecondsFromEpoch < minOleDate.getTime() - oleEpoch.getTime()) {
      // smaller than min OLE date
      // minimum legal date represented by doubleValue -657434.0
      // as fractions are always counted positive, this does not represent the minimum legal double value
      // e.g. -657434.1 is smaller, but represents a later OLE date
      return 'Invalid OLE Date (too small)';
    }
    else if (millisecondsFromEpoch > maxOleDate.getTime() - oleEpoch.getTime()) {
      // larger than max OLE date
      // corresponds to doubleValue >= 2958466.0 {
      return 'Invalid OLE Date (too large)';
    }
    const date = new Date(oleEpoch.getTime() + millisecondsFromEpoch);
    return date.toISOString();
  }

  updateState(address, file) {
    if (!file) {
      this.setState({
        data: {
          uint8: null,
          int8: null,
        }
      });
      return;
    }

    const numBytes = address + 8 <= file.size ?
      8 : file.size - address;
    const littleEndian = this.state.endianness === 'big' ? false : true;
    const convertDoubleToOleDateString = this.convertDoubleToOleDateString;

    file.readBytes(address, address + numBytes)
      .then((bytes) => {
        const reader = new BinaryReader(bytes);
        const bits = bytes.byteLength > 0 ? ("000000000" + reader.readUInt8().toString(2)).substr(-8) : null;
        reader.position = 0;
        const uint8 = bytes.byteLength > 0 ? reader.readUInt8() : null;
        reader.position = 0;
        const int8 = bytes.byteLength > 0 ? reader.readInt8() : null;
        reader.position = 0;
        let int16 = null;
        let uint16 = null;
        let int32 = null;
        let uint32 = null;
        let float32 = null;
        let float64 = null;
        // these are non native numbers
        let uint64 = null;
        let int64 = null;
        // timestamps
        let posixDate32 = null;
        let oleDate = null;

        if (littleEndian) {
          uint16 = bytes.byteLength > 1 ? reader.readUInt16LE() : null;
          reader.position = 0;
          int16 = bytes.byteLength > 1 ? reader.readInt16LE() : null;
          reader.position = 0;
          uint32 = bytes.byteLength > 3 ? reader.readUInt32LE() : null;
          reader.position = 0;
          int32 = bytes.byteLength > 3 ? reader.readInt32LE() : null;
          reader.position = 0;
          uint64 = bytes.byteLength > 7 ? reader.readUInt64LE() : null;
          reader.position = 0;
          int64 = bytes.byteLength > 7 ? reader.readInt64LE() : null;
          reader.position = 0;
          float32 = bytes.byteLength > 3 ? reader.readFloatLE() : null;
          reader.position = 0;
          float64 = bytes.byteLength > 7 ? reader.readDoubleLE() : null;
          reader.position = 0;
          posixDate32 = bytes.byteLength > 3 ? (new Date(reader.readInt32LE() * 1000)).toISOString() : null;
          reader.position = 0;
          oleDate = bytes.byteLength > 7
            ? convertDoubleToOleDateString(reader.readDoubleLE())
            : null;
        }
        else {
          uint16 = bytes.byteLength > 1 ? reader.readUInt16BE() : null;
          reader.position = 0;
          int16 = bytes.byteLength > 1 ? reader.readInt16BE() : null;
          reader.position = 0;
          uint32 = bytes.byteLength > 3 ? reader.readUInt32BE() : null;
          reader.position = 0;
          int32 = bytes.byteLength > 3 ? reader.readInt32BE() : null;
          reader.position = 0;
          uint64 = bytes.byteLength > 7 ? reader.readUInt64BE() : null;
          reader.position = 0;
          int64 = bytes.byteLength > 7 ? reader.readInt64BE() : null;
          reader.position = 0;
          float32 = bytes.byteLength > 3 ? reader.readFloatBE() : null;
          reader.position = 0;
          float64 = bytes.byteLength > 7 ? reader.readDoubleBE() : null;
          reader.position = 0;
          posixDate32 = bytes.byteLength > 3 ? (new Date(reader.readInt32BE() * 1000)).toISOString() : null;
          reader.position = 0;
          oleDate = bytes.byteLength > 7
            ? convertDoubleToOleDateString(reader.readDoubleBE())
            : null;
        }

        this.setState({
          data: {
            bits: bits,
            uint8: uint8,
            int8: int8,
            uint16: uint16,
            int16: int16,
            uint32: uint32,
            int32: int32,
            uint64: uint64,
            int64: int64,
            float32: float32,
            float64: float64,
            posixDate32: posixDate32,
            oleDate: oleDate,
          }
        });

      });
  }

  handleEndiannessChange(evt) {
    const endianness = evt.target.value;
    this.setState({
      endianness: endianness,
    });
    this.props.settingsStorage.endianness = endianness;
  }

  render() {
    const formItemLayout = {
      labelCol: {
        md: { span: 10 },
        lg: { span: 8 },
        xl: { span: 8 },
        xxl: { span: 6 },
      },
      wrapperCol: {
        md: { span: 14 },
        lg: { span: 16 },
        xl: { span: 16 },
        xxl: { span: 18 },
      },
    };

    const formItemLayout8And16 = {
      labelCol: {
        md: { span: 10 },
        lg: { span: 16 },
        xl: { span: 16 },
        xxl: { span: 12 },
      },
      wrapperCol: {
        md: { span: 14 },
        lg: { span: 8 },
        xl: { span: 8 },
        xxl: { span: 12 },
      },
    };

    const formItemLayout32 = {
      labelCol: {
        md: { span: 10 },
        lg: { span: 8 },
        xl: { span: 8 },
        xxl: { span: 12 },
      },
      wrapperCol: {
        md: { span: 14 },
        lg: { span: 16 },
        xl: { span: 16 },
        xxl: { span: 12 },
      },
    };

    return (
      <div>
        <Row
          type="flex"
          justify="end"
        >
          <Radio.Group onChange={this.handleEndiannessChange} value={this.state.endianness}>
            <Radio value={'little'}>Little Endian</Radio>
            <Radio value={'big'}>Big Endian</Radio>
          </Radio.Group>
        </Row>

        <Divider />

        <Form>
          <Form.Item
            {...formItemLayout}
            label="Bits"
            validateStatus={this.state.asciiValidateStatus}
            help={this.state.asciiErrorMessage}
          >
            <Input
              value={this.state.data.bits === null || typeof (this.state.data.bits) === 'undefined' ? '' : String(this.state.data.bits)}
              readOnly={true}
            />
          </Form.Item>
          <Row>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              md={{ span: 24 }}
              lg={{ span: 12 }}
              xl={{ span: 12 }}
              xxl={{ span: 12 }}
            >
              <Form.Item
                {...formItemLayout8And16}
                label="Unsigned&nbsp;Int&nbsp;8&nbsp;bit"
              >
                <Input
                  value={this.state.data.uint8}
                  readOnly={true}
                />
              </Form.Item>
            </Col>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              md={{ span: 24 }}
              lg={{ span: 12 }}
              xl={{ span: 12 }}
              xxl={{ span: 12 }}
            >
              <Form.Item
                {...formItemLayout8And16}
                label="Signed&nbsp;Int&nbsp;8&nbsp;bit"
              >
                <Input
                  value={this.state.data.int8}
                  readOnly={true}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              lg={{ span: 12 }}
              xl={{ span: 12 }}
              xxl={{ span: 12 }}
            >
              <Form.Item
                {...formItemLayout8And16}
                label="Unsigned&nbsp;Int&nbsp;16&nbsp;bit"
              >
                <Input
                  value={this.state.data.uint16}
                  readOnly={true}
                />
              </Form.Item>
            </Col>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              lg={{ span: 12 }}
              xl={{ span: 12 }}
              xxl={{ span: 12 }}
            >
              <Form.Item
                {...formItemLayout8And16}
                label="Signed&nbsp;Int&nbsp;16&nbsp;bit"
              >
                <Input
                  value={this.state.data.uint16}
                  readOnly={true}                  
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              lg={{ span: 24 }}
              xl={{ span: 24 }}
              xxl={{ span: 12 }}
            >
              <Form.Item
                {...formItemLayout32}
                label="Unsigned&nbsp;Int&nbsp;32&nbsp;bit"
              >
                <Input
                  value={this.state.data.uint32}
                  readOnly={true}
                />
              </Form.Item>
            </Col>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              lg={{ span: 24 }}
              xl={{ span: 24 }}
              xxl={{ span: 12 }}
            >
              <Form.Item
                {...formItemLayout32}
                label="Signed&nbsp;Int&nbsp;32&nbsp;bit"
              >
              <Input
                  value={this.state.data.int32}
                  readOnly={true}
                />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            {...formItemLayout}
            label="Unsigned&nbsp;Int&nbsp;64&nbsp;bit"
          >
            <Input
              value={this.state.data.uint64}
              readOnly={true}
            />
          </Form.Item>
          <Form.Item
            {...formItemLayout}
            label="Signed&nbsp;Int&nbsp;64&nbsp;bit"
          >
            <Input
              value={this.state.data.int64}
              readOnly={true}
            />
          </Form.Item>
          <Form.Item label="Float&nbsp;32&nbsp;bit" {...formItemLayout}>
            <Input
              value={this.state.data.float32 === null || typeof (this.state.data.float32) === 'undefined' ? '' : String(this.state.data.float32)}
              readOnly={true}
            />
          </Form.Item>
          <Form.Item label="Float&nbsp;64&nbsp;bit" {...formItemLayout}>
            <Input
              value={this.state.data.float64 === null || typeof (this.state.data.float64) === 'undefined' ? '' : String(this.state.data.float64)}
              readOnly={true}
            />
          </Form.Item>
          <Form.Item label="POSIX&nbsp;Date&nbsp;(32&nbsp;bit)" {...formItemLayout}>
            <Input
              value={this.state.data.posixDate32 === null || typeof (this.state.data.posixDate32) === 'undefined' ? '' : String(this.state.data.posixDate32)}
              readOnly={true}
            />
          </Form.Item>
          <Form.Item label="OLE&nbsp;Date" {...formItemLayout}>
            <Input
              value={this.state.data.oleDate === null || typeof (this.state.data.oleDate) === 'undefined' ? '' : String(this.state.data.oleDate)}
              readOnly={true}
            />
          </Form.Item>
        </Form>
      </div>
    );
  }

}

export default Values;