/**
 * The maximum buffer size.
 */
const maxBufferSize = 100;

/**
 * A document, buffering the last few reads for faster access.
 */
class BufferedDocument {
  constructor(document) {
    this._document = document;
    this._queue = [];
    this._buffer = {};
    this._removeDirtyEntries = this._removeDirtyEntries.bind(this);
    this.readBytes = this.readBytes.bind(this);
    this.updateBytes = this.updateBytes.bind(this);
    this.insertBytes = this.insertBytes.bind(this);
    this.deleteBytes = this.deleteBytes.bind(this);
    this.asBlob = this.asBlob.bind(this);
    this.undo = this.undo.bind(this);
  }

  /**
   * The document's unique ID.
   */
  get key() {
    return this._document.key;
  }

  /**
   * The document name.
   */
  get name() {
    return this._document.name;
  }

  /**
   * The document size in bytes.
   */
  get size() {
    return this._document.size;
  }

  /**
   * The document's cursor position.
   */
  get cursorPosition() {
    return this._document.cursorPosition;
  }

  /**
   * Sets the document's cursor position.
   */
  set cursorPosition(newCursorPosition) {
    return this._document.cursorPosition = newCursorPosition;
  }

  /**
   * Whether the document has been modified since opening.
   */
  get modified() {
    return this._document.modified;
  }
  
  /**
   * Gets the range of bytes marked.
   */
  get markedRange() {
    return this._document.markedRange;
  }

  /**
   * Sets the range of bytes marked.
   */
  set markedRange(range) {
    return this._document.markedRange = range;
  }

  /**
   * Read n bytes asynchronously from the document from zero based startIndex to endIndex.
   * @param {*} startIndex Zero based index of first byte to read.
   * @param {*} endIndex Zero based index of last byte to read (not inclusive).
   * @returns A Promise, resolving to an ArrayBuffer with the bytes read.
   */
  readBytes(startIndex, endIndex) {
    const key = startIndex + '_' + endIndex;

    // retrieve from buffer if available
    if (this._buffer.hasOwnProperty(key)) {
      return this._buffer[key];
    }

    // fetch from document
    const promise = this._document.readBytes(startIndex, endIndex);

    // determine oldest buffer entry and replace with new promise
    if (this._queue.length >= maxBufferSize) {
      // replace oldest entry
      const oldestKey = this._queue.shift();
      delete this._buffer[oldestKey];
    }
    // add new promise to buffer
    this._buffer[key] = promise;
    this._queue.push(key);

    return promise;
  }

  _removeDirtyEntries(operation, startIndex, numBytes) {
    // find dirty buffer entries
    let dirtyEntries = [];
    Object.keys(this._buffer).forEach(key => {
      const keySegments = key.split('_');
      const startAddress = parseInt(keySegments[0], 10);
      const endAddress = parseInt(keySegments[1], 10);
      const length = endAddress - startAddress;
      if (operation === 'insert' || operation === 'delete') {
        if (startIndex <= startAddress
          || (startIndex >= startAddress && startIndex < startAddress + length)) {
          dirtyEntries.push(key);
        }
      }
      else if (operation === 'update') {
        if ((startIndex < startAddress && startIndex + numBytes >= startAddress)
          || (startIndex >= startAddress && startIndex < startAddress + length)) {
          dirtyEntries.push(key);
        }
      }
      else {
        throw new Error('Unsupported operation: ' + operation);
      }
    });
    // remove dirty buffer entries
    dirtyEntries.forEach(key => {
      const historyIndex = this._queue.findIndex(historyKey => historyKey === key);
      this._queue.splice(historyIndex, 1);
      delete this._buffer[key];
    });
  }

  /**
   * Update bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based index of first byte to update.
   * @param {*} bytes A byte array withe the new values for the bytes beginning at startIndex.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  updateBytes(startIndex, bytes, cursorPosition) {
    this._removeDirtyEntries('update', startIndex, bytes.length);
    // update document
    return this._document.updateBytes(startIndex, bytes, cursorPosition);
  }

  /**
   * Insert bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based address of first byte to be inserted.
   * @param {*} bytes A byte array withe the values to be inserted.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  insertBytes(startIndex, bytes, cursorPosition) {
    this._removeDirtyEntries('insert', startIndex, bytes.length);
    // update document
    return this._document.insertBytes(startIndex, bytes, cursorPosition);
  }

  /**
   * Delete bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based address of first byte to be deleted.
   * @param {*} numBytes The number of bytes to delete.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  deleteBytes(startIndex, numBytes, cursorPosition) {
    this._removeDirtyEntries('delete', startIndex, numBytes);
    // update document
    return this._document.deleteBytes(startIndex, numBytes, cursorPosition);
  }

  asBlob() {
    return this._document.asBlob();
  }

  /**
   * Undoes the last modification.
   * @returns The undone operation or null if nothing was done.
   */
  undo() {
    const operation = this._document.undo();
    if (operation === null) {
      return null;
    }
    if (operation.action === 'update') {
      this._removeDirtyEntries('update', operation.startIndex, operation.oldValues.size);
    }
    else if (operation.action === 'insert') {
      this._removeDirtyEntries('delete', operation.startIndex, operation.newValues.size);
    }
    else if (operation.action === 'delete') {
      this._removeDirtyEntries('insert', operation.startIndex, operation.oldValues.size);
    }
    else {
      throw new Error('Unsupported undo action: ' + operation.action);
    }
    return operation;
  }

  /**
   * Redoes the last undo operation.
   * @returns The redone operation or null if there was no redo operation.
   */
  redo() {
    const operation = this._document.redo();
    if (operation === null) {
      return null;
    }
    const size = operation.newValues ? operation.newValues.size : operation.oldValues.size;
    this._removeDirtyEntries(operation.action, operation.startIndex, size);
    return operation;
  }

}

export default BufferedDocument;