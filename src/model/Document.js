/**
 * A document containing editable byte data.
 */
class Document {
  constructor(file, key) {
    this._blob = file;
    this._name = file.name;
    this._key = key;
    this._cursorPosition = {
      address: 0,
      nibble: 1,
      mode: 'bytes',
    }
    this.readBytes = this.readBytes.bind(this);
    this.updateBytes = this.updateBytes.bind(this);
    this.insertBytes = this.insertBytes.bind(this);
    this.deleteBytes = this.deleteBytes.bind(this);
    this.asBlob = this.asBlob.bind(this);
  }

  /**
   * History of edit (replace/insert) events.
   */
  _history = [];

  /**
   * Undo operations that may be redone.
   */
  _redoLog = [];

  /**
   * The range of bytes marked.
   * "start" is inclusive, "end" exclusive.
   */
  _markedRange = {
    start: 0,
    end: 0
  };

  /**
   * The document's unique ID.
   */
  get key() {
    return this._key;
  }

  /**
   * The document name.
   */
  get name() {
    return this._name;
  }

  /**
   * The document size in bytes.
   */
  get size() {
    return this._blob.size;
  }

  /**
   * The document's cursor position.
   */
  get cursorPosition() {
    return this._cursorPosition;
  }

  /**
   * Sets the document's cursor position.
   */
  set cursorPosition(newCursorPosition) {
    return this._cursorPosition = newCursorPosition;
  }

  /**
   * Whether the document has been modified since opening.
   */
  get modified() {
    return this._history.length > 0;
  }

  /**
   * Gets the range of bytes marked.
   */
  get markedRange() {
    return this._markedRange;
  }

  /**
   * Sets the range of bytes marked.
   */
  set markedRange(range) {
    if (typeof (range) === 'undefined'
      || range === null
      || typeof (range) !== 'object'
      || typeof (range.start) === 'undefined'
      || typeof (range.end) === 'undefined') {
      throw new Error('Range to be marked must be object with "start" and "end" properties.');
    }
    return this._markedRange = range;
  }

  /**
   * Read n bytes asynchronously from the document from zero based startIndex to endIndex.
   * @param {*} startIndex Zero based index of first byte to read.
   * @param {*} endIndex Zero based index of last byte to read (not inclusive).
   * @returns A Promise, resolving to an ArrayBuffer with the bytes read.
   */
  readBytes(startIndex, endIndex) {
    // console.log('this.readBytes()-->blobSize: ' + this._blob.size);

    const reader = new FileReader();
    const promise = new Promise(function (resolve, reject) {
      reader.onload = (e) => resolve(e.target.result);
      reader.onerror = (e) => reject(e.target.error);
    });
    // start the async reading operation
    const blob = this._blob.slice(startIndex, endIndex);
    reader.readAsArrayBuffer(blob);
    return promise;
  }

  /**
   * Update bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based index of first byte to update.
   * @param {*} bytes A byte array withe the new values for the bytes beginning at startIndex.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  updateBytes(startIndex, bytes, cursorPosition) {
    const newCursorPosition = cursorPosition ? cursorPosition : {
      address: startIndex + bytes.length,
      nibble: 1,
      mode: this.cursorPosition.mode
    };
    const historyEntry = {
      action: 'update',
      cursorPosition: this.cursorPosition,
      newCursorPosition: newCursorPosition,
      startIndex: startIndex,
      oldBlob: this._blob,
      newBlob: null,
      oldValues: null,
      newValues: null
    }
    // update Blob
    const precedingSegment = this._blob.slice(0, startIndex);
    const toBeReplacedSegment = this._blob.slice(startIndex, startIndex + bytes.length);
    const succeedingSegment = this._blob.slice(startIndex + bytes.length, this._blob.size);
    const newValues = Uint8Array.from(bytes);
    const newBlob = new Blob([precedingSegment, newValues, succeedingSegment]);
    historyEntry.newBlob = newBlob;
    this._blob = newBlob;
    this.cursorPosition = newCursorPosition;
    // update history
    historyEntry.oldValues = toBeReplacedSegment;
    historyEntry.newValues = new Blob([newValues]);
    this._history.push(historyEntry);
    this._redoLog = [];
  }

  /**
   * Insert bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based address of first byte to be inserted.
   * @param {*} bytes A byte array withe the values to be inserted.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  insertBytes(startIndex, bytes, cursorPosition) {
    const newCursorPosition = cursorPosition ? cursorPosition : {
      address: this.cursorPosition.address + bytes.length,
      nibble: 1,
      mode: this.cursorPosition.mode
    };
    const historyEntry = {
      action: 'insert',
      cursorPosition: this.cursorPosition,
      newCursorPosition: newCursorPosition,
      startIndex: startIndex,
      oldBlob: this._blob,
      newBlob: null,
      oldValues: null,
      newValues: null
    }
    // update state
    const precedingSegment = this._blob.slice(0, startIndex);
    const succeedingSegment = this._blob.slice(startIndex);
    const newValues = new Blob([Uint8Array.from(bytes)]);
    const newBlob = new Blob([precedingSegment, newValues, succeedingSegment]);
    historyEntry.newBlob = newBlob;
    this._blob = newBlob;
    this.cursorPosition = newCursorPosition;
    // update history
    historyEntry.oldValues = null;
    historyEntry.newValues = newValues;
    this._history.push(historyEntry);
    this._redoLog = [];
  }

  /**
   * Delete bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based address of first byte to be deleted.
   * @param {*} numBytes The number of bytes to delete.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  deleteBytes(startIndex, numBytes, cursorPosition) {
    let newCursorAddress = this.cursorPosition.address;
    if (newCursorAddress >= this.size) {
      newCursorAddress = this.size - 1;
    }
    const newCursorPosition = cursorPosition ? cursorPosition : {
      address: newCursorAddress,
      nibble: 1,
      mode: this.cursorPosition.mode
    };
    const historyEntry = {
      action: 'delete',
      cursorPosition: this.cursorPosition,
      newCursorPosition: newCursorPosition,
      startIndex: startIndex,
      oldBlob: this._blob,
      newBlob: null,
      oldValues: null,
      newValues: null
    }
    // update state
    const precedingSegment = this._blob.slice(0, startIndex);
    const deletedValues = this._blob.slice(startIndex, startIndex + numBytes);
    const succeedingSegment = this._blob.slice(startIndex + numBytes);
    const newBlob = new Blob([precedingSegment, succeedingSegment]);
    historyEntry.newBlob = newBlob;
    this._blob = newBlob;
    this.cursorPosition = newCursorPosition;
    // update history
    historyEntry.oldValues = deletedValues;
    historyEntry.newValues = null;
    this._history.push(historyEntry);
    this._redoLog = [];
  }

  /**
   * Returns a blob containing the document's data.
   */
  asBlob() {
    return this._blob;
  }

  /**
   * Undoes the last modification.
   * @returns The undone operation or null if nothing was done.
   */
  undo() {
    if (this._history.length <= 0) {
      return null;
    }
    const lastOperation = this._history.pop();
    this._blob = lastOperation.oldBlob;
    this.cursorPosition = lastOperation.cursorPosition;
    this._redoLog.push(lastOperation);
    return lastOperation;
  }

  /**
   * Redoes the last undo operation.
   * @returns The redone operation or null if there was no redo operation.
   */
  redo() {
    if (this._redoLog.length <= 0) {
      return null;
    }
    const lastOperation = this._redoLog.pop();
    this._blob = lastOperation.newBlob;
    this.cursorPosition = lastOperation.newCursorPosition;
    this._history.push(lastOperation);
    return lastOperation;
  }
}

export default Document;