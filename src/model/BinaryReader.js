import { Uint64BE, Uint64LE, Int64BE, Int64LE } from 'int64-buffer';

class BinaryReader {
  constructor(arrayBuffer) {
    this._buffer = arrayBuffer;
    this._position = 0; // current _position in buffer
    this._readNBytes = this._readNBytes.bind(this);
    this._findZeroIndex = this._findZeroIndex.bind(this);
    this.readString = this.readString.bind(this);
    this.readStringArray = this.readStringArray.bind(this);
    this.readByteArray = this.readByteArray.bind(this);
    this.readUInt8 = this.readUInt8.bind(this);
    this.readInt8 = this.readInt8.bind(this);
    this.readUInt16LE = this.readUInt16LE.bind(this);
    this.readUInt16BE = this.readUInt16BE.bind(this);
    this.readInt16LE = this.readInt16LE.bind(this);
    this.readInt16BE = this.readInt16BE.bind(this);
    this.readUInt32LE = this.readUInt32LE.bind(this);
    this.readUInt32BE = this.readUInt32BE.bind(this);
    this.readInt32LE = this.readInt32LE.bind(this);
    this.readInt32BE = this.readInt32BE.bind(this);
    this.readUInt64LE = this.readUInt64LE.bind(this);
    this.readUInt64BE = this.readUInt64BE.bind(this);
    this.readInt64LE = this.readInt64LE.bind(this);
    this.readInt64BE = this.readInt64BE.bind(this);
    this.readFloatLE = this.readFloatLE.bind(this);
    this.readFloatBE = this.readFloatBE.bind(this);
    this.readDoubleLE = this.readDoubleLE.bind(this);
    this.readDoubleBE = this.readDoubleBE.bind(this);
  }

  // private function
  _readNBytes(n) {
    let dataView = new DataView(this._buffer, this._position, n);
    this._position = this._position + n;
    return dataView;
  }

  _findZeroIndex(text, startIndex) {
    let zeroIndex = -1;
    for (let i = startIndex; i < text.length; i++) {
      if (text.charCodeAt(i) === 0) {
        zeroIndex = i;
        break;
      }
    }
    return zeroIndex;
  }

  // public functions
  readString(length, encoding) {
    let useEncoding = 'UTF-8';
    if (encoding) {
      useEncoding = encoding;
    }
    let decoder = new TextDecoder(useEncoding);
    let dataView = this._readNBytes(length);
    let text = decoder.decode(dataView);
    // find zero terminator
    let zeroIndex = length;
    for (let i = 0; i < text.length; i++) {
      if (text.charCodeAt(i) === 0) {
        zeroIndex = i;
        break;
      }
    }
    // remove zero terminator and everything else that might follow
    return text.substring(0, zeroIndex);
  }

  readStringArray(length, encoding) {
    let useEncoding = 'UTF-8';
    if (encoding) {
      useEncoding = encoding;
    }
    let textArray = [];
    let decoder = new TextDecoder(useEncoding);
    let dataView = this._readNBytes(length);
    let text = decoder.decode(dataView);
    let startIndex = 0;
    while (startIndex < text.length) {
      let zeroIndex = this._findZeroIndex(text, startIndex);
      if (zeroIndex < 0) {
        zeroIndex = text.length;
      }
      let val = text.substring(startIndex, zeroIndex);
      textArray.push(val);
      startIndex = zeroIndex + 1;
    }
    return textArray;
  }

  readByteArray(n) {
    let dataView = this._readNBytes(n);
    let bytes = [];
    for (let index = 0; index < n; index++) {
      const element = dataView.getUint8(index);
      bytes.push(element);
    }
    return bytes;
  }

  readUInt8() {
    return this._readNBytes(1).getUint8(0);
  }

  readInt8() {
    return this._readNBytes(1).getInt8(0);
  }

  readUInt16LE() {
    return this._readNBytes(2).getUint16(0, true);
  }

  readUInt16BE() {
    return this._readNBytes(2).getUint16(0, false);
  }

  readInt16LE() {
    return this._readNBytes(2).getInt16(0, true);
  }

  readInt16BE() {
    return this._readNBytes(2).getInt16(0, false);
  }

  readUInt32LE() {
    return this._readNBytes(4).getUint32(0, true);
  }

  readUInt32BE() {
    return this._readNBytes(4).getUint32(0, false);
  }

  readInt32LE() {
    return this._readNBytes(4).getInt32(0, true);
  }

  readInt32BE() {
    return this._readNBytes(4).getInt32(0, false);
  }

  readUInt64LE() {
    if (this.size - this.position < 8) {
      throw new Error('Attempted read past end of ArrayBuffer.');
    }
    const value = new Uint64LE(this.buffer, this.position);
    this.position = this.position + 8;
    return value;
  }

  readUInt64BE() {
    if (this.size - this.position < 8) {
      throw new Error('Attempted read past end of ArrayBuffer.');
    }
    const value = new Uint64BE(this.buffer, this.position);
    this.position = this.position + 8;
    return value;
  }

  readInt64LE() {
    if (this.size - this.position < 8) {
      throw new Error('Attempted read past end of ArrayBuffer.');
    }
    const value = new Int64LE(this.buffer, this.position);
    this.position = this.position + 8;
    return value;
  }

  readInt64BE() {
    if (this.size - this.position < 8) {
      throw new Error('Attempted read past end of ArrayBuffer.');
    }
    const value = new Int64BE(this.buffer, this.position);
    this.position = this.position + 8;
    return value;
  }

  readFloatLE() {
    return this._readNBytes(4).getFloat32(0, true);
  }

  readFloatBE() {
    return this._readNBytes(4).getFloat32(0, false);
  }

  readDoubleLE() {
    return this._readNBytes(8).getFloat64(0, true);
  }

  readDoubleBE() {
    return this._readNBytes(8).getFloat64(0, false);
  }

  get position() {
    return this._position;
  }

  set position(newPosition) {
    this._position = newPosition;
    return this._position;
  }

  get size() {
    return this._buffer.byteLength;
  }

  get buffer() {
    return this._buffer;
  }
}

export default BinaryReader;