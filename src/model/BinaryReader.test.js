import BinaryReader from "./BinaryReader";

it('initializes without crashing', () => {
  let arrayBuffer = new ArrayBuffer(1);
  let typedArray = new Uint8Array(arrayBuffer);
});

// apparently internally used TextDecoder is not available in test environment
// the text-encoding library might help: https://www.npmjs.com/package/text-encoding?activeTab=readme
// it('reads string', () => {
//     let ab = new ArrayBuffer(4);
//     let typedArray = new Uint8Array(ab);
//     typedArray[0] = 0x41; // A
//     typedArray[1] = 0x42; // B
//     typedArray[2] = 0x43; // C
//     typedArray[3] = 0; // 0
//     let reader = new BinaryReader(ab);
//     let text = reader.readString(4);
//     expect(text).toEqual('abc');
// });

// it('reads string array', () => {
//     let ab = new ArrayBuffer(4);
//     let typedArray = new Uint8Array(ab);
//     typedArray[0] = 0x41; // A
//     typedArray[1] = 0x42; // B
//     typedArray[2] = 0x43; // C
//     typedArray[3] = 0; // 0
//     let reader = new BinaryReader(ab);
//     let text = reader.readStringArray(4)[0];
//     expect(text).toEqual('abc');
// });

it('read byte array', () => {
  let arrayBuffer = new ArrayBuffer(2);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[0] = 99;
  typedArray[1] = 100;
  let reader = new BinaryReader(arrayBuffer);
  let byteArray = reader.readByteArray(2);
  expect(byteArray[0]).toEqual(99);
  expect(byteArray[1]).toEqual(100);
});

it('reads int8', () => {
  let arrayBuffer = new ArrayBuffer(1);
  let typedArray = new Int8Array(arrayBuffer);
  typedArray[0] = 99;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readInt8()).toEqual(99);
});

it('reads uint8', () => {
  let arrayBuffer = new ArrayBuffer(1);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[0] = 99;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readUInt8()).toEqual(99);
});

it('reads int16LE', () => {
  let arrayBuffer = new ArrayBuffer(2);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[0] = 99;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readInt16LE()).toEqual(99);
});

it('reads int16BE', () => {
  let arrayBuffer = new ArrayBuffer(2);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[1] = 99;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readInt16BE()).toEqual(99);
});

it('reads int32LE', () => {
  let arrayBuffer = new ArrayBuffer(4);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[0] = 99;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readInt32LE()).toEqual(99);
});

it('reads int32BE', () => {
  let arrayBuffer = new ArrayBuffer(4);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[3] = 99;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readInt32BE()).toEqual(99);
});

it('reads float32LE', () => {
  let arrayBuffer = new ArrayBuffer(4);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[3] = 0x40;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readFloatLE()).toEqual(2);
});

it('reads float32BE', () => {
  let arrayBuffer = new ArrayBuffer(4);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[0] = 0x40;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readFloatBE()).toEqual(2);
});

it('reads double64LE', () => {
  let arrayBuffer = new ArrayBuffer(8);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[7] = 0x40;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readDoubleLE()).toEqual(2);
});

it('reads double64BE', () => {
  let arrayBuffer = new ArrayBuffer(8);
  let typedArray = new Uint8Array(arrayBuffer);
  typedArray[0] = 0x40;
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.readDoubleBE()).toEqual(2);
});

it('moves position forward when reading', () => {
  let arrayBuffer = new ArrayBuffer(1);
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.position).toEqual(0);
  reader.readUInt8();
  expect(reader.position).toEqual(1);
});

it('returns correct data size', () => {
  let arrayBuffer = new ArrayBuffer(5);
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.size).toEqual(5);
});

it('returns the buffer', () => {
  let arrayBuffer = new ArrayBuffer(5);
  let reader = new BinaryReader(arrayBuffer);
  expect(reader.buffer).toEqual(arrayBuffer);
});
