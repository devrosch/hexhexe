import DocumentSearcher from './DocumentSearcher';
import { func } from 'prop-types';

it('finds byte pattern at start of document', done => {
  const blob = new Blob([Uint8Array.from([1, 2, 3])]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  function searchCompletedCallback(hitAddress) {
    expect(hitAddress).toEqual(0);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(0, [1, 2]);
});

it('finds byte pattern at end of document', done => {
  const blob = new Blob([Uint8Array.from([1, 2, 3])]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  function searchCompletedCallback(hitAddress) {
    expect(hitAddress).toEqual(1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(0, [2, 3]);
});

it('does not find byte pattern that wraps around', done => {
  const blob = new Blob([Uint8Array.from([1, 2, 3])]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  function searchCompletedCallback(hitAddress) {
    expect(hitAddress).toEqual(-1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(0, [3, 1]);
});

it('finds pattern in large document', done => {
  let view = new Uint8Array(1024 * 1024 * 2);
  view[1024 * 1024 + 1] = 1;
  view[1024 * 1024 + 2] = 2;
  view[1024 * 1024 + 3] = 3;
  const blob = new Blob([view]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  const searchCompletedCallback = function (hitAddress) {
    expect(hitAddress).toEqual(1024 * 1024 + 1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(0, [1, 2, 3]);
});

it('finds pattern spanning accross chunk limits in document', done => {
  let view = new Uint8Array(1024 * 1024 * 2);
  view[1024 * 1024 - 1] = 1;
  view[1024 * 1024 + 0] = 2;
  view[1024 * 1024 + 1] = 3;
  const blob = new Blob([view]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  const searchCompletedCallback = function (hitAddress) {
    expect(hitAddress).toEqual(1024 * 1024 - 1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(0, [1, 2, 3]);
});

it('wraps search around to search from start', done => {
  let view = new Uint8Array(1024 * 1024 * 2);
  view[512 - 1] = 1;
  view[512 + 0] = 2;
  view[512 + 1] = 3;
  const blob = new Blob([view]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  const searchCompletedCallback = function (hitAddress) {
    expect(hitAddress).toEqual(512 - 1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(1024, [1, 2, 3]);
});

it('terminates search when start address is close to end of file', done => {
  const view = new Uint8Array(1024 * 1024 * 4 + 500);
  const blob = new Blob([view]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  const searchCompletedCallback = function (hitAddress) {
    expect(hitAddress).toEqual(-1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(1024 * 1024 * 4 + 400, [1, 2, 3]);
});

it('does not find non existent pattern', done => {
  let view = new Uint8Array(1024 * 1024 * 2);
  const blob = new Blob([view]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  const searchCompletedCallback = function (hitAddress) {
    expect(hitAddress).toEqual(-1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(0, [1, 2, 3]);
});

it('does not find non existent pattern when document size smaller chunk size', done => {
  let view = new Uint8Array(16);
  const blob = new Blob([view]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  const searchCompletedCallback = function (hitAddress) {
    expect(hitAddress).toEqual(-1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback);
  searcher.searchDocument(3, [0xFF, 0xEE, 0xDD]);
});

it('periodically updates search progress', done => {
  const buf = new ArrayBuffer(1024 * 1024 * 4);
  const blob = new Blob([buf]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  const searchProgressCallback = jest.fn();

  const searchCompletedCallback = function (hitAddress) {
    expect(searchProgressCallback).toHaveBeenCalledTimes(4);
    expect(searcher.searchProgress).toEqual(1.0);
    expect(hitAddress).toEqual(-1);
    done();
  }

  const searcher = new DocumentSearcher(doc, searchCompletedCallback, searchProgressCallback);
  searcher.searchDocument(0, [1, 2, 3]);
});

it('stops search after cancellation', done => {
  let view = new Uint8Array(1024 * 1024 * 4);
  view[1024 * 1024 * 4 - 3] = 1;
  view[1024 * 1024 * 4 - 2] = 2;
  view[1024 * 1024 * 4 - 1] = 3;
  const blob = new Blob([view]);
  const doc = {
    size: blob.size,
    asBlob: function () { return blob }
  }

  let searcher = null;

  const searchProgressCallback = function() {
    searcher.cancelSearch();
  }

  const searchCompletedCallback = function (hitAddress) {
    expect(hitAddress).toEqual(-1);
    expect(searcher.searchProgress).toBeLessThan(0.5);
    done();
  }

  searcher = new DocumentSearcher(doc, searchCompletedCallback, searchProgressCallback);
  searcher.searchDocument(0, [1, 2, 3]);
});