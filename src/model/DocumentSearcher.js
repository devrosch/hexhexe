/**
 * The chunk size to read from the document for performing the search.
 */
const searchChunkSize = 1024 * 1024;
// const searchChunkSize = 1024;

/**
 * Searches the document for a byte pattern.
 */
class DocumentSearcher {
  /**
   * @param {*} document The document to search in.
   * @param {*} searchCompletedCallback Callback to call when search has completed.
   * @param {*} searchProgressCallback Callback to call with search progress updates.
   */
  constructor(document, searchCompletedCallback, searchProgressCallback) {
    if (document == null) {
      throw new Error('No document specified for search.');
    }
    if (searchCompletedCallback == null) {
      throw new Error('No callback for search completed specified.');
    }
    if (searchProgressCallback != null) {
      this._searchProgressCallback = searchProgressCallback;
    }
    this._document = document;
    this._searchCompletedCallback = searchCompletedCallback;
    this._cancelFlag = false;
    this._searchProgress = 0;

    this.cancelSearch = this.cancelSearch.bind(this);
    this.searchDocument = this.searchDocument.bind(this);
    this._calculateProgress = this._calculateProgress.bind(this);
    this._findHit = this._findHit.bind(this);
    this._searchDocument = this._searchDocument.bind(this);
  }

  /**
   * The document to search in.
   */
  _document = null;

  /**
   * Callback to call when search has completed.
   * The hit address (or -1 if no hit) will be passed.
   */
  _searchCompletedCallback = null;

  /**
   * Callback to call with search progress updates.
   * Value will be a number between 0 and 1.
   */
  _searchProgressCallback = null;

  /**
   * Whether to cancel the search.
   */
  _cancelFlag = false;

  /**
   * The search progress between 0 and 1.
   */
  _searchProgress = 0;

  /**
   * The fraction (between 0 and 1) of the document that has been searched.
   */
  get searchProgress() {
    return this._searchProgress === null ? 0 : this._searchProgress;
  }

  /**
   * Cancel the search.
   */
  cancelSearch() {
    this._cancelFlag = true;
  }

  /**
   * Searches the document for a byte pattern.
   * Recursively reads the document in chunks to perform the search.
   * @param {*} startAddress The address to start the search from.
   * @param {*} bytePattern The byte pattern to search.
   * @returns Nothing.
   */
  searchDocument(startAddress, bytePattern) {
    if (typeof (this._document) !== 'object') {
      throw new Error('Illegal document type specified for search: ' + this._document);
    }
    if (startAddress == null) {
      throw new Error('No start address for search specified.');
    }
    if (bytePattern == null || bytePattern.length <= 0) {
      throw new Error('No byte pattern to search for specified.');
    }
    this._searchProgress = 0;
    this._cancelFlag = false;
    this._searchDocument(startAddress, startAddress, bytePattern, false);
  }

  _calculateProgress(originalStartAddress, currentSearchAddress) {
    const size = this._document.size;
    if (currentSearchAddress > originalStartAddress) {
      return 1.0 * (currentSearchAddress - originalStartAddress) / size;
    }
    else {
      return 1.0 * (size - originalStartAddress + currentSearchAddress) / size;
    }
  }

  _findHit(arrayBufferData, bytePattern) {
    const uint8Array = new Uint8Array(arrayBufferData);
    // todo: search can be optimized, see: https://codereview.stackexchange.com/questions/20136/uint8array-indexof-method-that-allows-to-search-for-byte-sequences
    const hitIndex = uint8Array.findIndex((value, index, array) => {
      for (let i = 0; i < bytePattern.length; i++) {
        if ((index + i) >= array.length) {
          return false;
        }
        const element = array[index + i];
        if (element !== bytePattern[i]) {
          return false;
        }
      }
      return true;
    });
    return hitIndex;
  }

  /**
   * Searches the document for a byte pattern.
   * Recursively reads the document in chunks to perform the search.
   * @param {*} startAddress The start address of the current chunk to search in.
   * @param {*} originalStartAddress The initial address to start the search from.
   * @param {*} bytePattern The byte pattern to search.
   * @param {*} hasWrappedAround Indicates whether search has wrapped around at end of document.
   */
  _searchDocument(startAddress, originalStartAddress, bytePattern, hasWrappedAround) {
    const that = this;
    // determine end address and make sure to detect patterns spanning two chunks
    let endAddress = startAddress + searchChunkSize + bytePattern.length - 1;
    // never read past document end
    if (endAddress > that._document.size) {
      endAddress = that._document.size;
    }
    // read blob
    const docBlob = that._document.asBlob();
    const slice = docBlob.slice(startAddress, endAddress);
    const reader = new FileReader();
    const promise = new Promise(function (resolve, reject) {
      reader.onload = (e) => resolve(e.target.result);
      reader.onerror = (e) => reject(e.target.error);
    });
    // start the async reading operation
    reader.readAsArrayBuffer(slice);
    promise.then(data => {
      const hitIndex = that._findHit(data, bytePattern);

      if (that._cancelFlag) {
        // search cancelled => do not call callbacks
        that._searchCompletedCallback(-1);
        return;
      }

      if (hitIndex >= 0) {
        // found hit
        const hitAddress = startAddress + hitIndex;
        that._searchProgress = 1;
        that._searchCompletedCallback(hitAddress);
        return;
      }
      else {
        // update progress indication
        const progress = that._calculateProgress(originalStartAddress, endAddress);
        that._searchProgress = progress;
        if (that._searchProgressCallback) {
          that._searchProgressCallback(progress);
        }

        let newStartAddress = startAddress + searchChunkSize;
        if (newStartAddress >= that._document.size && !hasWrappedAround) {
          newStartAddress = 0;
          hasWrappedAround = true;
        }

        // determine search has completed
        if ((newStartAddress >= originalStartAddress
          && hasWrappedAround)
          || newStartAddress === startAddress) {
          // whole document has been searched
          that._searchCompletedCallback(-1);
          return;
        }
        else {
          // search next chunk
          that._searchDocument(newStartAddress, originalStartAddress, bytePattern, hasWrappedAround);
        }
      }
    });
  }

}

export default DocumentSearcher;