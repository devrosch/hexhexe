import BufferedDocument from './BufferedDocument';

const mockKey = 'Mock key';
const getKey = jest.fn(() => mockKey)
const mockName = 'Mock name';
const getName = jest.fn(() => mockName)
const mockSize = 'Mock size';
const getSize = jest.fn(() => mockSize)
const cursorPosition = {
  address: 2,
  nibble: 1,
  mode: 'bytes',
};
const getCursorPosition = jest.fn(() => cursorPosition);
const setCursorPosition = jest.fn(() => { });
const modified = true;
const getModified = jest.fn(() => modified);
const markedRange = { start: 0, end: 1 };
const getMarkedRange = jest.fn(() => markedRange);
const setMarkedRange = jest.fn();
const readBytes = jest.fn((start, end) => {
  const promise = new Promise((resolve, reject) => {
    resolve(new ArrayBuffer(end - start));
  });
  return promise;
});
const insertBytes = jest.fn();
const updateBytes = jest.fn();
const deleteBytes = jest.fn();
const asBlob = jest.fn();
const document = {
  readBytes: readBytes,
  insertBytes: insertBytes,
  updateBytes: updateBytes,
  deleteBytes: deleteBytes,
  asBlob: asBlob,
};
Object.defineProperty(document, 'key', { get: getKey, });
Object.defineProperty(document, 'name', { get: getName, });
Object.defineProperty(document, 'size', { get: getSize, });
Object.defineProperty(document, 'cursorPosition', { get: getCursorPosition, set: setCursorPosition });
Object.defineProperty(document, 'modified', { get: getModified, });
Object.defineProperty(document, 'markedRange', { get: getMarkedRange, set: setMarkedRange });

beforeEach(() => {
  jest.clearAllMocks();
});

it('reads the key correctly from underlying document', () => {
  const bDoc = new BufferedDocument(document);
  expect(bDoc.key).toEqual(mockKey);
  expect(getKey).toHaveBeenCalledTimes(1);
});

it('reads the name correctly from underlying document', () => {
  const bDoc = new BufferedDocument(document);
  expect(bDoc.name).toEqual(mockName);
  expect(getName).toHaveBeenCalledTimes(1);
});

it('reads the size correctly from underlying document', () => {
  const bDoc = new BufferedDocument(document);
  expect(bDoc.size).toEqual(mockSize);
  expect(getSize).toHaveBeenCalledTimes(1);
});

it('reads the cursor position correctly from underlying document', () => {
  const bDoc = new BufferedDocument(document);
  const returnedCursorPosition = bDoc.cursorPosition;
  expect(returnedCursorPosition).toBe(cursorPosition);
  expect(getCursorPosition).toHaveBeenCalledTimes(1);
});

it('sets the cursor position correctly in underlying document', () => {
  const bDoc = new BufferedDocument(document);
  const newCursorPosition = { address: 3, nibble: 0, mode: 'ascii' };;
  bDoc.cursorPosition = newCursorPosition;
  expect(setCursorPosition).toHaveBeenCalledTimes(1);
  expect(setCursorPosition).toHaveBeenCalledWith(newCursorPosition);
});

it('reads the modified information correctly from underlying document', () => {
  const bDoc = new BufferedDocument(document);
  expect(bDoc.modified).toEqual(modified);
  expect(getModified).toHaveBeenCalledTimes(1);
});

it('gets marked range in underlying document', () => {
  const bDoc = new BufferedDocument(document);
  expect(bDoc.markedRange).toBe(markedRange);
  expect(getMarkedRange).toHaveBeenCalledTimes(1);
});

it('sets marked range in underlying document', () => {
  const bDoc = new BufferedDocument(document);
  const markedRange = { start: 1, end: 2 };
  bDoc.markedRange = markedRange;
  expect(setMarkedRange).toHaveBeenCalledTimes(1);
  expect(setMarkedRange.mock.calls[0][0]).toBe(markedRange);
});

it('reads data from underlying document and buffers results', () => {
  const bDoc = new BufferedDocument(document);
  bDoc.readBytes(1, 2);
  expect(readBytes).toHaveBeenCalledTimes(1);
  bDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(2);
  bDoc.readBytes(1, 3);
  expect(readBytes).toHaveBeenCalledTimes(3);
  bDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(3);
});

it('invalidates buffer after insert', () => {
  const bDoc = new BufferedDocument(document);
  const result0 = bDoc.readBytes(4, 5);
  expect(readBytes).toHaveBeenCalledTimes(1);
  bDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(2);
  bDoc.insertBytes(1, [1]);
  const result1 = bDoc.readBytes(4, 5);
  expect(insertBytes).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(3);
  expect(result1).not.toBe(result0);
});

it('invalidates buffer after update', () => {
  const bDoc = new BufferedDocument(document);
  const result0 = bDoc.readBytes(4, 5);
  expect(readBytes).toHaveBeenCalledTimes(1);
  bDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(2);
  bDoc.updateBytes(1, [1]);
  bDoc.updateBytes(2, [1]);
  bDoc.readBytes(2, 3);
  const result1 = bDoc.readBytes(4, 5);
  expect(updateBytes).toHaveBeenCalledTimes(2);
  expect(readBytes).toHaveBeenCalledTimes(3);
  expect(result1).toBe(result0);
});

it('invalidates buffer after delete', () => {
  const bDoc = new BufferedDocument(document);
  const result0 = bDoc.readBytes(4, 5);
  expect(readBytes).toHaveBeenCalledTimes(1);
  bDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(2);
  bDoc.deleteBytes(1, 1);
  const result1 = bDoc.readBytes(4, 5);
  expect(deleteBytes).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(3);
  expect(result1).not.toBe(result0);
});

it('reads Blob from underlying document', () => {
  const bDoc = new BufferedDocument(document);
  bDoc.asBlob();
  expect(asBlob).toHaveBeenCalledTimes(1);
});

it('calls undo() in underlying document during undo operation', () => {
  document.undo = jest.fn(() => {
    const historyEntry = {
      action: 'insert',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: null,
      newValues: new Blob([1])
    }
    return historyEntry;
  });
  const bDoc = new BufferedDocument(document);
  bDoc.undo();
  expect(document.undo).toHaveBeenCalledTimes(1);
});

it('invalidates dirty buffer entries after undo of update operation', () => {
  document.undo = jest.fn(() => {
    const historyEntry = {
      action: 'update',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const bDoc = new BufferedDocument(document);
  const buffer0 = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(1);
  const buffer1 = bDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(2);
  bDoc.undo();
  const buffer0b = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(3);
  const buffer1b = bDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(3);
  expect(buffer0b).not.toBe(buffer0);
  expect(buffer1b).toBe(buffer1);
});

it('invalidates dirty buffer entries after undo of insert operation', () => {
  document.undo = jest.fn(() => {
    const historyEntry = {
      action: 'insert',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 3,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const bDoc = new BufferedDocument(document);
  const buffer0 = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(1);
  const buffer1 = bDoc.readBytes(3, 5);
  expect(readBytes).toHaveBeenCalledTimes(2);
  const buffer2 = bDoc.readBytes(6, 8);
  expect(readBytes).toHaveBeenCalledTimes(3);
  bDoc.undo();
  const buffer0b = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(3);
  const buffer1b = bDoc.readBytes(3, 5);
  expect(readBytes).toHaveBeenCalledTimes(4);
  const buffer2b = bDoc.readBytes(6, 8);
  expect(readBytes).toHaveBeenCalledTimes(5);
  expect(buffer0b).toBe(buffer0);
  expect(buffer1b).not.toBe(buffer1);
  expect(buffer2b).not.toBe(buffer2);
});

it('invalidates dirty buffer entries after undo of delete operation', () => {
  document.undo = jest.fn(() => {
    const historyEntry = {
      action: 'delete',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 3,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const bDoc = new BufferedDocument(document);
  const buffer0 = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(1);
  const buffer1 = bDoc.readBytes(3, 5);
  expect(readBytes).toHaveBeenCalledTimes(2);
  const buffer2 = bDoc.readBytes(6, 8);
  expect(readBytes).toHaveBeenCalledTimes(3);
  bDoc.undo();
  const buffer0b = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(3);
  const buffer1b = bDoc.readBytes(3, 5);
  expect(readBytes).toHaveBeenCalledTimes(4);
  const buffer2b = bDoc.readBytes(6, 8);
  expect(readBytes).toHaveBeenCalledTimes(5);
  expect(buffer0b).toBe(buffer0);
  expect(buffer1b).not.toBe(buffer1);
  expect(buffer2b).not.toBe(buffer2);
});

it('returns null from undo if no undo operation is available', () => {
  document.undo = jest.fn(() => null);
  const bDoc = new BufferedDocument(document);
  const operation = bDoc.undo();
  expect(operation).toBe(null);
});

it('calls redo() in underlying document during redo operation', () => {
  document.redo = jest.fn(() => {
    const historyEntry = {
      action: 'insert',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: null,
      newValues: new Blob([1])
    }
    return historyEntry;
  });
  const bDoc = new BufferedDocument(document);
  bDoc.redo();
  expect(document.redo).toHaveBeenCalledTimes(1);
});

it('invalidates dirty buffer entries after redo of update operation', () => {
  document.redo = jest.fn(() => {
    const historyEntry = {
      action: 'update',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const bDoc = new BufferedDocument(document);
  const buffer0 = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(1);
  const buffer1 = bDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(2);
  bDoc.redo();
  const buffer0b = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(3);
  const buffer1b = bDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(3);
  expect(buffer0b).not.toBe(buffer0);
  expect(buffer1b).toBe(buffer1);
});

it('invalidates dirty buffer entries after redo of insert operation', () => {
  document.redo = jest.fn(() => {
    const historyEntry = {
      action: 'insert',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 3,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const bDoc = new BufferedDocument(document);
  const buffer0 = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(1);
  const buffer1 = bDoc.readBytes(3, 5);
  expect(readBytes).toHaveBeenCalledTimes(2);
  const buffer2 = bDoc.readBytes(6, 8);
  expect(readBytes).toHaveBeenCalledTimes(3);
  bDoc.redo();
  const buffer0b = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(3);
  const buffer1b = bDoc.readBytes(3, 5);
  expect(readBytes).toHaveBeenCalledTimes(4);
  const buffer2b = bDoc.readBytes(6, 8);
  expect(readBytes).toHaveBeenCalledTimes(5);
  expect(buffer0b).toBe(buffer0);
  expect(buffer1b).not.toBe(buffer1);
  expect(buffer2b).not.toBe(buffer2);
});

it('invalidates dirty buffer entries after redo of delete operation', () => {
  document.redo = jest.fn(() => {
    const historyEntry = {
      action: 'delete',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 3,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const bDoc = new BufferedDocument(document);
  const buffer0 = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(1);
  const buffer1 = bDoc.readBytes(3, 5);
  expect(readBytes).toHaveBeenCalledTimes(2);
  const buffer2 = bDoc.readBytes(6, 8);
  expect(readBytes).toHaveBeenCalledTimes(3);
  bDoc.redo();
  const buffer0b = bDoc.readBytes(0, 2);
  expect(readBytes).toHaveBeenCalledTimes(3);
  const buffer1b = bDoc.readBytes(3, 5);
  expect(readBytes).toHaveBeenCalledTimes(4);
  const buffer2b = bDoc.readBytes(6, 8);
  expect(readBytes).toHaveBeenCalledTimes(5);
  expect(buffer0b).toBe(buffer0);
  expect(buffer1b).not.toBe(buffer1);
  expect(buffer2b).not.toBe(buffer2);
});

it('returns null from redo if no redo operation is available', () => {
  document.redo = jest.fn(() => null);
  const bDoc = new BufferedDocument(document);
  const operation = bDoc.redo();
  expect(operation).toBe(null);
});
