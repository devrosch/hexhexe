import Document from './Document';

it('stores the key correctly', () => {
  const key = '1';
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, key);
  expect(doc.key).toEqual(key);
});

it('reads name from underlying file', () => {
  const fileName = 'test name';
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = fileName;
  const doc = new Document(file, '1');
  expect(doc.name).toEqual(fileName);
});

it('reads size from underlying file', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  expect(doc.size).toEqual(bytes.length);
});

it('initializes the cursor position correctly', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, '1');
  expect(doc.cursorPosition).toEqual(expect.anything());
  expect(doc.cursorPosition.address).toEqual(0);
});

it('updates the cursor position correctly', () => {
  const cursorPosition = {
    address: 2,
    nibble: 1,
    mode: 'bytes',
  }
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.cursorPosition = cursorPosition;
  expect(doc.cursorPosition).toBe(cursorPosition);
  expect(doc.cursorPosition.address).toEqual(cursorPosition.address);
  expect(doc.cursorPosition.nibble).toEqual(cursorPosition.nibble);
  expect(doc.cursorPosition.mode).toEqual(cursorPosition.mode);
});

it('reports unmodified state correctly', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  expect(doc.modified).toEqual(false);
});

it('reports modified state correctly after update', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.updateBytes(1, [4]);
  expect(doc.modified).toEqual(true);
});

it('reports modified state correctly after insert', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.insertBytes(1, [4]);
  expect(doc.modified).toEqual(true);
});

it('reports modified state correctly after delete', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.deleteBytes(1, 1);
  expect(doc.modified).toEqual(true);
});

it('returns the previously set marked range', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  const mark = { start: 1, end: 3 };
  doc.markedRange = mark;
  expect(doc.markedRange).toBe(mark);
});

it('rejects setting illegal marked range lacking start property', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  const mark = { /* start: 1 , */ end: 3 };
  expect(() => doc.markedRange = mark).toThrowError('must be object with "start" and "end" properties');
});

it('rejects setting illegal marked range lacking end property', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  const mark = { start: 1 /*, end: 3 */ };
  expect(() => doc.markedRange = mark).toThrowError('must be object with "start" and "end" properties');
});

it('rejects setting illegal marked range with no object', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  const mark = null;
  expect(() => doc.markedRange = mark).toThrowError('must be object with "start" and "end" properties');
});

it('reads data from underlying file correctly', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, '1');
  return doc.readBytes(1, 3).then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(2);
    expect(dataArray[0]).toEqual(2);
    expect(dataArray[1]).toEqual(3);
  });
});

it('returns correct data after update', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.updateBytes(1, [4, 5]);
  return doc.readBytes(0, 3).then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(3);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(4);
    expect(dataArray[2]).toEqual(5);
  });
});

it('returns correct data after insert', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.insertBytes(1, [4, 5]);
  return doc.readBytes(0, 5).then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(5);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(4);
    expect(dataArray[2]).toEqual(5);
    expect(dataArray[3]).toEqual(2);
    expect(dataArray[4]).toEqual(3);
  });
});

it('returns correct data after delete', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3, 4])]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.deleteBytes(1, 2);
  return doc.readBytes(0, 2).then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(2);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(4);
  });
});

it('returns correct Blob after init', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, '1');
  const reader = new FileReader();
  const promise = new Promise(function (resolve, reject) {
    reader.onload = (e) => resolve(e.target.result);
    reader.onerror = (e) => reject(e.target.error);
  });
  reader.readAsArrayBuffer(doc.asBlob());
  return promise.then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(3);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(2);
    expect(dataArray[2]).toEqual(3);
  });
});

it('returns correct Blob after update', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.updateBytes(1, [4, 5]);
  const reader = new FileReader();
  const promise = new Promise(function (resolve, reject) {
    reader.onload = (e) => resolve(e.target.result);
    reader.onerror = (e) => reject(e.target.error);
  });
  reader.readAsArrayBuffer(doc.asBlob());
  return promise.then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(3);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(4);
    expect(dataArray[2]).toEqual(5);
  });
});

it('returns correct Blob after insert', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3])]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.insertBytes(1, [4, 5]);
  const reader = new FileReader();
  const promise = new Promise(function (resolve, reject) {
    reader.onload = (e) => resolve(e.target.result);
    reader.onerror = (e) => reject(e.target.error);
  });
  reader.readAsArrayBuffer(doc.asBlob());
  return promise.then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(5);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(4);
    expect(dataArray[2]).toEqual(5);
    expect(dataArray[3]).toEqual(2);
    expect(dataArray[4]).toEqual(3);
  });
});

it('returns correct Blob after delete', () => {
  let file = new Blob([Uint8Array.from([1, 2, 3, 4])]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.deleteBytes(1, 2);
  const reader = new FileReader();
  const promise = new Promise(function (resolve, reject) {
    reader.onload = (e) => resolve(e.target.result);
    reader.onerror = (e) => reject(e.target.error);
  });
  reader.readAsArrayBuffer(doc.asBlob());
  return promise.then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(2);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(4);
  });
});

it('undo() returns operation undone', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.updateBytes(1, [4]);
  doc.insertBytes(1, [5]);
  doc.deleteBytes(1, 1);
  const operation0 = doc.undo();
  expect(operation0.action).toEqual('delete');
  const operation1 = doc.undo();
  expect(operation1.action).toEqual('insert');
  const operation2 = doc.undo();
  expect(operation2.action).toEqual('update');
  const operation3 = doc.undo();
  expect(operation3).toEqual(null);
});

it('correctly processes undo after update', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.updateBytes(1, [4]);
  const operation = doc.undo();
  expect(operation.action).toEqual('update');
  expect(doc.modified).toEqual(false);
  return doc.readBytes(0, 3).then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(3);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(2);
    expect(dataArray[2]).toEqual(3);
  });
});

it('correctly processes undo after insert', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.insertBytes(1, [4]);
  const operation = doc.undo();
  expect(operation.action).toEqual('insert');
  expect(doc.modified).toEqual(false);
  expect(doc.size).toEqual(3);
  return doc.readBytes(0, 3).then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(3);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(2);
    expect(dataArray[2]).toEqual(3);
  });
});

it('correctly processes undo after delete', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.deleteBytes(1, 1);
  const operation = doc.undo();
  expect(operation.action).toEqual('delete');
  expect(doc.modified).toEqual(false);
  expect(doc.size).toEqual(3);
  return doc.readBytes(0, 3).then(data => {
    expect(data).toEqual(expect.any(ArrayBuffer));
    const dataArray = new Uint8Array(data);
    expect(dataArray.byteLength).toEqual(3);
    expect(dataArray[0]).toEqual(1);
    expect(dataArray[1]).toEqual(2);
    expect(dataArray[2]).toEqual(3);
  });
});

it('correctly redoes last undo', () => {
  const bytes = [1, 2, 3];
  let file = new Blob([Uint8Array.from(bytes)]);
  file.name = '';
  const doc = new Document(file, '1');
  doc.deleteBytes(1, 1);
  const blob = doc.asBlob();
  const cursorPosition = doc.cursorPosition;
  const size = doc.size;
  doc.undo();
  expect(doc.asBlob()).not.toBe(blob);
  expect(doc.cursorPosition).not.toBe(cursorPosition);
  expect(doc.size).not.toBe(size);
  doc.redo();
  expect(doc.asBlob()).toBe(blob);
  expect(doc.cursorPosition).toBe(cursorPosition);
  expect(doc.size).toBe(size);
});

