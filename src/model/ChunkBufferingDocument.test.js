import ChunkBufferingDocument from './ChunkBufferingDocument';

const mockKey = 'Mock key';
const getKey = jest.fn(() => mockKey);
const mockName = 'Mock name';
const getName = jest.fn(() => mockName);
const mockSize = 'Mock size';
const getSize = jest.fn(() => mockSize);
const cursorPosition = {
  address: 2,
  nibble: 1,
  mode: 'bytes',
};
const getCursorPosition = jest.fn(() => cursorPosition);
const setCursorPosition = jest.fn(() => { });
const modified = true;
const getModified = jest.fn(() => modified);
const markedRange = { start: 0, end: 1 };
const getMarkedRange = jest.fn(() => markedRange);
const setMarkedRange = jest.fn();
const readBytes = jest.fn((start, end) => {
  const promise = new Promise((resolve, reject) => {
    resolve(new ArrayBuffer(end - start));
  });
  return promise;
});
const insertBytes = jest.fn();
const updateBytes = jest.fn();
const deleteBytes = jest.fn();
const asBlob = jest.fn();
const document = {
  readBytes: readBytes,
  insertBytes: insertBytes,
  updateBytes: updateBytes,
  deleteBytes: deleteBytes,
  asBlob: asBlob,
};
Object.defineProperty(document, 'key', { get: getKey, });
Object.defineProperty(document, 'name', { get: getName, });
Object.defineProperty(document, 'size', { get: getSize, });
Object.defineProperty(document, 'cursorPosition', { get: getCursorPosition, set: setCursorPosition });
Object.defineProperty(document, 'modified', { get: getModified, });
Object.defineProperty(document, 'modified', { get: getModified, });
Object.defineProperty(document, 'markedRange', { get: getMarkedRange, set: setMarkedRange });

beforeEach(() => {
  jest.clearAllMocks();
  // jest.resetAllMocks();
  // jest.restoreAllMocks();
  // getKey.mockClear();
});

it('reads the key correctly from underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  expect(cbDoc.key).toEqual(mockKey);
  expect(getKey).toHaveBeenCalledTimes(1);
});

it('reads the name correctly from underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  expect(cbDoc.name).toEqual(mockName);
  expect(getName).toHaveBeenCalledTimes(1);
});

it('reads the size correctly from underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  expect(cbDoc.size).toEqual(mockSize);
  expect(getSize).toHaveBeenCalledTimes(1);
});

it('reads the cursor position correctly from underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  const returnedCursorPosition = cbDoc.cursorPosition;
  expect(returnedCursorPosition).toBe(cursorPosition);
  expect(getCursorPosition).toHaveBeenCalledTimes(1);
});

it('sets the cursor position correctly in underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  const newCursorPosition = { address: 3, nibble: 0, mode: 'ascii' };;
  cbDoc.cursorPosition = newCursorPosition;
  expect(setCursorPosition).toHaveBeenCalledTimes(1);
  expect(setCursorPosition).toHaveBeenCalledWith(newCursorPosition);
});

it('reads the modified information correctly from underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  expect(cbDoc.modified).toEqual(modified);
  expect(getModified).toHaveBeenCalledTimes(1);
});

it('gets marked range in underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  expect(cbDoc.markedRange).toBe(markedRange);
  expect(getMarkedRange).toHaveBeenCalledTimes(1);
});

it('sets marked range in underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  const markedRange = { start: 1, end: 2 };
  cbDoc.markedRange = markedRange;
  expect(setMarkedRange).toHaveBeenCalledTimes(1);
  expect(setMarkedRange.mock.calls[0][0]).toBe(markedRange);
});

it('reads data from underlying document and buffers chunks', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(1, 2);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(8192, 8193);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('invalidates buffer after insert', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.insertBytes(1, [1]);
  cbDoc.readBytes(2, 3);
  expect(insertBytes).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('invalidates buffer after insert in 2nd chunk', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(8192, 8193);
  expect(readBytes).toHaveBeenCalledTimes(2); // 1st and 2nd chunk
  cbDoc.readBytes(8193, 8194);
  expect(readBytes).toHaveBeenCalledTimes(2);
  cbDoc.insertBytes(8193, [1]);
  cbDoc.readBytes(8193, 8194);
  expect(insertBytes).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(3); // 2nd chunk again
});

it('invalidates buffer after update', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.updateBytes(1, [1]);
  cbDoc.readBytes(2, 3);
  expect(updateBytes).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('invalidates buffer after update in 2nd chunk', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(8192, 8193);
  expect(readBytes).toHaveBeenCalledTimes(2); // 1st and 2nd chunk
  cbDoc.readBytes(8193, 8194);
  expect(readBytes).toHaveBeenCalledTimes(2);
  cbDoc.updateBytes(8193, [1]);
  cbDoc.readBytes(8193, 8194);
  expect(updateBytes).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(3);
});

it('invalidates buffer after delete', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.deleteBytes(1, 1);
  cbDoc.readBytes(2, 3);
  expect(deleteBytes).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('invalidates buffer after delete in 2nd chunk', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(8192, 8193);
  expect(readBytes).toHaveBeenCalledTimes(2); // 1st and 2nd chunk
  cbDoc.readBytes(8193, 8194);
  expect(readBytes).toHaveBeenCalledTimes(2);
  cbDoc.deleteBytes(8193, 8194);
  cbDoc.readBytes(8193, 8194);
  expect(deleteBytes).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(3);
});

it('reads Blob from underlying document', () => {
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.asBlob();
  expect(asBlob).toHaveBeenCalledTimes(1);
});

it('calls undo() in underlying document during undo operation and return operation', () => {
  const historyEntry = {
    action: 'insert',
    cursorPosition: {
      address: 0,
      nibble: 1,
      mode: 'bytes',
    },
    startIndex: 0,
    oldBlob: new Blob([1]),
    oldValues: null,
    newValues: new Blob([1])
  }
  document.undo = jest.fn(() => historyEntry);
  const cbDoc = new ChunkBufferingDocument(document);
  const operation = cbDoc.undo();
  expect(document.undo).toHaveBeenCalledTimes(1);
  expect(operation).toBe(historyEntry);
});

it('invalidates buffer after undo of update operation', () => {
  document.undo = jest.fn(() => {
    const historyEntry = {
      action: 'update',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.undo();
  cbDoc.readBytes(2, 3);
  expect(document.undo).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('invalidates buffer after undo of insert operation', () => {
  document.undo = jest.fn(() => {
    const historyEntry = {
      action: 'insert',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: null,
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.undo();
  cbDoc.readBytes(2, 3);
  expect(document.undo).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('invalidates buffer after undo of delete operation', () => {
  document.undo = jest.fn(() => {
    const historyEntry = {
      action: 'delete',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: null
    }
    return historyEntry;
  });
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.undo();
  cbDoc.readBytes(2, 3);
  expect(document.undo).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('returns null from undo if no undo operation is available', () => {
  document.undo = jest.fn(() => null);
  const cbDoc = new ChunkBufferingDocument(document);
  const operation = cbDoc.undo();
  expect(operation).toBe(null);
});

it('calls redo() in underlying document during redo operation and return operation', () => {
  const historyEntry = {
    action: 'insert',
    cursorPosition: {
      address: 0,
      nibble: 1,
      mode: 'bytes',
    },
    startIndex: 0,
    oldBlob: new Blob([1]),
    oldValues: null,
    newValues: new Blob([1])
  }
  document.redo = jest.fn(() => historyEntry);
  const cbDoc = new ChunkBufferingDocument(document);
  const operation = cbDoc.redo();
  expect(document.redo).toHaveBeenCalledTimes(1);
  expect(operation).toBe(historyEntry);
});

it('invalidates buffer after redo of update operation', () => {
  document.redo = jest.fn(() => {
    const historyEntry = {
      action: 'update',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.redo();
  cbDoc.readBytes(2, 3);
  expect(document.redo).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('invalidates buffer after redo of insert operation', () => {
  document.redo = jest.fn(() => {
    const historyEntry = {
      action: 'insert',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: null,
      newValues: new Blob([2])
    }
    return historyEntry;
  });
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.redo();
  cbDoc.readBytes(2, 3);
  expect(document.redo).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('invalidates buffer after redo of delete operation', () => {
  document.redo = jest.fn(() => {
    const historyEntry = {
      action: 'delete',
      cursorPosition: {
        address: 0,
        nibble: 1,
        mode: 'bytes',
      },
      startIndex: 0,
      oldBlob: new Blob([1]),
      oldValues: new Blob([1]),
      newValues: null
    }
    return historyEntry;
  });
  const cbDoc = new ChunkBufferingDocument(document);
  cbDoc.readBytes(3, 4);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.readBytes(2, 3);
  expect(readBytes).toHaveBeenCalledTimes(1);
  cbDoc.redo();
  cbDoc.readBytes(2, 3);
  expect(document.redo).toHaveBeenCalledTimes(1);
  expect(readBytes).toHaveBeenCalledTimes(2);
});

it('returns null from redo if no redo operation is available', () => {
  document.redo = jest.fn(() => null);
  const cbDoc = new ChunkBufferingDocument(document);
  const operation = cbDoc.redo();
  expect(operation).toBe(null);
});
