const defaultSettings = {
  bytesPerRow: 8,
  byteGroupSize: 1,
  endianness: 'little'
}

class SettingsStorage {
  constructor() {
    this._getValue = this._getValue.bind(this);
    this._setValue = this._setValue.bind(this);
  }

  _getValue(key) {
    const storage = localStorage;
    if (typeof (storage) !== "undefined") {
      // local storage should be available
      try {
        const val = storage[key];
        if (typeof (val) !== 'undefined') {
          return val;
        }
      }
      catch (exception) {
        console.warn('error, reading localStorage: ' + exception.name);
      }
    }
    // no local storage or error reading localStorage, return default value
    return defaultSettings[key];
  }

  _setValue(key, value) {
    const storage = localStorage;
    if (typeof (storage) !== "undefined") {
      // local storage should be available
      try {
        storage[key] = value;
      }
      catch (exception) {
        console.warn('error writing to localStorage: ' + exception.name);
      }
    } else {
      // no local storage, don't do anything
      console.warn('error writing to localStorage: ' + key + ' -> ' + value);
    }
  }

  get bytesPerRow() {
    const value = this._getValue('bytesPerRow');
    if (value !== null) {
      return parseInt(value, 10);
    }
    return value;
  }

  set bytesPerRow(value) {
    this._setValue('bytesPerRow', value);
  }

  get byteGroupSize() {
    const value = this._getValue('byteGroupSize');
    if (value !== null) {
      return parseInt(value, 10);
    }
    return value;
  }

  set byteGroupSize(value) {
    this._setValue('byteGroupSize', value);
  }

  get endianness() {
    return this._getValue('endianness');
  }

  set endianness(value) {
    this._setValue('endianness', value);
  }
}

export default SettingsStorage;