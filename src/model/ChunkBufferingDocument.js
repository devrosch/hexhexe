/**
 * The maximum number of chunks to buffer.
 */
const maxBufferSize = 6;

/**
 * The size of a chunk in bytes.
 */
const chunkSize = 8192;

/**
 * The proximity in number of bytes to a chunk start/end to trigger a prefetch.
 */
const prefetchThreshold = 4096;

class ChunkBufferingDocument {
  constructor(document) {
    // private properties
    /**
     * The underlying document.
     */
    this._document = document;
    /**
     * Chunk start addresses in the order of most recent access.
     */
    this._history = [];
    /**
     * A buffer of document chunks.
     */
    this._buffer = {};
    // private methods
    this._findBufferedChunk = this._findBufferedChunk.bind(this);
    this._updateHistory = this._updateHistory.bind(this);
    this._updateBuffer = this._updateBuffer.bind(this);
    this._prefetchChunks = this._prefetchChunks.bind(this);
    this._removeObsoleteChunks = this._removeObsoleteChunks.bind(this);
    // public methods
    this.readBytes = this.readBytes.bind(this);
    this.updateBytes = this.updateBytes.bind(this);
    this.insertBytes = this.insertBytes.bind(this);
    this.deleteBytes = this.deleteBytes.bind(this);
    this.asBlob = this.asBlob.bind(this);
    this.undo = this.undo.bind(this);
  }

  /**
   * The document's unique ID.
   */
  get key() {
    return this._document.key;
  }

  /**
   * The document name.
   */
  get name() {
    return this._document.name;
  }

  /**
   * The document size in bytes.
   */
  get size() {
    return this._document.size;
  }

  /**
   * The document's cursor position.
   */
  get cursorPosition() {
    return this._document.cursorPosition;
  }

  /**
   * Sets the document's cursor position.
   */
  set cursorPosition(newCursorPosition) {
    return this._document.cursorPosition = newCursorPosition;
  }

  /**
   * Whether the document has been modified since opening.
   */
  get modified() {
    return this._document.modified;
  }

  /**
   * Gets the range of bytes marked.
   */
  get markedRange() {
    return this._document.markedRange;
  }

  /**
   * Sets the range of bytes marked.
   */
  set markedRange(range) {
    return this._document.markedRange = range;
  }

  /**
   * Search address in already buffered data.
   * @param {*} address 
   * @returns A reference to the buffered chunk that holds the address or null if not found.
   */
  _findBufferedChunk(address) {
    const chunkStartAddress = Math.floor(address / chunkSize) * chunkSize;
    const bufferedChunk = this._buffer[chunkStartAddress];
    if (bufferedChunk) {
      this._updateHistory(chunkStartAddress);
      return bufferedChunk;
    }
    // console.log('_findBufferedChunk address miss: ' + address);
    // console.log('_findBufferedChunk chunk start address miss: ' + chunkStartAddress);
    return null;
  }

  /**
   * Updates history. 
   * @param {*} chunkStartAddress Start address of recently used chunk.
   */
  _updateHistory(chunkStartAddress) {
    // assumes that there are no two chunks with the same start address
    for (let i = 0; i < this._history.length; i++) {
      const historyAddress = this._history[i];
      if (chunkStartAddress === historyAddress) {
        this._history.splice(i, 1);
        this._history.push(historyAddress);
      }
    }
  }

  /**
   * Updates buffer with chunk that holds address and returns that chunk.
   * @param {*} address A valid address in the file.
   */
  _updateBuffer(address) {
    const chunkStartAddress = Math.floor(address / chunkSize) * chunkSize;
    const data = this._document.readBytes(chunkStartAddress, chunkStartAddress + chunkSize);
    if (this._history.length >= maxBufferSize) {
      // remove oldest buffered chunk
      const oldestKey = this._history.shift();
      delete this._buffer[oldestKey];
    }
    // add new chunk to buffer
    this._history.push(chunkStartAddress);
    const chunk = {
      address: chunkStartAddress,
      data: data
    }
    this._buffer[chunkStartAddress] = chunk;
    return chunk;
  }

  /**
   * Prefetches chunks that are likely to be required next based on requested address range.
   * @param {*} startAddress Zero based address of first byte to read.
   * @param {*} endAddress Zero based address of last byte to read (not inclusive).
   */
  _prefetchChunks(startAddress, endAddress) {
    const chunkStartAddress = Math.floor(startAddress / chunkSize) * chunkSize;
    const chunkEndAddress = Math.floor(endAddress / chunkSize) * chunkSize;

    if (startAddress % chunkSize < prefetchThreshold) {
      if (chunkStartAddress > 0 && this._findBufferedChunk(chunkStartAddress - 1) === null) {
        // prefetch chunk before chunk containing startAddress
        this._updateBuffer(chunkStartAddress - 1);
      }
    }
    if ((chunkSize - (endAddress % chunkSize)) < prefetchThreshold) {
      const nextChunkAddress = chunkEndAddress + chunkSize;
      if (nextChunkAddress < this._document.size &&
        this._findBufferedChunk(nextChunkAddress) === null) {
        // prefetch chunk after chunk containing endAddress
        this._updateBuffer(nextChunkAddress);
      }
    }
  }

  /**
   * Read n bytes asynchronously from the file from zero based startAddress to endAddress.
   * @param {*} startAddress Zero based address of first byte to read.
   * @param {*} endAddress Zero based address of last byte to read (not inclusive).
   * @returns A Promise, resolving to an ArrayBuffer with the bytes read.
   */
  readBytes(startAddress, endAddress) {
    if ((endAddress - startAddress) > chunkSize) {
      // no buffering if requested range is larger than chunk
      return this._document.readBytes(startAddress, endAddress);
    }

    let chunk0 = this._findBufferedChunk(startAddress);
    if (chunk0 === null) {
      chunk0 = this._updateBuffer(startAddress);
    }
    let chunk1 = this._findBufferedChunk(endAddress);
    if (chunk1 === null) {
      chunk1 = this._updateBuffer(endAddress);
    }

    this._prefetchChunks(startAddress, endAddress);

    if (chunk0 === chunk1) {
      // requsted data in same chunk
      const chunk = chunk0;
      const chunkAddress = chunk.address;
      const data = chunk.data;
      return data.then(arrayBuffer => {
        const bytes = arrayBuffer.slice(startAddress - chunkAddress, endAddress - chunkAddress);
        return bytes;
      });
    }
    else {
      // requsted data spread accross both chunks
      const address0 = chunk0.address;
      const data0 = chunk0.data;
      const address1 = chunk1.address;
      const data1 = chunk1.data;

      return Promise.all([data0, data1]).then(arrayBuffers => {
        const arrayBuf0 = arrayBuffers[0].slice(startAddress - address0, chunkSize);
        const arrayBuf1 = arrayBuffers[1].slice(0, endAddress - address1);
        // concatenate ArrayBuffers
        var output = new Uint8Array(arrayBuf0.byteLength + arrayBuf1.byteLength);
        output.set(new Uint8Array(arrayBuf0), 0);
        output.set(new Uint8Array(arrayBuf1), arrayBuf0.byteLength);
        return output.buffer;
      });
    }
  }

  /**
   * 
   * @param {*} operation The operation that causes the removal. Either "update", "insert", or "delete".
   * @param {*} startIndex Zero based index of first byte affected by the operation.
   * @param {*} numBytes The number of bytes affected.
   */
  _removeObsoleteChunks(operation, startIndex, numBytes) {
    let outdatedChunkAddresses = [];
    if (operation === 'update') {
      // find chunks made obsolete by update
      const firstChunkStartAddress = Math.floor(startIndex / chunkSize) * chunkSize;
      const lastChunkStartAddress = Math.floor((startIndex + numBytes) / chunkSize) * chunkSize;
      for (let address = firstChunkStartAddress; address <= lastChunkStartAddress; address += chunkSize) {
        const chunk = this._findBufferedChunk(address);
        if (chunk) {
          outdatedChunkAddresses.push(address);
        }
      }
    }
    else if (operation === 'insert' || operation === 'delete') {
      outdatedChunkAddresses = Object.keys(this._buffer).filter(value => {
        const address = parseInt(value, 10);
        return address + chunkSize >= startIndex;
      });
    }
    // delete outdated chunks from buffer and history
    outdatedChunkAddresses.forEach(address => {
      delete this._buffer[address];
    });
    this._history = this._history.filter((value, index) => {
      return outdatedChunkAddresses.indexOf(value) === -1;
    });
  }

  /**
   * Update bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based index of first byte to update.
   * @param {*} bytes A byte array withe the new values for the bytes beginning at startIndex.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  updateBytes(startIndex, bytes, cursorPosition) {
    this._removeObsoleteChunks('update', startIndex, bytes.length);
    // update actual document
    return this._document.updateBytes(startIndex, bytes, cursorPosition);
  }

  /**
   * Insert bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based address of first byte to be inserted.
   * @param {*} bytes A byte array withe the values to be inserted.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  insertBytes(startIndex, bytes, cursorPosition) {
    this._removeObsoleteChunks('insert', startIndex, bytes.length);
    // insert into actual document
    return this._document.insertBytes(startIndex, bytes, cursorPosition);
  }

  /**
   * Delete bytes in the document from zero based startIndex.
   * @param {*} startIndex Zero based address of first byte to be deleted.
   * @param {*} numBytes The number of bytes to delete.
   * @param {*} cursorPosition Optional: The new cursor position.
   */
  deleteBytes(startIndex, numBytes, cursorPosition) {
    this._removeObsoleteChunks('delete', startIndex, numBytes);
    // delete from actual document
    return this._document.deleteBytes(startIndex, numBytes, cursorPosition);
  }

  /**
   * Returns a blob containing the document's data.
   */
  asBlob() {
    return this._document.asBlob();
  }

  undo() {
    const operation = this._document.undo();
    if (operation === null) {
      return null;
    }
    if (operation.action === 'update') {
      this._removeObsoleteChunks('update', operation.startIndex, operation.oldValues.size);
    }
    else if (operation.action === 'insert') {
      this._removeObsoleteChunks('delete', operation.startIndex, operation.newValues.size);
    }
    else if (operation.action === 'delete') {
      this._removeObsoleteChunks('insert', operation.startIndex, operation.oldValues.size);
    }
    else {
      throw new Error('Unsupported undo action: ' + operation.action);
    }
    return operation;
  }

  /**
   * Redoes the last undo operation.
   * @returns The redone operation or null if there was no redo operation.
   */
  redo() {
    const operation = this._document.redo();
    if (operation === null) {
      return null;
    }
    const size = operation.newValues ? operation.newValues.size : operation.oldValues.size;
    this._removeObsoleteChunks(operation.action, operation.startIndex, size);
    return operation;
  }

}

export default ChunkBufferingDocument;