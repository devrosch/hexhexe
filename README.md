# HexHexe

HexHexe is a hex editor that runs in the browser.

You can find the application published at [GitLab Pages](https://devrosch.gitlab.io/hexhexe/).

## Features
* A hex editor, running in the browser.
* Edit arbitrarily sized files.
* Responsive. Open documents on mobile devices.
* Offline capable. After the first start, the application will run without network connectivity.
* All data processing happens in the browser. The files you open and the data you enter are not sent to a server.
* Compatible with recent version of Firefox, Chrome, and Safari.

## Limitations
* Editing data on mobile devices is currently not possible.
* File open shortcut does not work reliably across browsers and platforms. This is due to browser security features and likely cannot be fixed.
* While this software should work on any browser, it has not tested for browsers other than those listed above.

## Development

### Prerequisites

You will need recent versions of git, Node.js and npm to download the source code and to build this application. Recent versions of the above listed browsers (Firefox or Chrome recommended) can be used to run the application. Other browsers have not been tested for compatibility.

### Running a development build

To download and build this application, go through the following steps.

First run
```
git clone https://gitlab.com/devrosch/hexhexe
```
to download the source code.

Change to the newly created folder and run
```
npm install
```
to download all dependencies.

To start the development server and open the app in a browser, open a console, change to the root folder and run:
```
npm run start
```

### Running the tests

To run the automated tests, enter
```
npm run test
```
from the root folder.

### Deployment

In order to create a production build, run
```
npm run build
```
which will write its output to the "build" folder.

## Built With

* [react](https://www.npmjs.com/package/react) (license: [MIT](https://github.com/facebook/react/blob/master/LICENSE), source code: [GitHub](https://github.com/facebook/react))
* [react-dom](https://www.npmjs.com/package/react-dom) (license: [MIT](https://github.com/facebook/react/blob/master/LICENSE), source code: [GitHub](https://github.com/facebook/react))
* [antd](https://www.npmjs.com/package/antd) (license: [MIT](https://github.com/ant-design/ant-design/blob/master/LICENSE), source code: [GitHub](https://github.com/ant-design/ant-design))
* [@ant-design/icons](https://www.npmjs.com/package/@ant-design/icons) (license: [MIT](https://github.com/ant-design/ant-design-icons/blob/master/packages/icons-react/LICENSE), source code: [GitHub](https://github.com/ant-design/ant-design-icons))
* [int64-buffer](https://www.npmjs.com/package/int64-buffer) (license: [MIT](https://github.com/kawanet/int64-buffer/blob/master/LICENSE), source code: [GitHub](https://github.com/kawanet/int64-buffer))
* [Workbox](https://developer.chrome.com/docs/workbox) (license: [MIT](https://github.com/GoogleChrome/workbox/blob/v6/LICENSE), source code: [GitHub](https://github.com/googlechrome/workbox), see HexHexe's "package.json" for details on which components are used)

and for development

* [react-scripts / create-react-app](https://www.npmjs.com/package/react-scripts) (license: [MIT](https://github.com/facebook/create-react-app/blob/master/LICENSE), source code: [GitHub](https://github.com/facebook/create-react-app))
* [babel-plugin-import](https://www.npmjs.com/package/babel-plugin-import) (license: [MIT](https://github.com/ant-design/babel-plugin-import/blob/master/package.json), source code: [GitHub](https://github.com/ant-design/babel-plugin-import))

and their various dependencies.

## Authors

* **Robert Schiwon** - *Main developer* - [devrosch](https://gitlab.com/devrosch)

## License

Copyright (C) 2018-2020, 2022 Robert Schiwon

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
